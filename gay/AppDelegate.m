//
//  AppDelegate.m
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

#import "ImSDK.h"
#import "IQKeyboardManager.h"
#import <Bugly/Bugly.h>
#import "TUIKit.h"
#import "Firebase.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self initSDK];
    
    if (![DataTool isEmptyString:[UserInfo shareInstant].token] ) { //homePage
        [[UserInfo shareInstant] checkUserDataModel:[UserInfo shareInstant].user callBackInfo:^(id  _Nullable obj) {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"appdelegate_userLogin"];
            [[UserInfo shareInstant] userLogin];
            NSLog(@"userid %@", [UserInfo shareInstant].user.userId);
        }];
    } else {
        [[NetTool shareInstance] sendFireBaseWithEventName:@"appdelegate_userLogout"];
        [[UserInfo shareInstant] userLogout];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[SocketTool shareInstance] closeSocket];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [SocketTool shareInstance].reConnectTime = 0;
    [[SocketTool shareInstance] reconnectSocket];
}

- (void) initSDK {
    
    [[IQKeyboardManager sharedManager] setEnable:YES]; //keyboard
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:NO];
    
    BuglyConfig *config = [[BuglyConfig alloc] init]; //设置bug监测
    config.unexpectedTerminatingDetectionEnable = YES;
    config.reportLogLevel = BuglyLogLevelError;
    [Bugly startWithAppId:BuglyID developmentDevice:YES config:config];
    
    [FIRApp configure];
    [[TUIKit sharedInstance] setupWithAppId:[TXIMSDK_APP_KEY longLongValue]]; // SDKAppID
    [self registNotification];
}

- (void)registNotification {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:
                [UIUserNotificationSettings settingsForTypes:
                (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
}

//在 AppDelegate 的回调中会返回 deviceToken，需要在登录后上报给腾讯云后台
-(void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:DeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

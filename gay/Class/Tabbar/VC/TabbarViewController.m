//
//  TabbarViewController.m
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "TabbarViewController.h"

#import "HomeViewController.h"
#import "DiscoverViewController.h"
#import "PostViewController.h"
#import "MessageViewController.h"
#import "MeViewController.h"

@interface TabbarViewController ()

@end

@implementation TabbarViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    
    DiscoverViewController *discoverVC = [[DiscoverViewController alloc]init]; //搜索用户
    PostViewController *postVC = [[PostViewController alloc]init]; //发布内容
    HomeViewController *homeVC = [[HomeViewController alloc] init]; //活动界面
    MessageViewController *messageVC = [[MessageViewController alloc]init];
    MeViewController *meVC = [[MeViewController alloc] init];
    
    [self setUpOneChildVcWithVc:discoverVC Image:@"tabbar_discovery_default" selectedImage:@"tabbar_discovery_selected" title:@"Discover"];
    [self setUpOneChildVcWithVc:postVC Image:@"tabbar_post_default" selectedImage:@"tabbar_post_selected" title:@"Post"];
    [self setUpOneChildVcWithVc:homeVC Image:@"tabbar_home_default" selectedImage:@"tabbar_home_selected" title:@"Home"];
    [self setUpOneChildVcWithVc:messageVC Image:@"tabbar_message_default" selectedImage:@"tabbar_message_selected" title:@"Message"];
    [self setUpOneChildVcWithVc:meVC Image:@"tabbar_user_default" selectedImage:@"tabbar_user_selected" title:@"Me"];
    
    //去掉tabbar上的黑线，并设置背景色为白色
//    [UITabBar appearance].clipsToBounds = YES;
//    [UINavigationBar appearance].clipsToBounds = YES; //去掉导航栏上面的黑线
//    [UITabBar appearance].tintColor = BlackColor;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, TabBarHeight)];
    view.backgroundColor = RGB(27, 31, 32);
    [[UITabBar appearance] insertSubview:view atIndex:0];
    
}

- (void)setData {
    
    //设置tabbar的预加载
    for(UINavigationController *nav in  self.viewControllers){
        if ([self.viewControllers indexOfObject:nav] == 0 || [self.viewControllers indexOfObject:nav] == 3 ) { // 指定需要加载的vc
            UIViewController *viewController = nav.viewControllers.firstObject;
            if (@available(iOS 9.0, *)) {
                [viewController loadViewIfNeeded];
            } else {
                [viewController loadView];
            }
        }
    }
    
}

#pragma mark - function

- (void)setUpOneChildVcWithVc:(UIViewController *)Vc Image:(NSString *)image selectedImage:(NSString *)selectedImage title:(NSString *)title {
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:Vc];
    
    UIImage *myImage = [UIImage imageNamed:image];
    myImage = [myImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //tabBarItem，是系统提供模型，专门负责tabbar上按钮的文字以及图片展示
    
    Vc.tabBarItem.image = myImage;
    UIImage *mySelectedImage = [UIImage imageNamed:selectedImage];
    mySelectedImage = [mySelectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    Vc.tabBarItem.selectedImage = mySelectedImage;
    
//    Vc.tabBarItem.title = title;
    
    //     设置 tabbarItem 选中状态下的文字颜色(不被系统默认渲染,显示文字自定义颜色)
//    NSDictionary *dictHome = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:255.0f/255.0f green:53.0f/255.0f blue:111.0f/255.0f alpha:1.0f] forKey:NSForegroundColorAttributeName];
    
//    [Vc.tabBarItem setTitleTextAttributes:dictHome forState:UIControlStateSelected];
//    Vc.navigationItem.title = title;
//    [Vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -3)];
    
    [self addChildViewController:nav];
    
}

//- (void) showHomeTab {
//    [self setSelectedIndex:1];
//}

@end

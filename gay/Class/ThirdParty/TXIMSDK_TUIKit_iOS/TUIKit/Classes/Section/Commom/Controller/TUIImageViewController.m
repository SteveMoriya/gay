//
//  TUIImageViewController.m
//  TUIKitDemo
//
//  Created by kennethmiao on 2018/10/31.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#import "TUIImageViewController.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "ISVImageScrollView.h"
#import "MMLayout/UIView+MMLayout.h"
#import <UIImageView+WebCache.h>

@interface TUIImageViewController () <UIScrollViewDelegate>
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *progress;
@property ISVImageScrollView *imageScrollView;

@property UIImage *saveBackgroundImage;
@property UIImage *saveShadowImage;

@end

@implementation TUIImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.saveBackgroundImage = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
    self.saveShadowImage = self.navigationController.navigationBar.shadowImage;

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];

    self.imageScrollView = [[ISVImageScrollView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.imageScrollView];
    self.imageScrollView.backgroundColor = [UIColor blackColor];
    self.imageScrollView.mm_fill();

    self.imageView = [[UIImageView alloc] initWithImage:nil];
    self.imageScrollView.imageView = self.imageView;
    self.imageScrollView.maximumZoomScale = 4.0;
    self.imageScrollView.delegate = self;

    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    singleFingerOne.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:singleFingerOne];

    BOOL isExist = NO;
    [_data getImagePath:TImage_Type_Origin isExist:&isExist];
    if (isExist) {
        if(_data.originImage) {
            _imageView.image = _data.originImage;
        } else {
            
            [_data decodeImage:TImage_Type_Origin];
            @weakify(self)
            [RACObserve(_data, originImage) subscribeNext:^(UIImage *x) {
                @strongify(self)
                
                self.imageView.image = x;
                [self.imageScrollView setNeedsLayout];
            }];
        }
    } else {
        
//        _imageView.image = _data.thumbImage;
//        _progress = [[UILabel alloc] initWithFrame:self.view.bounds];
//        _progress.textColor = [UIColor whiteColor];
//        _progress.font = [UIFont systemFontOfSize:18];
//        _progress.textAlignment = NSTextAlignmentCenter;
//        [self.view addSubview:_progress];
        
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:((TUIImageItem *) _data.items.firstObject).url] placeholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
            dispatch_async(dispatch_get_main_queue(), ^{
                int progress = (CGFloat)receivedSize / expectedSize * 100;
                self.progress.text = [NSString stringWithFormat:@"%d%%", progress];
                self.progress.hidden = (progress >= 100 || progress == 0);
            });
        } completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            self.imageView.image = image;
        }];
        
    }

}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)downloadOrigin:(id)sender
{
    [_data downloadImage:TImage_Type_Origin];
    @weakify(self)
    [RACObserve(_data, originImage) subscribeNext:^(UIImage *x) {
        @strongify(self)
        if (x) {
            self.imageView.image = x;
            [self.imageScrollView setNeedsLayout];
            self.progress.hidden = YES;
        } else {
            
//            [self.imageView sd_setImageWithURL:[NSURL URLWithString:((TUIImageItem *)data.items.firstObject).url] placeholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    int progress = (CGFloat)receivedSize / expectedSize * 100;
//                    self.progress.text = [NSString stringWithFormat:@"%d%%", progress];
//                    self.progress.hidden = (progress >= 100 || progress == 0);
//                });
//            } completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//                self.thumb.image = image;
//            }];
            
        }
        
    }];
    [RACObserve(_data, originProgress) subscribeNext:^(NSNumber *x) {
        @strongify(self)
        int progress = [x intValue];
        self.progress.text =  [NSString stringWithFormat:@"%d%%", progress];
        if (progress >= 100)
            self.progress.hidden = YES;
    }];
    self.button.hidden = YES;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    if (parent == nil) {
        [self.navigationController.navigationBar setBackgroundImage:self.saveBackgroundImage
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = self.saveShadowImage;
    }
}
@end

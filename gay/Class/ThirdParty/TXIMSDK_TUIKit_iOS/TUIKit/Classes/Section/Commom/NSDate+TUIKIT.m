//
//  NSDate+TUIKIT.m
//  TXIMSDK_TUIKit_iOS
//
//  Created by annidyfeng on 2019/5/20.
//

#import "NSDate+TUIKIT.h"

@implementation NSDate (TUIKIT)

- (NSString *)tk_messageString
{

    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:self];
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc ] init ];

    NSDateComponents *comp =  [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday fromDate:self];

    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy/MM/dd";
    }
    else{
        if (nowCmps.day==myCmps.day) {
            dateFmt.dateFormat = @"HH:mm";
        } else if((nowCmps.day-myCmps.day)==1) {
            dateFmt.AMSymbol = @"AM";
            dateFmt.PMSymbol = @"PM";
            dateFmt.dateFormat = @"yyyy/MM/dd";
        } else {
            if ((nowCmps.day-myCmps.day) <=7) {
                switch (comp.weekday) {
                    case 1:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    case 2:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    case 3:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    case 4:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    case 5:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    case 6:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    case 7:
                        dateFmt.dateFormat = @"EEEE";
                        break;
                    default:
                        break;
                }
            }else {
                dateFmt.dateFormat = @"yyyy/MM/dd";
            }
        }
    }
    return [dateFmt stringFromDate:self];
}
@end

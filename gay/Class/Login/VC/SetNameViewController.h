//
//  SetNameViewController.h
//  gay
//
//  Created by steve on 2020/10/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetNameViewController : UIViewController

@property (strong, nonatomic) NSString *ageString;

@end

NS_ASSUME_NONNULL_END

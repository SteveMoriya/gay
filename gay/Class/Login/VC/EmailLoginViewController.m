//
//  EmailLoginViewController.m
//  gay
//
//  Created by steve on 2020/10/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "EmailLoginViewController.h"
#import "VerificationCodeViewController.h"

@interface EmailLoginViewController () {
    UITextField *emailTF;
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;

@end

@implementation EmailLoginViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    
    [self.view addSubview:self.navBgView];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(290))/2.0, ZoomSize(133), ZoomSize(290), ZoomSize(29))];
    [self.view addSubview:tipLb];
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.text = @"What’s your email address?";
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(24)];
    tipLb.textColor = WhiteColor;
    
    emailTF = [[UITextField alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(190), ZoomSize(294), ZoomSize(30))];
    [self.view addSubview:emailTF];
    emailTF.font = [UIFont systemFontOfSize:ZoomSize(20)];
    emailTF.textColor = TextWhite;
    emailTF.tintColor = TextWhite;
    emailTF.textAlignment = NSTextAlignmentCenter;
    emailTF.keyboardType = UIKeyboardTypeEmailAddress;
    [emailTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *tipLineView = [[UIView alloc] init];
    tipLineView.frame = CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(224), ZoomSize(294), ZoomSize(2));
    [self.view addSubview:tipLineView];
    tipLineView.backgroundColor = TextWhite;
    
    continueBt = [[UIButton alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(303))/2.0, ZoomSize(313), ZoomSize(303), ZoomSize(48)) ];
    [self.view addSubview:continueBt];
    continueBt.layer.cornerRadius = ZoomSize(24);
    [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
    [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    continueBt.backgroundColor = TextGray;
    [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    continueBt.enabled = NO;
    
}

- (void)setData {
    [emailTF becomeFirstResponder];
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) textFieldChange :(id) sender {
    UITextField  *getTextField = sender;
    
    if (getTextField == emailTF) {
        if ([DataTool isValidateEmail:getTextField.text]) {
            [continueBt setBackgroundColor:ThemeYellow];
            continueBt.enabled = YES;
        } else {
            [continueBt setBackgroundColor:TextGray];
            continueBt.enabled = NO;
        }
    }
}

- (void) continueAction {
    NSLog(@"continueAction");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"emailLogin_continue"];
    
    NSDictionary *paramsDic = @{ @"email":emailTF.text};
    [[NetTool shareInstance] startRequest:API_user_email_code method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        VerificationCodeViewController *vc = [[VerificationCodeViewController alloc] init];
        vc.emailString = self->emailTF.text;
        [DataTool.getCurrentVC.navigationController pushViewController:vc animated:YES];
    }];
}

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}

@end

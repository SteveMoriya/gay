//
//  LoginViewController.m
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "LoginViewController.h"
#import "EmailLoginViewController.h"
#import "SetAgeViewController.h"

#import <AuthenticationServices/AuthenticationServices.h>

@interface LoginViewController ()<UITextViewDelegate, ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
    
#pragma 添加默认设置
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:UserVideoCall];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ChatRoom];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    
    UIImageView *bgIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
    [self.view addSubview:bgIV];
    bgIV.image = [UIImage imageNamed:@"launch_background"];
    bgIV.contentMode = UIViewContentModeScaleAspectFill;
    
    if (@available(iOS 13.0, *)) {//13.0的新属性
        UIButton *appleIDSignInButton = [[UIButton alloc] initWithFrame:CGRectMake( (Screen_Width - ZoomSize(303))/2.0, Screen_Height - Bottom_SafeHeight - ZoomSize(240), ZoomSize(303), ZoomSize(48)) ];
        [self.view addSubview:appleIDSignInButton];
        [appleIDSignInButton addTarget:self action:@selector(appleLoginAction) forControlEvents:UIControlEventTouchUpInside];
        
        appleIDSignInButton.backgroundColor = WhiteColor;
        [appleIDSignInButton setTitle:@"Continue with Apple" forState:UIControlStateNormal];
        [appleIDSignInButton setTitleColor:BlackColor forState:UIControlStateNormal];
        [appleIDSignInButton setImage:[UIImage imageNamed:@"login_apple"] forState:UIControlStateNormal];
        appleIDSignInButton.imageEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(-10), 0, 0);
        appleIDSignInButton.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        appleIDSignInButton.layer.cornerRadius = ZoomSize(24);
    }
    
    UIButton *loginBt = [[UIButton alloc] initWithFrame:CGRectMake( (Screen_Width - ZoomSize(303))/2.0, Screen_Height - Bottom_SafeHeight - ZoomSize(180), ZoomSize(303), ZoomSize(48)) ];
    [self.view addSubview:loginBt];
    loginBt.layer.cornerRadius = ZoomSize(24);
    [loginBt setTitle:@"Direct Login" forState:UIControlStateNormal];
    [loginBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    loginBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    loginBt.backgroundColor = ThemeYellow;
    [loginBt addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *emailLoginBt = [[UIButton alloc] initWithFrame:CGRectMake( (Screen_Width - ZoomSize(303))/2.0, Screen_Height - Bottom_SafeHeight - ZoomSize(125), ZoomSize(303), ZoomSize(40)) ];
    [self.view addSubview:emailLoginBt];
    [emailLoginBt setTitle:@"or Login In With Email" forState:UIControlStateNormal];
    [emailLoginBt setTitleColor:ThemeYellow forState:UIControlStateNormal];
    emailLoginBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
    [emailLoginBt addTarget:self action:@selector(emailLoginAction) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *content = @"By creating an account for signing back in you are agreeing to our Terms and Privacy";
    UITextView *contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(( Screen_Width - ZoomSize(288))/2.0, Screen_Height - Bottom_SafeHeight - ZoomSize(70), ZoomSize(288), ZoomSize(50))];
    [self.view addSubview:contentTextView];
    contentTextView.attributedText = [self getContentLabelAttributedText:content];
    contentTextView.backgroundColor = UIColor.clearColor;
    contentTextView.linkTextAttributes = @{NSForegroundColorAttributeName:UIColor.whiteColor};
    contentTextView.textAlignment = NSTextAlignmentCenter;
    contentTextView.delegate = self;
    contentTextView.editable = NO;
    contentTextView.scrollEnabled = NO;
    
}

- (void)setData {
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    
    if ([[URL scheme] isEqualToString:@"Terms"]) {
        [DataTool openWebVC:TermURLString];
    } else if ([[URL scheme] isEqualToString:@"Privacy"]) {
        [DataTool openWebVC:PolicyURLString];
    }
    
    return YES;
}

#pragma function

- (void) emailLoginAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"login_email"];
    EmailLoginViewController *vc = [[EmailLoginViewController alloc] init];
    [DataTool.getCurrentVC.navigationController pushViewController:vc animated:YES];
}

- (void) loginAction {
//    NSLog(@"loginAction");
    
    NSDictionary *paramsDic = @{@"deviceId":[DataTool getDeviceUUID]};
    [[NetTool shareInstance] startRequest:API_Login_Device method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
       
        NSDictionary *resultDic = obj;
        [UserInfo shareInstant].token = resultDic[@"token"];
        [UserInfo shareInstant].imSig = resultDic[@"imSig"];
        
        NSDictionary *userParams = @{};
        [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {

            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];

            if ([DataTool isEmptyString:userProfileDataModel.age]) {
                SetAgeViewController *vc = [[SetAgeViewController alloc] init];
                [DataTool.getCurrentVC.navigationController pushViewController:vc animated:YES];
            } else {
                [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                    [[UserInfo shareInstant] userLogin];
                }];
            }
        }];
        
    }];
    
}

- (void) appleLoginAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"login_apple"];
    if (@available(iOS 13.0, *)) {//13.0的新属性
        ASAuthorizationAppleIDProvider *appleIdProvider = [[ASAuthorizationAppleIDProvider alloc] init]; // 创建新的AppleID 授权请求
        ASAuthorizationAppleIDRequest *request = appleIdProvider.createRequest; // 在用户授权期间请求的联系信息
        request.requestedScopes = @[ASAuthorizationScopeEmail,ASAuthorizationScopeFullName];
        ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
        controller.delegate = self;
        controller.presentationContextProvider = self;
        [controller performRequests];
    }
}

- (NSAttributedString *)getContentLabelAttributedText:(NSString *)text {
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(14)],NSForegroundColorAttributeName:TextGray, NSParagraphStyleAttributeName: paragraphStyle}];
    
    NSDictionary *attribtDic1 = @{NSLinkAttributeName: @"Terms://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic1 range:NSMakeRange(text.length - 17, 5)];
    
    NSDictionary *attribtDic2 = @{NSLinkAttributeName: @"Privacy://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic2 range:NSMakeRange(text.length - 7, 7)];
    
    return attrStr;
}

//#pragma mark - 授权成功的回调
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)) {
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        
        NSString *getEmailString = @"";
        NSString *getUserIdString = @"";
        NSString *getNameString = @"";
        
        ASAuthorizationAppleIDCredential *credential = (ASAuthorizationAppleIDCredential *)authorization.credential; // 用户登录使用ASAuthorizationAppleIDCredential
        NSString *getIdentityTokenString = [[NSString alloc]initWithData:credential.identityToken encoding:NSUTF8StringEncoding];
        
        if ([DataTool isEmptyString:credential.email]) { //由于可能隐藏，这里使用存储
            if (![DataTool isEmptyString:[DataTool getContentFromKeyChain:@"appleSignInEmail"]]) {
                getEmailString = [DataTool getContentFromKeyChain:@"appleSignInEmail"];
            }
        } else {
            getEmailString = credential.email;
            [DataTool saveContentToKeyChain:credential.email forKey:@"appleSignInEmail" service:@"DeviceUUID"];
        }
        
        if (![DataTool isEmptyString:credential.user]) {
            getUserIdString = credential.user;
        }
        
        if (![DataTool isEmptyString:credential.fullName.givenName]) { //名字有许多内容，这里选一个就行
            getNameString = credential.fullName.givenName;
        }
        
        NSDictionary *paramsDic = @{@"email":getEmailString, @"identityToken":getIdentityTokenString, @"appleUserId":getUserIdString, @"name":getNameString};
        
        [[NetTool shareInstance] startRequest:API_Login_Apple method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            
            NSDictionary *resultDic = obj;
            [UserInfo shareInstant].token = resultDic[@"token"];
            [UserInfo shareInstant].imSig = resultDic[@"imSig"];
            
            NSDictionary *userParams = @{};
            [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
                
                NSDictionary *resultDic = obj;
                UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
                
                if ([DataTool isEmptyString:userProfileDataModel.age]) {
                    SetAgeViewController *vc = [[SetAgeViewController alloc] init];
                    [DataTool.getCurrentVC.navigationController pushViewController:vc animated:YES];
                } else {
                    [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                        [[UserInfo shareInstant] userLogin];
                    }];
                }
            }];
        }];
        
    } else {
        [DataTool showHUDWithString:@"please retry later"];
    }
}

- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller API_AVAILABLE(ios(13.0)){
    return self.view.window;
}

@end

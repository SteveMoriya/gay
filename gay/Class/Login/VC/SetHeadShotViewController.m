//
//  SetHeadShotViewController.m
//  gay
//
//  Created by steve on 2020/10/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "SetHeadShotViewController.h"

#import "SelectPhotoManager.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SetHeadShotViewController (){
    UIImageView *chooseImageView;
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation SetHeadShotViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    
    self.view.backgroundColor = BGBlackColor;
    
    [self.view addSubview:self.navBgView];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(290))/2.0, ZoomSize(133), ZoomSize(290), ZoomSize(29))];
    [self.view addSubview:tipLb];
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.text = @"Upload Your Photo";
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(24)];
    tipLb.textColor = WhiteColor;
    
    chooseImageView = [[UIImageView alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(151))/2.0, ZoomSize(215), ZoomSize(151), ZoomSize(151))];
    [self.view addSubview:chooseImageView];
    chooseImageView.layer.cornerRadius = ZoomSize(76);
    chooseImageView.layer.masksToBounds = YES;
    chooseImageView.image = [UIImage imageNamed:@"login_upload_photo"];
    [chooseImageView setContentMode:UIViewContentModeScaleAspectFill];
    chooseImageView.layer.masksToBounds = YES;
    
    UIButton *chooseImageBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width/2.0 + ZoomSize(25), ZoomSize(315), ZoomSize(51), ZoomSize(51))];
    [self.view addSubview:chooseImageBt];
    [chooseImageBt setImage:[UIImage imageNamed:@"login_camera"] forState:UIControlStateNormal];
    [chooseImageBt addTarget:self action:@selector(chooseImageAction) forControlEvents:UIControlEventTouchUpInside];
    
    continueBt = [[UIButton alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(303))/2.0, ZoomSize(460), ZoomSize(303), ZoomSize(48)) ];
    [self.view addSubview:continueBt];
    continueBt.layer.cornerRadius = ZoomSize(24);
    [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
    [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    continueBt.backgroundColor = TextGray;
    [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    continueBt.enabled = NO;
    
//    UIButton *skipBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), ZoomSize(570), ZoomSize(303), ZoomSize(48)) ];
//    [self.view addSubview:skipBt];
//    [skipBt setTitle:@"SKIP" forState:UIControlStateNormal];
//    [skipBt setTitleColor:WhiteColor forState:UIControlStateNormal];
//    skipBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
//    [skipBt addTarget:self action:@selector(skipAction) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setData {
    _photoManager = [[SelectPhotoManager alloc] init];
    [self chooseImageAction];
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)chooseImageAction {
    NSLog(@"chooseImageAction");
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    
    __weak __typeof (self) weakSelf = self;
    _photoManager.successHandle = ^(SelectPhotoManager * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        __strong __typeof(self) strongSelf = weakSelf;
        strongSelf->chooseImageView.image = resizeImage;
        
        [strongSelf->continueBt setBackgroundColor:ThemeYellow];
        strongSelf->continueBt.enabled = YES;
    };
}

- (void)continueAction {
    NSLog(@"continueAction");
    
    if (chooseImageView.image == nil) {
        [DataTool showHUDWithString:@"please set the headshot"];
        [self chooseImageAction];
        return;
    }
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"SetHeadShot_continue"];
    
    NSData *resizeImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:chooseImageView.image], 0.25);
    NSMutableArray *dataArray = [NSMutableArray arrayWithObjects: resizeImageData, nil];
    
    [[NetTool shareInstance] putDatas:dataArray fileType:@"jpg" needShowHUD:YES callback:^(id  _Nullable obj) {
        NSMutableArray *getArray = obj;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSDictionary *paramsDic = @{@"headImgUrl":getArray[0], @"age":self.ageString, @"nickName":self.nameString, @"identity":self.identityString};

            [[NetTool shareInstance] startRequest:API_user method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                
                NSDictionary *resultDic = obj;
                UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
                
                [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                    [[UserInfo shareInstant] userLogin];
                }];
            }];
        });
    }];
    
}

//- (void)skipAction {
//
//    NSDictionary *paramsDic = @{@"age":self.ageString, @"nickName":self.nameString, @"identity":self.identityString};
//
//    [[NetTool shareInstance] startRequest:API_user method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
//
//        NSDictionary *resultDic = obj;
//        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
//
//        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
//            [[UserInfo shareInstant] userLogin];
//        }];
//    }];
//}

#pragma Lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}



@end

//
//  VerificationCodeViewController.m
//  gay
//
//  Created by steve on 2020/10/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "VerificationCodeViewController.h"
#import "SetAgeViewController.h"

@interface VerificationCodeViewController () {
    UITextField *codeTF;
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;

@end

@implementation VerificationCodeViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    
    self.view.backgroundColor = BGBlackColor;
    
    [self.view addSubview:self.navBgView];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(290))/2.0, ZoomSize(133), ZoomSize(290), ZoomSize(29))];
    [self.view addSubview:tipLb];
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.text = @"Verfication Code";
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(24)];
    tipLb.textColor = WhiteColor;
    
    codeTF = [[UITextField alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(190), ZoomSize(294), ZoomSize(30))];
    [self.view addSubview:codeTF];
    codeTF.font = [UIFont systemFontOfSize:ZoomSize(20)];
    codeTF.textColor = TextWhite;
    codeTF.tintColor = TextWhite;
    codeTF.textAlignment = NSTextAlignmentCenter;
    codeTF.keyboardType = UIKeyboardTypeNumberPad;
    [codeTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *tipLineView = [[UIView alloc] init];
    tipLineView.frame = CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(224), ZoomSize(294), ZoomSize(2));
    [self.view addSubview:tipLineView];
    tipLineView.backgroundColor = TextWhite;
    
    UILabel *infoTipLb = [[UILabel alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(228), ZoomSize(294), ZoomSize(50))];
    [self.view addSubview:infoTipLb];
    infoTipLb.numberOfLines = 0;
    infoTipLb.textAlignment = NSTextAlignmentCenter;
    infoTipLb.text = @"We just sent you a verification code to your Email, please check!";
    infoTipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    infoTipLb.textColor = TextGray;
    
    continueBt = [[UIButton alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(303))/2.0, ZoomSize(313), ZoomSize(303), ZoomSize(48)) ];
    [self.view addSubview:continueBt];
    continueBt.layer.cornerRadius = ZoomSize(24);
    [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
    [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    continueBt.backgroundColor = TextGray;
    [continueBt addTarget:self action:@selector(verifyCodeAction) forControlEvents:UIControlEventTouchUpInside];
    continueBt.enabled = NO;
    
}

- (void)setData {
    [codeTF becomeFirstResponder];
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) textFieldChange :(id) sender {
    UITextField  *getTextField = sender;
    
    if (getTextField == codeTF) {
        
        if (getTextField.text.length > 6) {
            getTextField.text = [getTextField.text substringToIndex:6];
        }
        
        if (getTextField.text.length == 6) {
            [continueBt setBackgroundColor:ThemeYellow];
            continueBt.enabled = YES;
        } else {
            [continueBt setBackgroundColor:TextGray];
            continueBt.enabled = NO;
        }
    }
}

- (void) verifyCodeAction {
    NSLog(@"verifyCodeAction");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"verifyCode_continue"];
//#warning 修改为不需要uuid，不校验设备唯一性
    NSDictionary *paramsDic = @{ @"email":_emailString, @"code":codeTF.text};
    
    [[NetTool shareInstance] startRequest:API_user_verify_mail_code method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        NSDictionary *resultDic = obj;
        [UserInfo shareInstant].token = resultDic[@"token"];
        [UserInfo shareInstant].imSig = resultDic[@"imSig"];
        
        NSDictionary *userParams = @{};
        [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {

            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];

            if ([DataTool isEmptyString:userProfileDataModel.age]) {
                SetAgeViewController *vc = [[SetAgeViewController alloc] init];
                [DataTool.getCurrentVC.navigationController pushViewController:vc animated:YES];
            } else {
                [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                    [[UserInfo shareInstant] userLogin];
                }];
            }
            
        }];
    }];
    
    
}

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}

@end

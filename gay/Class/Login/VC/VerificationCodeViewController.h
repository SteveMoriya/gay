//
//  VerificationCodeViewController.h
//  gay
//
//  Created by steve on 2020/10/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerificationCodeViewController : UIViewController

@property (nonatomic, strong) NSString *emailString;

@end

NS_ASSUME_NONNULL_END

//
//  SetAgeViewController.m
//  gay
//
//  Created by steve on 2020/10/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "SetAgeViewController.h"
#import "SetNameViewController.h"

#import "JKPickView.h"

@interface SetAgeViewController (){
    UITextField *ageTF;
    NSString *chooseAgeString;
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;

@end

@implementation SetAgeViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    
    self.view.backgroundColor = BGBlackColor;
    
    [self.view addSubview:self.navBgView];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(290))/2.0, ZoomSize(133), ZoomSize(290), ZoomSize(29))];
    [self.view addSubview:tipLb];
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.text = @"Your age?";
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(24)];
    tipLb.textColor = WhiteColor;

    ageTF =  [[UITextField alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(190), ZoomSize(294), ZoomSize(30))];
    [self.view addSubview:ageTF];
    ageTF.font = [UIFont systemFontOfSize:ZoomSize(20)];
    ageTF.textColor = TextWhite;
    ageTF.tintColor = TextWhite;
    ageTF.textAlignment = NSTextAlignmentCenter;
    
    UIButton *chooseAgeBt = [[UIButton alloc] initWithFrame:ageTF.frame];
    [self.view addSubview:chooseAgeBt];
    [chooseAgeBt addTarget:self action:@selector(chooseAgeAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *tipLineView = [[UIView alloc] init];
    tipLineView.frame = CGRectMake((Screen_Width - ZoomSize(294))/2.0, ZoomSize(224), ZoomSize(294), ZoomSize(2));
    [self.view addSubview:tipLineView];
    tipLineView.backgroundColor = TextWhite;
    
    continueBt = [[UIButton alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(303))/2.0, ZoomSize(313), ZoomSize(303), ZoomSize(48)) ];
    [self.view addSubview:continueBt];
    continueBt.layer.cornerRadius = ZoomSize(24);
    [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
    [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    continueBt.backgroundColor = TextGray;
    [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    continueBt.enabled = NO;
    
}

- (void)setData {
    [self chooseAgeAction];
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)continueAction {
    
    if ([DataTool isEmptyString:ageTF.text]) {
        [DataTool showHUDWithString:@"please fill the age"];
        [self chooseAgeAction];
        return;
    }
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"SetAge_continue"];
    SetNameViewController *vc = [[SetNameViewController alloc] init];
    vc.ageString = ageTF.text;
    [DataTool.getCurrentVC.navigationController pushViewController:vc animated:YES];
    
}

- (void)chooseAgeAction {
    
    JKPickView *pickview = [[JKPickView alloc] initWithFrame:self.view.frame];
    [pickview.dataArray addObjectsFromArray:AgeArray];
    [pickview show];
    
    NSInteger getIndex = 7;
    if (![DataTool isEmptyString:self->chooseAgeString]) {
        getIndex = [AgeArray indexOfObject:self->chooseAgeString];
    }
    
    pickview.selectedRow = [NSString stringWithFormat:@"%ld", (long)getIndex];
    pickview.titleString = @"Choose age";
    
    pickview.back = ^(NSInteger getIndex) {
        self->chooseAgeString = AgeArray[getIndex];
        self->ageTF.text = AgeArray[getIndex];
        
        [self->continueBt setBackgroundColor:ThemeYellow];
        self->continueBt.enabled = YES;
    };
    
}

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}


@end

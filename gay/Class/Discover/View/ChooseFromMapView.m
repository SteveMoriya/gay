//
//  ChooseFromMapView.m
//  gay
//
//  Created by steve on 2020/11/12.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ChooseFromMapView.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ChooseFromMapView()<MKMapViewDelegate> {
    //地图视图
    MKMapView* createMapView;
    MKPointAnnotation* pointAnnotation;
//    CLLocationManager *locatonmanger;
    
    NSArray *getGeolocationArray;
    CLLocationCoordinate2D touchMapCoordinate; //获取到的定位信息
    
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation ChooseFromMapView

- (instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
//    locatonmanger = [[CLLocationManager alloc]init]; //1、主动请求位置授权方式
//    [locatonmanger requestAlwaysAuthorization];
//    createMapView.userTrackingMode= MKUserTrackingModeFollow; //2、设置用户追踪模式
    
//    float latitude = 37.09024;
//    float longitude = -95.712891;
//
//    CLLocationCoordinate2D lastLocation = CLLocationCoordinate2DMake(latitude, longitude);
//    touchMapCoordinate = lastLocation;
//    getGeolocationArray = [NSArray arrayWithObjects:[NSNumber numberWithFloat:touchMapCoordinate.latitude], [NSNumber numberWithFloat:touchMapCoordinate.longitude],nil]; //获取经纬度
}

#pragma function

- (void)show {
    [[DataTool getCurrentVC].view addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, 0 , self.frame.size.width, self.frame.size.height)];
    }];
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height, self.frame.size.width, self.frame.size.height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}

//- (void) closeAction {
//    if (pointAnnotation != nil) {
//        [createMapView removeAnnotation:pointAnnotation]; //删除大头针
//    }
//
//    getGeolocationArray = nil;
//    if (_getLocationBlock) {
//        _getLocationBlock(getGeolocationArray);
//    }
//
//    [self dismiss];
//}

- (void) doneAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChooseLocationView_done"];
    
    if (_getLocationBlock) {
        _getLocationBlock(getGeolocationArray);
    }
    [self dismiss];
}

- (void) getPlaceAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChooseLocationView_getPlace"];
    
    NSLog(@"getPlaceAction");
    
//    获取一个随机位置
    NSString *pathBundle = [[NSBundle mainBundle]pathForResource:@"CityLocation" ofType:@"plist"];
    NSArray *cityLocationArray = [NSArray arrayWithContentsOfFile:pathBundle];
    int getIndex  = arc4random() % cityLocationArray.count ;
    
    NSDictionary *getDic = cityLocationArray[getIndex];
    float latitude = [getDic[@"latitude"] floatValue];
    float longitude = [getDic[@"lontitude"] floatValue];
    
    CLLocationCoordinate2D lastLocation = CLLocationCoordinate2DMake(latitude, longitude);
    touchMapCoordinate = lastLocation;
    
    CLLocationCoordinate2D center = lastLocation;
    MKCoordinateSpan span = MKCoordinateSpanMake(createMapView.region.span.latitudeDelta * 1, createMapView.region.span.longitudeDelta * 1); //设置地图范围
    [createMapView setRegion:MKCoordinateRegionMake(center, span) animated:YES];
    
    [self addAnnotation];
    getGeolocationArray = [NSArray arrayWithObjects:[NSNumber numberWithFloat:latitude], [NSNumber numberWithFloat:longitude],nil];
}

- (void)singlePress:(UITapGestureRecognizer *)gestureRecognizer {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChooseLocationView_touchMap"];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:createMapView]; //坐标转换
    touchMapCoordinate = [createMapView convertPoint:touchPoint toCoordinateFromView:createMapView];
    getGeolocationArray = [NSArray arrayWithObjects:[NSNumber numberWithFloat:touchMapCoordinate.latitude], [NSNumber numberWithFloat:touchMapCoordinate.longitude],nil]; //获取经纬度
    
    [self addAnnotation];
}

- (void) addAnnotation {
    
    if (pointAnnotation != nil) { //添加大头针
        [createMapView removeAnnotation:pointAnnotation];
    }
    
    pointAnnotation = [[MKPointAnnotation alloc] init];
    pointAnnotation.coordinate = touchMapCoordinate;
    [createMapView addAnnotation:pointAnnotation];
    
}

//- (void) getLocationString {
//    //解析地址
//}

#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, self.frame.size.width, self.frame.size.height)];
        _contentView.backgroundColor = BGBlackColor;
        _contentView.layer.masksToBounds = YES;
        
        //创建地图
        createMapView = [[MKMapView alloc] initWithFrame:_contentView.bounds];
        createMapView.mapType = MKMapTypeStandard; //地图类型
        createMapView.scrollEnabled = YES; //滑动
        createMapView.rotateEnabled = NO; //旋转
        createMapView.zoomEnabled = YES; //放大/缩小

        [_contentView addSubview:createMapView];
        
        UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singlePress:)];
        singleRecognizer.numberOfTouchesRequired  =1;
        [createMapView addGestureRecognizer:singleRecognizer];
        
        
        UIButton *closeBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(15), ZoomSize(15), ZoomSize(30), ZoomSize(30))];
        [_contentView addSubview:closeBt];
        [closeBt addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [closeBt setImage:[UIImage imageNamed:@"discover_map_close"] forState:UIControlStateNormal];
        
//        UIButton *doneBt = [[UIButton alloc] initWithFrame:CGRectMake( Screen_Width - ZoomSize(85), ZoomSize(15), ZoomSize(70), ZoomSize(30))];
//        [_contentView addSubview:doneBt];
//        doneBt.layer.cornerRadius = 5.0;
//        [doneBt setTitle:@"Done" forState:UIControlStateNormal];
//        [doneBt setTitleColor:WhiteColor forState:UIControlStateNormal];
//        doneBt.backgroundColor = ThemeYellow;
//        doneBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
//        [doneBt addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        
//        UIButton *getPlaceBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), self.frame.size.height - ZoomSize(74), self.frame.size.width - ZoomSize(72),  ZoomSize(48)) ];
//        [_contentView addSubview:getPlaceBt];
//        getPlaceBt.layer.cornerRadius = ZoomSize(24);
//        [getPlaceBt setTitle:@"New Place" forState:UIControlStateNormal];
//        [getPlaceBt setTitleColor:WhiteColor forState:UIControlStateNormal];
//        getPlaceBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
//        getPlaceBt.backgroundColor = ThemeYellow;
//        [getPlaceBt addTarget:self action:@selector(getPlaceAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton *doneBt = [[UIButton alloc] initWithFrame:CGRectMake( Screen_Width - ZoomSize(115), ZoomSize(15), ZoomSize(100), ZoomSize(30))];
        [_contentView addSubview:doneBt];
        doneBt.layer.cornerRadius = 5.0;
        [doneBt setTitle:@"new place" forState:UIControlStateNormal];
        [doneBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        doneBt.backgroundColor = ThemeYellow;
        doneBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
        [doneBt addTarget:self action:@selector(getPlaceAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton *getPlaceBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), self.frame.size.height - ZoomSize(74), self.frame.size.width - ZoomSize(72),  ZoomSize(48)) ];
        [_contentView addSubview:getPlaceBt];
        getPlaceBt.layer.cornerRadius = ZoomSize(24);
        [getPlaceBt setTitle:@"Start Search" forState:UIControlStateNormal];
        [getPlaceBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        getPlaceBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        getPlaceBt.backgroundColor = ThemeYellow;
        [getPlaceBt addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _contentView;
}

@end

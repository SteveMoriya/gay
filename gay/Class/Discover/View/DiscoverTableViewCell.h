//
//  DiscoverTableViewCell.h
//  gay
//
//  Created by steve on 2020/10/27.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DiscoverTableViewCell : UITableViewCell

@property (nonatomic, strong) UserProfileDataModel *profileDataModel;

@property (nonatomic, copy) ClickBlock blockBlock;

@property (nonatomic, copy) CallBack clickIndexBlock;

@end

NS_ASSUME_NONNULL_END

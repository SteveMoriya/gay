//
//  DiscoverTableViewCell.m
//  gay
//
//  Created by steve on 2020/10/27.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "DiscoverTableViewCell.h"
#import "PhotoBannerView.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface DiscoverTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *imageContentView;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UIButton *reportBt;

@end

@implementation DiscoverTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)reportAction:(id)sender {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"DiscoverCell_blockReport"];
    
    NSLog(@"reportAction");
    
    [DataTool reportBlockUserName:_profileDataModel.nickName andBlockUserId:_profileDataModel.userId reportCallback:^(id  _Nullable obj) {
        NSLog(@"reportSuccess");
    } blockCallback:^(id  _Nullable obj) {
        if (self.blockBlock) {
            self.blockBlock();
        }
    }];
    
}

- (IBAction)videoAction:(id)sender {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"DiscoverCell_video"];
    
    if ( ![[UserInfo shareInstant] checkUserIsVip]) {
        [DataTool showBuyVipView];
        return;
    }
    
    [[UserInfo shareInstant] showCallOutView:self->_profileDataModel];
    
}

- (IBAction)messageAction:(id)sender {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"DiscoverCell_message"];
    
    [DataTool showChatViewControllerWithType:TIM_C2C receiver:_profileDataModel.userId receiverProfileModel:_profileDataModel];
}


- (void)setProfileDataModel:(UserProfileDataModel *)profileDataModel {
    _profileDataModel = profileDataModel;
    
    NSMutableArray *photoArray = [NSMutableArray arrayWithObjects:_profileDataModel.headImgUrl, nil];
    
    NSString *ageString = @"";
    if (![DataTool isEmptyString:_profileDataModel.age]) {
        ageString = [NSString stringWithFormat:@", %@", _profileDataModel.age];
    }
    NSString *languageString = @"";
    if (![DataTool isEmptyString:_profileDataModel.languages]) {
        languageString = [NSString stringWithFormat:@", %@", _profileDataModel.languages];
    }
    
    NSString *identityString = @"";
    if (![DataTool isEmptyString:_profileDataModel.identity]) {
        identityString = [NSString stringWithFormat:@"%@ ", _profileDataModel.identity];
    }
    NSString *roleString = @"";
    if (![DataTool isEmptyString:_profileDataModel.role]) {
        roleString = [NSString stringWithFormat:@"/ %@ ", _profileDataModel.role];
    }
    
    NSString *addressString = @"";
    if (![DataTool isEmptyString:_profileDataModel.address]) {
        addressString = [NSString stringWithFormat:@"\n%@ ", _profileDataModel.address];
    }
    
    int finishPercent = 0;
    if (![DataTool isEmptyString:_profileDataModel.intro]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.identity]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.role]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.age]) {
        finishPercent = finishPercent + 10;
    }
    if (![_profileDataModel.height isEqualToString:@"0"]) {
        finishPercent = finishPercent + 10;
    }
    
    if (![DataTool isEmptyString:_profileDataModel.bodyType]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.languages]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.ethnicity]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.lookFor]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:_profileDataModel.address]) {
        finishPercent = finishPercent + 10;
    }
    
    _nameLb.text =  [NSString stringWithFormat:@"%@%@%@\n%@%@%@%@",_profileDataModel.nickName, ageString, languageString, identityString, roleString, [NSString stringWithFormat:@"/ %d %%",finishPercent], addressString];
        
    for (UIView *subView in _imageContentView.subviews) {
        [subView removeFromSuperview];
    }
    PhotoBannerView *photoBannerView = [[PhotoBannerView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 30,  Screen_Width * (460.0/375.0) ) photoArray:photoArray andPageIndex:0];
    
    __weak __typeof (self) weakSelf = self;
    
    photoBannerView.clickIndexBlock = ^(id  _Nullable obj) {
        int clickIndex = [obj intValue];
        __strong __typeof(self) strongSelf = weakSelf;
        if (strongSelf->_clickIndexBlock) {
            
            [[NetTool shareInstance] sendFireBaseWithEventName:@"DiscoverCell_openUserProfile"];
            strongSelf->_clickIndexBlock([NSNumber numberWithInt:clickIndex]);
        }
    };
    
    [_imageContentView addSubview:photoBannerView];
    
}

@end

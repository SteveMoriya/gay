//
//  ChooseFromMapView.h
//  gay
//
//  Created by steve on 2020/11/12.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChooseFromMapView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

- (void)show;

- (void)dismiss;

@property (nonatomic, copy) CallBack getLocationBlock;

@end

NS_ASSUME_NONNULL_END

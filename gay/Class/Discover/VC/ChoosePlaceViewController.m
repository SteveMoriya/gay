//
//  ChoosePlaceViewController.m
//  gay
//
//  Created by steve on 2020/11/12.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ChoosePlaceViewController.h"

@interface ChoosePlaceViewController ()

@end

@implementation ChoosePlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = BGBlackColor;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

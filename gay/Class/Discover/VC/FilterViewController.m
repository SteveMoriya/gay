//
//  FilterViewController.m
//  gay
//
//  Created by steve on 2020/11/4.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "FilterViewController.h"
#import "TTRangeSlider.h"
#import "TagListView.h"
#import "TagView.h"

@interface FilterViewController ()<TTRangeSliderDelegate>{
    
    UISwitch *onlineSwitch;
    
    UILabel *distanceInfoLb;
    UILabel *heightInfoLb;
    UILabel *ageInfoLb;
    
    TagListView *bodyTypeTagListView;
    TagListView *roleTagListView;
    TagListView *identityTagListView;
    
    NSString *nolineString;
    
    NSString *distanceRangeString;
    NSString *startHeightString;
    NSString *endHeightString;
    
    NSString *bodyTypeString;
    NSString *roleString;
    NSString *identityString;
    
    TTRangeSlider *distanceSlider;
    TTRangeSlider *heightSlider;
    TTRangeSlider *ageSlider;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation FilterViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    nolineString = @"";
    
    distanceRangeString = @"";
    startHeightString = @"";
    endHeightString = @"";
    
    bodyTypeString = @"";
    roleString = @"";
    identityString = @"";
    
    for (NSString *textString in BodyTypeArray) {
        [[bodyTypeTagListView addTag:textString] setOnTap:^(TagView *tagView) {
            if ( ![[UserInfo shareInstant] checkUserIsVip] ) {
                [DataTool showBuyVipView];
                return;
            }
            
            [self setTagViewStyle:self->bodyTypeTagListView];
            tagView.borderWidth = 0;
            tagView.textColor = UIColor.whiteColor;
            tagView.backgroundColor = ThemeYellow;
            self->bodyTypeString = tagView.titleLabel.text;
        }];
    }
    
    for (NSString *textString in RoleTypeArray) {
        [[roleTagListView addTag:textString] setOnTap:^(TagView *tagView) {
            
            if ( ![[UserInfo shareInstant] checkUserIsVip] ) {
                [DataTool showBuyVipView];
                return;
            }
            
            [self setTagViewStyle:self->roleTagListView];
            tagView.borderWidth = 0;
            tagView.textColor = UIColor.whiteColor;
            tagView.backgroundColor = ThemeYellow;
            
            self->roleString = tagView.titleLabel.text;
        }];
    }
    
    for (NSString *textString in IdentityArray) {
        [[identityTagListView addTag:textString] setOnTap:^(TagView *tagView) {
            [self setTagViewStyle:self->identityTagListView];
            tagView.borderWidth = 0;
            tagView.textColor = UIColor.whiteColor;
            tagView.backgroundColor = ThemeYellow;
            
            self->identityString = tagView.titleLabel.text;
        }];
    }
    
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void) switchFunctionAction {
//
////    if ( ![[UserInfo shareInstant] checkUserIsVip] ) {
////        [DataTool showBuyVipView];
////        onlineSwitch.on = NO;
////        nolineString = @"";
////    }
//
//    if (onlineSwitch.isOn == YES) {
//        NSLog(@"switchFunctionAction yes");
//        nolineString = @"noline";
//    } else {
//        NSLog(@"switchFunctionAction no");
//        nolineString = @"";
//    }
//}

- (void) sliderChangeAction :(TTRangeSlider *) slider {
    if (slider == distanceSlider) {
        if ([[UserInfo shareInstant] checkUserIsVip] ) {
            distanceInfoLb.text = [NSString stringWithFormat:@"0~%.0fkm+", distanceSlider.selectedMaximum];
            distanceRangeString = [NSString stringWithFormat:@"%.0f", distanceSlider.selectedMaximum];
        }
        
    } else if (slider == heightSlider) {
        if ( [[UserInfo shareInstant] checkUserIsVip] ) {
            heightInfoLb.text = [NSString stringWithFormat:@"%.0fcm~%.0fcm", heightSlider.selectedMinimum + 140, heightSlider.selectedMaximum + 140];
            startHeightString = [NSString stringWithFormat:@"%.0f", heightSlider.selectedMinimum + 140];
            endHeightString = [NSString stringWithFormat:@"%.0f", heightSlider.selectedMaximum + 140];
        }
        
    } else if (slider == ageSlider) {
        ageInfoLb.text = [NSString stringWithFormat:@"%.0f~%.0f", ageSlider.selectedMinimum, ageSlider.selectedMaximum];
    }
}

- (void) setTagViewStyle:(TagListView *)tagListView {
    
    for(TagView *tagView in [tagListView tagViews]) {
        tagView.borderWidth = 1;
        tagView.textColor = RGB(214, 215, 215);
        tagView.backgroundColor = [UIColor clearColor];
    }
}

- (void) doneAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Filter_done"];
    
    NSDictionary *filterParamsDic = @{@"online":nolineString,
                                      @"distanceRange":distanceRangeString,
                                      @"startHeight":startHeightString,
                                      @"endHeight":endHeightString,
                                      @"bodyTypes":bodyTypeString,
                                      @"roles":roleString,
                                      @"identities":identityString,
                                      @"startAge":[NSString stringWithFormat:@"%.0f", ageSlider.selectedMinimum],
                                      @"endAge":[NSString stringWithFormat:@"%.0f", ageSlider.selectedMaximum]};
    
    if (_getFilterBlock) {
        _getFilterBlock(filterParamsDic);
    }
    
    [self backAction];
}

- (void)drawDashLine:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:lineView.bounds];
    [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame))];
    
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    [shapeLayer setStrokeColor:lineColor.CGColor];//  设置虚线颜色为
    [shapeLayer setLineWidth:CGRectGetHeight(lineView.frame)]; //  设置虚线宽度
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]]; //  设置线宽，线间距
    CGMutablePathRef path = CGPathCreateMutable(); //  设置路径
    CGPathMoveToPoint(path, NULL, 0, 0);
    CGPathAddLineToPoint(path, NULL, CGRectGetWidth(lineView.frame), 0);
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [lineView.layer addSublayer:shapeLayer]; //  把绘制好的虚线添加上来
 }

- (void)didStartTouchesInRangeSlider:(TTRangeSlider *)sender {
    if (sender == distanceSlider) {
        if ( ![[UserInfo shareInstant] checkUserIsVip] ) {
            [DataTool showBuyVipView];
        }
    } else if (sender == heightSlider) {
        if ( ![[UserInfo shareInstant] checkUserIsVip] ) {
            [DataTool showBuyVipView];
        }
    }
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navBgView.backgroundColor = RGB(38, 44, 45);
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(8), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_close_clean"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(175))/2.0, StatusBarHeight, ZoomSize(175), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Filter";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
        UIButton *doneBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(38), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:doneBt];
        [doneBt setImage:[UIImage imageNamed:@"discover_filter_confirm"] forState:UIControlStateNormal];
        [doneBt addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = RGB(38, 44, 45);
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, ZoomSize(850));
        
#pragma mark - recent, distance, height
        
        UILabel *vipTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(25), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:vipTipLb];
        vipTipLb.text = @"VIP Only";
        vipTipLb.textColor = WhiteColor;
        vipTipLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
//        UILabel *onlineTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(70), ZoomSize(200), ZoomSize(25))];
//        [_contentScrollView addSubview:onlineTipLb];
//        onlineTipLb.text = @"Recent Online";
//        onlineTipLb.textColor = WhiteColor;
//        onlineTipLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
//
//        onlineSwitch = [[UISwitch alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(15) - 57, ZoomSize(83) - 16, 57, 32 )];
//        onlineSwitch.onTintColor = RGB(250, 180, 22);
//        [onlineSwitch addTarget:self action:@selector(switchFunctionAction) forControlEvents:UIControlEventValueChanged];
//        onlineSwitch.on = NO;
//        [_contentScrollView addSubview:onlineSwitch];
        
        UILabel *distanceTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(100), ZoomSize(80), ZoomSize(20))];
        [_contentScrollView addSubview:distanceTipLb];
        distanceTipLb.text = @"Distance";
        distanceTipLb.textColor = RGB(202, 198, 198);
        distanceTipLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
        
        distanceInfoLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(215), ZoomSize(90), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:distanceInfoLb];
        distanceInfoLb.text = @"0~100km+";
        distanceInfoLb.textAlignment = NSTextAlignmentRight;
        distanceInfoLb.textColor = UIColor.whiteColor;
        distanceInfoLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        
        distanceSlider = [[TTRangeSlider alloc] initWithFrame:CGRectMake( ZoomSize(10), ZoomSize(130), SCREEN_Width - ZoomSize(20), ZoomSize(40))];
        [_contentScrollView addSubview:distanceSlider];
        distanceSlider.lineHeight = ZoomSize(2);
        distanceSlider.minValue = 10;
        distanceSlider.maxValue = 500;
        distanceSlider.selectedMaximum = 100;
        distanceSlider.enableStep = YES;
        distanceSlider.step = 10;
        distanceSlider.disableRange = YES;
        distanceSlider.handleDiameter = 20;
        distanceSlider.selectedHandleDiameterMultiplier = 1.0;
        distanceSlider.hideLabels = YES;
        distanceSlider.handleColor = ThemeYellow;
        [distanceSlider setTintColorBetweenHandles:ThemeYellow];
        [distanceSlider setTintColor:RGB(83, 93, 94)];
        [distanceSlider addTarget:self action:@selector(sliderChangeAction:) forControlEvents:UIControlEventValueChanged];
        distanceSlider.delegate = self;
        
        
        UILabel *heightTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(210), ZoomSize(80), ZoomSize(20))];
        [_contentScrollView addSubview:heightTipLb];
        heightTipLb.text = @"Height";
        heightTipLb.textColor = RGB(202, 198, 198);
        heightTipLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
        
        heightInfoLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(215), ZoomSize(200), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:heightInfoLb];
        heightInfoLb.text = @"160cm~180cm";
        heightInfoLb.textAlignment = NSTextAlignmentRight;
        heightInfoLb.textColor = UIColor.whiteColor;
        heightInfoLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        
        
        heightSlider = [[TTRangeSlider alloc] initWithFrame:CGRectMake( ZoomSize(10), ZoomSize(240), SCREEN_Width - ZoomSize(20), ZoomSize(40))];
        [_contentScrollView addSubview:heightSlider];
        heightSlider.lineHeight = ZoomSize(2);
        heightSlider.minValue = 0;
        heightSlider.maxValue = 80;
        heightSlider.selectedMinimum = 20;
        heightSlider.selectedMaximum = 40;
        heightSlider.enableStep = YES;
        heightSlider.step = 1;
        heightSlider.handleDiameter = ZoomSize(20);
        heightSlider.selectedHandleDiameterMultiplier = 1.0;
        heightSlider.hideLabels = YES;
        [heightSlider setTintColorBetweenHandles:ThemeYellow];
        [heightSlider setTintColor:RGB(83, 93, 94)];
        heightSlider.handleColor = ThemeYellow;
        [heightSlider addTarget:self action:@selector(sliderChangeAction:) forControlEvents:UIControlEventValueChanged];
        heightSlider.delegate = self;
        
#pragma mark - BodyType
        
        UILabel *bodyTypeTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(305), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:bodyTypeTipLb];
        bodyTypeTipLb.text = @"Body Type";
        bodyTypeTipLb.textColor = WhiteColor;
        bodyTypeTipLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        
        bodyTypeTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(350), Screen_Width - ZoomSize(30), ZoomSize(80))];
        [_contentScrollView addSubview:bodyTypeTagListView];
        bodyTypeTagListView.cornerRadius = ZoomSize(6);
        bodyTypeTagListView.borderWidth = ZoomSize(1);
        bodyTypeTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
        bodyTypeTagListView.paddingX = ZoomSize(8);
        bodyTypeTagListView.paddingY = ZoomSize(6);
        bodyTypeTagListView.marginX = ZoomSize(8);
        bodyTypeTagListView.marginY = ZoomSize(6);
        bodyTypeTagListView.tagViewHeight = ZoomSize(30);
        bodyTypeTagListView.textColor = RGB(214, 215, 215);
        bodyTypeTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        
#pragma mark - Role
        UILabel *roleTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(450), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:roleTipLb];
        roleTipLb.text = @"Role";
        roleTipLb.textColor = WhiteColor;
        roleTipLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        
        roleTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(490), Screen_Width - ZoomSize(30), ZoomSize(80))];
        [_contentScrollView addSubview:roleTagListView];
        roleTagListView.cornerRadius = ZoomSize(6);
        roleTagListView.borderWidth = ZoomSize(1);
        roleTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
        roleTagListView.paddingX = ZoomSize(8);
        roleTagListView.paddingY = ZoomSize(6);
        roleTagListView.marginX = ZoomSize(8);
        roleTagListView.marginY = ZoomSize(6);
        roleTagListView.tagViewHeight = ZoomSize(30);
        roleTagListView.textColor = RGB(214, 215, 215);
        roleTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        
        UIView *crossDashLineView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(590), SCREEN_Width, 1)];
        [_contentScrollView addSubview:crossDashLineView];
        [self drawDashLine:crossDashLineView lineLength:10 lineSpacing:5 lineColor:RGB(57, 66, 67)];
        

#pragma mark - idntity age
        
        UILabel *basicTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(610), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:basicTipLb];
        basicTipLb.text = @"Basic";
        basicTipLb.textColor = WhiteColor;
        basicTipLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
        UILabel *identityTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(650), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:identityTipLb];
        identityTipLb.text = @"Identity";
        identityTipLb.textColor = WhiteColor;
        identityTipLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        
        identityTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(690), ZoomSize(345), ZoomSize(40))];
        [_contentScrollView addSubview:identityTagListView];
        
        identityTagListView.cornerRadius = ZoomSize(6);
        identityTagListView.borderWidth = ZoomSize(1);
        identityTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
        identityTagListView.paddingX = ZoomSize(8);
        identityTagListView.paddingY = ZoomSize(6);
        identityTagListView.marginX = ZoomSize(8);
        identityTagListView.marginY = ZoomSize(6);
        identityTagListView.tagViewHeight = ZoomSize(30);
        identityTagListView.textColor = RGB(214, 215, 215);
        identityTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        
        UILabel *ageTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(750), ZoomSize(80), ZoomSize(20))];
        [_contentScrollView addSubview:ageTipLb];
        ageTipLb.text = @"Age";
        ageTipLb.textColor = RGB(202, 198, 198);
        ageTipLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
        
        ageInfoLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(215), ZoomSize(740), ZoomSize(200), ZoomSize(30))];
        [_contentScrollView addSubview:ageInfoLb];
        ageInfoLb.text = @"18~30";
        ageInfoLb.textAlignment = NSTextAlignmentRight;
        ageInfoLb.textColor = UIColor.whiteColor;
        ageInfoLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        
        ageSlider = [[TTRangeSlider alloc] initWithFrame:CGRectMake( ZoomSize(10), ZoomSize(780), SCREEN_Width - ZoomSize(20), ZoomSize(40))];
        [_contentScrollView addSubview:ageSlider];
        ageSlider.lineHeight = ZoomSize(2);
        ageSlider.minValue = 18;
        ageSlider.maxValue = 80;
        ageSlider.selectedMinimum = 18;
        ageSlider.selectedMaximum = 30;
        ageSlider.enableStep = YES;
        ageSlider.step = 1;
        ageSlider.handleDiameter = ZoomSize(20);
        ageSlider.selectedHandleDiameterMultiplier = 1.0;
        ageSlider.hideLabels = YES;
        [ageSlider setTintColorBetweenHandles:ThemeYellow];
        [ageSlider setTintColor:RGB(83, 93, 94)];
        ageSlider.handleColor = ThemeYellow;
        [ageSlider addTarget:self action:@selector(sliderChangeAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _contentScrollView;
}

@end


//
//  SearchViewController.m
//  gay
//
//  Created by steve on 2020/9/21.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.navView];
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) nameTFChange :(UITextField *) textField {
    if (textField.text.length > 20) {
        textField.text = [textField.text substringToIndex:20];
    }
}

- (void) getSearchData {
    
    
//    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:1], @"nickName":_searchTF.text};
    
    //    @"startAge":@"20", @"endAge":@"30"
    //    @"startWeight":@"70", @"endWeight":@"80"
//    @"startHeight":@"170", @"endHeight":@"180"
//    @"bodyType":@"1"
    
    //    @"ethnicity":@"1"
    //    @"style":@"1"
    //    @"role":@"1"

    //    @"state":@"1"
    //    @"HIV":@"1"
//    @"UserSortEnum":@"CREATE_TIME" 数据排序方式，默认为 CREATE_TIME
    
    
    
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:1], @"UserSortEnum":@"CREATE_TIME" };
    
    [[NetTool shareInstance] startRequest:API_user_pageList method:GetMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        
        
    }];
    
    
}



#pragma mark - lazy

#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(0), StatusBarHeight, ZoomSize(40), ZoomSize(40))];
        [_navView addSubview:backBt];
//        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(45), StatusBarHeight , SCREEN_Width - ZoomSize(60), ZoomSize(40))];
        [_navView addSubview:searchView];
        searchView.layer.cornerRadius = ZoomSize(20);
        searchView.layer.masksToBounds = YES;
        searchView.layer.borderColor = RGB(99, 78, 122).CGColor;
        searchView.layer.borderWidth = ZoomSize(2);
        
        UIImageView *searchTipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(13), ZoomSize(10), ZoomSize(21), ZoomSize(20))];
//        searchTipIV.image = GetImageByName(@"messages_search");
        searchTipIV.contentMode = UIViewContentModeCenter;
        [searchView addSubview:searchTipIV];
        
        _searchTF = [[UITextField alloc] initWithFrame:CGRectMake(ZoomSize(45), 0 , SCREEN_Width - ZoomSize(185), ZoomSize(40))];
        [searchView addSubview:_searchTF];
        _searchTF.font = [UIFont systemFontOfSize:ZoomSize(18)];
//        _searchTF.tintColor = LightGrayColor;
//        _searchTF.textColor = LightGrayColor;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"Search Name Or ID" attributes: @{NSForegroundColorAttributeName: RGB(99, 78, 122), NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(18)] }];
        _searchTF.attributedPlaceholder = attrString;
         [_searchTF addTarget:self action:@selector(nameTFChange:) forControlEvents:UIControlEventEditingChanged];
        
        UIButton *searchBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(140), 0, ZoomSize(80), ZoomSize(40))];
        [searchBt setTitle:@"Search" forState:UIControlStateNormal];
        [searchBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        searchBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
        [searchBt addTarget:self action:@selector(getSearchData) forControlEvents:UIControlEventTouchUpInside];
        [searchView addSubview:searchBt];
        
    }
    return _navView;
}



@end

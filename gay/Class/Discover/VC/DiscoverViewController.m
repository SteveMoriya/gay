//
//  DiscoverViewController.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "DiscoverViewController.h"
#import "FilterViewController.h"
#import "ChooseFromMapView.h"
#import "ChatRoomViewController.h"

#import "DiscoverTableViewCell.h"
#import "TipVipTableViewCell.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

#import "MJRefresh.h"
#import "ActivitesDataModel.h"

static NSString * const reuseIdentifier = @"DiscoverTableViewCell";
static NSString * const tipVipIdentifier = @"TipVipTableViewCell";

@interface DiscoverViewController()<UITableViewDelegate, UITableViewDataSource> {
    ChooseFromMapView * chooseView;
    int pageIndex;
    
    NSString *latitude;
    NSString *longtitude;
    NSString *distanceRange;
    
    NSString *startHeight;
    NSString *endHeight;
    NSString *startAge;
    NSString *endAge;
    
    NSString *bodyTypes;
    NSString *roles;
    NSString *identities;
    
    NSDictionary *searchParamsDic;
    NSString *urlString;
    
    UIButton *chatRoomBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation DiscoverViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ChatRoom]) { //使用存储的关键字信息
        chatRoomBt.hidden = YES;
    } else {
        chatRoomBt.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.tableView];
    
    chatRoomBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(166), Screen_Height - TabBarHeight - ZoomSize(80), ZoomSize(166), ZoomSize(58))];
    [self.view addSubview:chatRoomBt];
    [chatRoomBt addTarget:self action:@selector(chatRoomAction) forControlEvents:UIControlEventTouchUpInside];
    [chatRoomBt setBackgroundImage:[UIImage imageNamed:@"message_group_chat"] forState:UIControlStateNormal];
    chatRoomBt.hidden = YES;
    
    UILabel *chatRoomTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(16), ZoomSize(100), ZoomSize(20))];
    [chatRoomBt addSubview:chatRoomTitleLb];
    chatRoomTitleLb.textColor = WhiteColor;
    chatRoomTitleLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
    chatRoomTitleLb.text = @"ChatRoom";
    
    UIImageView *chatRoomUserIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(120), ZoomSize(12), ZoomSize(30), ZoomSize(30))];
    [chatRoomBt addSubview:chatRoomUserIV];
    chatRoomUserIV.image = [UIImage imageNamed:@"message_fuzzy_portrait"];
    chatRoomUserIV.layer.cornerRadius = ZoomSize(15);
    chatRoomUserIV.layer.masksToBounds = YES;
    chatRoomUserIV.layer.borderWidth = ZoomSize(1);
    chatRoomUserIV.layer.borderColor = WhiteColor.CGColor;
}

- (void)setData {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blockUser:) name:blockUserNotification object:nil];
    
    chooseView = [[ChooseFromMapView alloc] initWithFrame:CGRectMake(0, NavigationHeight, Screen_Width, Screen_Height - NavigationHeight - TabBarHeight)];
    
    __weak __typeof (self) weakSelf = self;
    chooseView.getLocationBlock = ^(id  _Nullable obj) {
        if (obj != nil ) {
            NSArray *getLocationArray = obj;
            __strong __typeof(self) strongSelf = weakSelf;
            
            [strongSelf setDefaultData];
            
            strongSelf->latitude = getLocationArray.firstObject;
            strongSelf->longtitude = getLocationArray.lastObject;
            strongSelf->distanceRange = @"500";
            [strongSelf requestData];
        }
    };
    
    self.array = [NSMutableArray array];
    [self setDefaultData];
    [self getHeaderData];
}

#pragma function

- (void) chatRoomAction {
    NSLog(@"chatRoomAction");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Message_chatRoom"];
    
//    [[V2TIMManager sharedInstance] createGroup:@"Meeting" groupID:ChatRoomId groupName:@"chatroom" succ:^(NSString *groupID) {
//    } fail:^(int code, NSString *desc) {
//    }];
    
    [[V2TIMManager sharedInstance] joinGroup:ChatRoomId msg:@"hi" succ:^{
        TIMConversation *conv = [[TIMManager sharedInstance] getConversation:2 receiver:ChatRoomId];
        ChatRoomViewController *vc = [[ChatRoomViewController alloc] initWithConversation:conv];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    } fail:^(int code, NSString *desc) {
        TIMConversation *conv = [[TIMManager sharedInstance] getConversation:2 receiver:ChatRoomId];
        ChatRoomViewController *vc = [[ChatRoomViewController alloc] initWithConversation:conv];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];
    
}


- (void) blockUser:(NSNotification *)notification{
    NSString *userIdString = notification.object;
    NSUInteger blockIndex = 0;
    BOOL needDelete = NO;
    
    for (UserProfileDataModel *profileModel in self.array) { //将系统的数据放在第一个
        if ([profileModel.userId isEqualToString:userIdString]) {
            blockIndex = [self.array indexOfObject:profileModel];
            needDelete = YES;
        }
    }
    
    if (needDelete) {
        [self.array removeObjectAtIndex:blockIndex];
        [self.tableView reloadData];
    }
}

- (void)getHeaderData {
    pageIndex = 1;
    [self requestData];
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Discover_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Discover_down"];
}

- (void) setDefaultData {
    self->pageIndex = 1;
    
    latitude = @"";
    longtitude = @"";
    distanceRange = @"";
    
    startHeight = @"";
    endHeight = @"";
    startAge = @"";
    endAge = @"";
    
    bodyTypes = @"";
    roles = @"";
    identities = @"";
    urlString = API_user_pageList;
}

- (void)requestData {
    
    self->searchParamsDic = @{@"pageNum": [NSNumber numberWithInt:self->pageIndex], @"pageSize": [NSNumber numberWithInt:20], @"latitude":latitude, @"longitude":longtitude,  @"distanceRange":distanceRange, @"startHeight":startHeight, @"endHeight":endHeight, @"startAge":startAge, @"endAge":endAge, @"bodyTypes":bodyTypes, @"roles":roles,  @"identities":identities };
    
    [[NetTool shareInstance] startRequest:urlString method:GetMethod params:searchParamsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
        }
        self->pageIndex ++;
        
        NSArray *userArray = [UserProfileDataModel mj_objectArrayWithKeyValuesArray:obj];
        [self.array addObjectsFromArray:userArray];
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];
}

- (void)filterAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"DisCover_filter"];
    
    [chooseView dismiss];
    
    FilterViewController *vc = [[FilterViewController alloc] init];
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
    vc.getFilterBlock = ^(id  _Nullable obj) {
        
        NSDictionary *filterDic = obj;
        [self setDefaultData];
        
        self->distanceRange = filterDic[@"distanceRange"];
        
        self->startHeight = filterDic[@"startHeight"];
        self->endHeight = filterDic[@"endHeight"];
        
        self->bodyTypes = filterDic[@"bodyTypes"];
        self->roles = filterDic[@"roles"];
        
        self->identities = filterDic[@"identities"];
        self->startAge = filterDic[@"startAge"];
        self->endAge = filterDic[@"endAge"];
        
        [self requestData];
    };
}

- (void)placeAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Discover_place"];
    [chooseView show];
}

- (void) onlineAction:(UIButton *)sender {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Discover_online"];
    
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self setDefaultData];
        self->urlString = API_user_onLineList;
    } else {
        [self setDefaultData];
//        self->urlString = API_user_pageList;
    }
    [self requestData];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return Screen_Width * (460.0/375.0);
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DiscoverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(cell == nil) {
        cell = [[DiscoverTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    UserProfileDataModel *profileModel = self.array[indexPath.row];
    cell.profileDataModel = profileModel;
    
    cell.blockBlock = ^{
        NSLog(@"blockBlock");
        [self.array removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
    };
    
    cell.clickIndexBlock = ^(id  _Nullable obj) {
        
        if ([DataTool checkUserIsShare:profileModel.userId]) {
            [DataTool openUserProfileView:profileModel andClickImgIndex:0 isFromShare:YES];
        } else {
            int clickImgIndex = [obj intValue];
            NSDictionary *userParams = @{@"id":profileModel.userId};
            [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
                NSDictionary *resultDic = obj;
                UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
                [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:clickImgIndex isFromShare:NO];
            }];
        }
        
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navBgView.backgroundColor = BGBlackColor;
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), StatusBarHeight, ZoomSize(175), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Nearby";
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(26) weight:UIFontWeightMedium];
        
        UIButton *onlineBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(175), StatusBarHeight , ZoomSize(65), ZoomSize(30))];
        [_navBgView addSubview:onlineBt];
        onlineBt.layer.cornerRadius = ZoomSize(8);
        onlineBt.layer.masksToBounds = YES;
        [onlineBt setTitle:@"Online" forState:UIControlStateNormal];
        onlineBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [onlineBt setTitleColor:ThemeYellow forState:UIControlStateNormal];
        [onlineBt setBackgroundImage:[DataTool imageWithColor:RGB(40, 46, 47)] forState:UIControlStateNormal];
        [onlineBt setTitleColor:WhiteColor forState:UIControlStateSelected];
        [onlineBt setBackgroundImage:[DataTool imageWithColor:ThemeYellow] forState:UIControlStateSelected];
        [onlineBt addTarget:self action:@selector(onlineAction:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *placeBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(90), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:placeBt];
        [placeBt setImage:[UIImage imageNamed:@"discover_button_plane"] forState:UIControlStateNormal];
        [placeBt addTarget:self action:@selector(placeAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *filterBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(45), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:filterBt];
        [filterBt setImage:[UIImage imageNamed:@"discover_button_filter"] forState:UIControlStateNormal];
        [filterBt addTarget:self action:@selector(filterAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:tipVipIdentifier bundle:nil] forCellReuseIdentifier:tipVipIdentifier];
        
        _tableView.backgroundColor = BGBlackColor;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self setDefaultData];
            [self getHeaderData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        ListPlaceHoderActionView *listPlaceHolderActionView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"No Data" tipsInfo:@"" btTitle:@"Search again"];
        listPlaceHolderActionView.actionBlock = ^{
            [self setDefaultData];
            [self getHeaderData];
        };
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = listPlaceHolderActionView;
    }
    return _tableView;
}

@end

//
//  OfficialMessageTableViewCell.m
//  Video
//
//  Created by steve on 2020/6/5.
//  Copyright © 2020 steve. All rights reserved.
//

#import "OfficialMessageTableViewCell.h"

@interface OfficialMessageTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLb;
@property (weak, nonatomic) IBOutlet UITextView *infoTV;

@end

@implementation OfficialMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _infoTV.textContainerInset = UIEdgeInsetsMake(ZoomSize(10), ZoomSize(10), ZoomSize(0), ZoomSize(10));
    
}

- (void)setMessage:(V2TIMMessage *)message {
    _message = message;
    _timeLb.text = [DataTool getTimTimeFromString:message.timestamp];
    
    
    NSData *jsonData = [message.textElem.text dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    
    
    if (dic != nil) {
        jsonData = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
        dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        _infoTV.text = dic[@"content"];
    } else {
        _infoTV.text = _message.textElem.text;
//        _infoTV.text = @"no message";
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end

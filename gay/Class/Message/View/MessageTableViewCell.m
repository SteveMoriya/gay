//
//  MessageTableViewCell.m
//  gay
//
//  Created by steve on 2020/10/28.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "MessageTableViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <MJExtension.h>

@interface MessageTableViewCell() {
}

@property (weak, nonatomic) IBOutlet UIImageView *headIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *textLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLb;
@property (weak, nonatomic) IBOutlet UILabel *numLb;
@property (weak, nonatomic) IBOutlet UIImageView *numBGIV;


@end

@implementation MessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setConv:(V2TIMConversation *)conv {
    _conv = conv;
    _nameLb.text = @"";
    _textLb.text = @"";
    _timeLb.text = @"";
    _numLb.text = @"";
    
    _nameLb.text = conv.showName;
    [_headIV sd_setImageWithURL:[NSURL URLWithString:conv.faceUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    
    if (conv.lastMessage.elemType == V2TIM_ELEM_TYPE_TEXT) {
        if ([conv.userID isEqualToString:OfficialUserID]) {
            
            NSData *jsonData = [conv.lastMessage.textElem.text dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
            
            if ([dic isKindOfClass:[NSNull class]] || dic == nil ) {
                if (![DataTool isEmptyString:conv.lastMessage.textElem.text]) {
                    _textLb.text = conv.lastMessage.textElem.text;
                }
            } else {
                jsonData = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                _textLb.text = dic[@"content"];
            }
            
            
        } else {
            _textLb.text = conv.lastMessage.textElem.text;
        }
        
    } else if (conv.lastMessage.elemType == V2TIM_ELEM_TYPE_CUSTOM ) {
        
        V2TIMCustomElem *dataElem = conv.lastMessage.customElem;
        NSDictionary *getDic = [NSJSONSerialization JSONObjectWithData:dataElem.data options:NSJSONReadingMutableContainers error:nil];
        
        //用于拨打电话的消息展示
        UserProfileDataModel *otherUserModel = [UserProfileDataModel mj_objectWithKeyValues:getDic]; //对方信息
        if (![DataTool isEmptyString:otherUserModel.messageText]) {
            _textLb.text = otherUserModel.messageText;
        } else {
            //用于显示打招呼内容
            if ([getDic[@"mold"] intValue] == 0) {
                _textLb.text =  @"[Photo]";
            }
        }

//       {"high":"798","wide":"600","mold":0,"content":"https://yt-bullet.oss-cn-shenzhen.aliyuncs.com/MncHA5_1595486088334.webp"}
        
        
    } else if (conv.lastMessage.elemType == V2TIM_ELEM_TYPE_IMAGE ) {
        _textLb.text = @"[Photo]";
    } else {
        _textLb.text = @"Media Info";
    }
    
    if (conv.lastMessage.timestamp != nil) {
        _timeLb.text = [DataTool getTimTimeFromString:conv.lastMessage.timestamp];
    }
    
    if (conv.unreadCount == 0) {
        _numBGIV.hidden = YES;
        _numLb.hidden = YES;
    } else {
        _numBGIV.hidden = NO;
        _numLb.hidden = NO;
        
        if (conv.unreadCount > 0 && conv.unreadCount <= 9) {
            _numBGIV.image = [UIImage imageNamed:@"messages_unread_1"];
            _numLb.text = [NSString stringWithFormat:@"%d", conv.unreadCount];
        } else if (conv.unreadCount > 9 && conv.unreadCount <= 99 ) {
            _numBGIV.image = [UIImage imageNamed:@"messages_unread_2"];
            _numLb.text = [NSString stringWithFormat:@"%d", conv.unreadCount];
        } else if (conv.unreadCount > 99) {
            _numBGIV.image = [UIImage imageNamed:@"messages_unread_2"];
            _numLb.text = @"+99";
        }
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end

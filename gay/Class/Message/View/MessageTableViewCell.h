//
//  MessageTableViewCell.h
//  gay
//
//  Created by steve on 2020/10/28.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTableViewCell : UITableViewCell

@property (nonatomic, strong) V2TIMConversation *conv;
@property (nonatomic, copy) ClickBlock actionBlock;

@end

NS_ASSUME_NONNULL_END

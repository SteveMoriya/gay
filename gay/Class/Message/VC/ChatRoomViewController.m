//
//  ChatRoomViewController.m
//  gay
//
//  Created by steve on 2020/11/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ChatRoomViewController.h"
#import "SelectPhotoManager.h"
#import "FTPopOverMenu.h"

@interface ChatRoomViewController () {
    UIButton *moreBt;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation ChatRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
    [TUIMessageCellLayout incommingMessageLayout].avatarSize = CGSizeMake(40, 40);
    [TUIMessageCellLayout outgoingMessageLayout].avatarSize = CGSizeMake(40, 40);
}

- (void) viewWillDisappear:(BOOL)animated {
    [TUIMessageCellLayout incommingMessageLayout].avatarSize = CGSizeMake(0, 0);
    [TUIMessageCellLayout outgoingMessageLayout].avatarSize = CGSizeMake(0, 0);
}

- (void)setupUI {
    [self.view addSubview:self.navView];
    self.inputController.inputBar.inputTextView.frame = CGRectMake(49, 8, Screen_Width - 60, 40);
    self.inputController.inputBar.videoButton.hidden = YES;
    
}

- (void)setdata {
    _photoManager = [[SelectPhotoManager alloc] init];
}

#pragma mark - Function
- (void)backAction {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [[V2TIMManager sharedInstance] quitGroup:ChatRoomId succ:^{
        NSLog(@"quite succ");
    } fail:^(int code, NSString *desc) {
        NSLog(@"quite fail");
    }];
}

- (void) moreAction {
//    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_reportBlock"];
//
//    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
//    } blockCallback:^(id  _Nullable obj) {
//        [self backAction];
//    }];
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.imageSize = CGSizeMake(30, 20);
    configuration.selectedCellBackgroundColor = RGB(85, 95, 98);
    configuration.textColor = WhiteColor;
    configuration.textFont = [UIFont systemFontOfSize:16];
    configuration.textAlignment = NSTextAlignmentLeft;
    
    configuration.separatorColor = RGB(104, 118, 122);
    configuration.backgroundColor = RGB(85, 95, 98);
    
    NSArray *titleArray = @[ @"Hide ChatRoom", @"Share ChatRoom"  ];
    NSArray *imgArray = @[[UIImage imageNamed:@"messages_video_open"], [UIImage imageNamed:@"tabbar_post_default"]];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:UserVideoCall]) {
        imgArray = @[[UIImage imageNamed:@"messages_video_close"], [UIImage imageNamed:@"tabbar_post_default"]];
    }

    [FTPopOverMenu showForSender:moreBt
                   withMenuArray:titleArray
                      imageArray:imgArray
                   configuration:configuration
                       doneBlock:^(NSInteger selectedIndex) {
        
        if (selectedIndex == 0) {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_SetCall"];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:ChatRoom]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ChatRoom];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:ChatRoom];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        } else if (selectedIndex == 1) {
            
            NSString *shareText = @"This is where i long to be, GFUN";
            NSURL *shareUrl = [NSURL URLWithString:@"https://itunes.apple.com/app/id1540050892"];
            NSArray *activityItems = @[shareText,shareUrl];
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            
            if (IS_IPad) {
                activityVC.popoverPresentationController.sourceView = self.view;
                activityVC.popoverPresentationController.sourceRect = self->moreBt.frame;
                activityVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp ;
            }
            [[DataTool getCurrentVC] presentViewController:activityVC animated:YES completion:nil];
            
        }
    } dismissBlock:^{
    }];
    
}


#pragma mark - inputController Delegate
- (void)inputControllerSelectPhotoBt:(TUIInputController *)inputController {
    NSLog(@"inputControllerSelectPhotoBt");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatRoom_SelectPhoto"];
    
    if ( ![[UserInfo shareInstant] checkUserIsVip]) {
        [DataTool showBuyVipView];
        return;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Photo" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    if (IS_IPad) {
        alertController = [UIAlertController alertControllerWithTitle:@"Select Photo" message:nil preferredStyle: UIAlertControllerStyleAlert];
    }
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Take a Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self takePictureForSend];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Choose from Library" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoForSend];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
    
}

- (void)inputController:(TUIInputController *)inputController didSendMessage:(TUIMessageCellData *)msg {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatRoom_sendMessage"];
    [self sendMessage:msg];
}

- (void)messageController:(TUIMessageController *)controller onSelectMessageAvatar:(TUIMessageCell *)cell {
    
    if ([DataTool isEmptyString:cell.messageData.identifier]) {
        return;
    }
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatRoom_openUserProfile"];
    
    if ([DataTool checkUserIsShare:cell.messageData.identifier]) {
        UserProfileDataModel *userProfileDataModel = [[UserProfileDataModel alloc]init];
        
        userProfileDataModel.userId = cell.messageData.identifier;
        userProfileDataModel.nickName = cell.messageData.name;
        userProfileDataModel.headImgUrl = [NSString stringWithFormat:@"%@", cell.messageData.avatarUrl];
        [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:YES];
    } else {
        NSDictionary *userParams = @{@"id":cell.messageData.identifier};
        [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
            NSDictionary *resultDic = obj;
            
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:NO];
        }];
    }
    
}


- (void)messageController:(TUIMessageController *)controller tapReportMenuInCell:(NSString *)msgId {
    
    [DataTool reportBlockUserName:@"The Selected User" andBlockUserId:msgId reportCallback:^(id  _Nullable obj) {
    } blockCallback:^(id  _Nullable obj) {
    }];
    
}


#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navView.backgroundColor = BGBlackColor;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight, Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navView addSubview:titleLb];
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(20)];
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.text = @"ChatRoom";
        
        moreBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(40), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
        [_navView addSubview:moreBt];
        [moreBt setImage:[UIImage imageNamed:@"messages_more"] forState:UIControlStateNormal];
        [moreBt addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navView;
}

@end

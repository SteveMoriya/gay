//
//  ChatViewController.h
//  gay
//
//  Created by steve on 2020/9/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "TUIChatController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatViewController : TUIChatController

@property (nonatomic, strong) UserProfileDataModel *profileModel;
@property (nonatomic, assign) TIMConversationType type;
@property (nonatomic, strong) NSString *receiverId;

@end

NS_ASSUME_NONNULL_END

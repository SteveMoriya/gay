//
//  OfficialChatListViewController.m
//  Video
//
//  Created by steve on 2020/5/29.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MJExtension.h"
#import "MJRefresh.h"
#import "OfficialChatListViewController.h"
#import "FeedBackViewController.h"
#import "UITableView+PlaceHolderView.h"

#import "V2TIMManager+Message.h"
#import "V2TIMManager+Conversation.h"

#import "OfficialMessageTableViewCell.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"OfficialMessageTableViewCell";

@interface OfficialChatListViewController ()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation OfficialChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = RGB(27, 31, 32);
    [self.view addSubview:self.navView];
    [self.view addSubview:self.tableView];
    
    UIButton *continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), SCREEN_Height - ZoomSize(60) - Bottom_SafeHeight, ZoomSize(303), ZoomSize(48)) ];
    [self.view addSubview:continueBt];
    continueBt.layer.cornerRadius = ZoomSize(24);
    [continueBt setTitle:@"Feedback" forState:UIControlStateNormal];
    [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    continueBt.backgroundColor = ThemeYellow;
    [continueBt addTarget:self action:@selector(feedbackAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setData {
    [self getOfficialMessage];
}

- (void) getOfficialMessage {
    
    self.array = [NSMutableArray array];
    [[V2TIMManager sharedInstance] getC2CHistoryMessageList:OfficialUserID count:20 lastMsg:nil succ:^(NSArray<V2TIMMessage *> *msgs) {
        if (msgs.count > 0) {
            [self.array setArray:msgs];
            [self.tableView reloadData];
        }
        [self.tableView.mj_header endRefreshing];
        
    } fail:^(int code, NSString *desc) {
        [self.tableView.mj_header endRefreshing];
    }];
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) feedbackAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"OfficialChat_feedback"];
    FeedbackViewController *vc = [[FeedbackViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    V2TIMMessage *getMessage = self.array[indexPath.row];
    NSData *jsonData = [getMessage.textElem.text dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    
    if (dic != nil) {
        jsonData = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
        dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        NSString *getTestString = dic[@"content"];
        
        CGFloat textHeight = [DataTool getTextHeightWithFont:[UIFont systemFontOfSize:16] getTextString: getTestString getFrameWidth:SCREEN_Width - ZoomSize(52)] + ZoomSize(80);
        
        return textHeight;
        
    } else {
        CGFloat textHeight = [DataTool getTextHeightWithFont:[UIFont systemFontOfSize:16] getTextString: getMessage.textElem.text getFrameWidth:SCREEN_Width - ZoomSize(52)] + ZoomSize(80);
        
        return textHeight;
        
    }
    
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OfficialMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];

    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(cell == nil) {
        cell = [[OfficialMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    V2TIMMessage *getMessage = self.array[indexPath.row];
    cell.message = getMessage;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
}

#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        _navView.backgroundColor = BGBlackColor;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight, ZoomSize(40), ZoomSize(40))];
        [_navView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(100), StatusBarHeight , ZoomSize(170), ZoomSize(40))];
        titleLb.text = @"Official";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [_navView addSubview:titleLb];
        
    }
    return _navView;
}


- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        _tableView.backgroundColor = BGBlackColor;
        _tableView.rowHeight = ZoomSize(140);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self setData];
        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
        
    }
    return _tableView;
}

@end

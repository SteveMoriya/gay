//
//  ChatViewController.m
//  gay
//
//  Created by steve on 2020/9/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ChatViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SelectPhotoManager.h"
#import "FTPopOverMenu.h"

#import "UserCallInView.h"
#import "UserCallOutView.h"
#import "EBCustomBannerView.h"

@interface ChatViewController () {
    UIButton *infoBt;
    UIImageView *logoIV;
    UILabel *titleLb;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    [self.view addSubview:self.navView];
}

- (void)setdata {
    
    _photoManager = [[SelectPhotoManager alloc] init];
    
    if (_profileModel != nil) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:_profileModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
        titleLb.text = _profileModel.nickName;
    }
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) infoAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_info"];
    
    NSDictionary *userParams = @{@"id":_profileModel.userId};
    
    [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        if ([DataTool checkUserIsShare:userProfileDataModel.userId]) {
            [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:YES];
        } else {
            [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:NO];
        }
    }];
    
}

- (void) moreAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_reportBlock"];
    
    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
    } blockCallback:^(id  _Nullable obj) {
        [self backAction];
    }];
    
}

#pragma mark - inputController Delegate
- (void)inputControllerSelectPhotoBt:(TUIInputController *)inputController {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_SelectPhoto"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Photo" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    if (IS_IPad) {
        alertController = [UIAlertController alertControllerWithTitle:@"Select Photo" message:nil preferredStyle: UIAlertControllerStyleAlert];
    }
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Take a Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self takePictureForSend];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Choose from Library" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoForSend];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
    
}

- (void)inputControllerSelectVideoBt:(TUIInputController *)inputController {
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.imageSize = CGSizeMake(30, 20);
    configuration.selectedCellBackgroundColor = RGB(85, 95, 98);
    configuration.textColor = WhiteColor;
    configuration.textFont = [UIFont systemFontOfSize:16];
    configuration.textAlignment = NSTextAlignmentLeft;
    
    configuration.separatorColor = RGB(104, 118, 122);
    configuration.backgroundColor = RGB(85, 95, 98);
    
    NSArray *titleArray = @[ @"Allow video calls", @"Start video chat"];
    NSArray *imgArray = @[[UIImage imageNamed:@"messages_video_close"], [UIImage imageNamed:@"messages_video_start"]];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:UserVideoCall]) {
        imgArray = @[[UIImage imageNamed:@"messages_video_open"], [UIImage imageNamed:@"messages_video_start"]];
    }

    [FTPopOverMenu showForSender:self.inputController.inputBar.videoButton
                   withMenuArray:titleArray
                      imageArray:imgArray
                   configuration:configuration
                       doneBlock:^(NSInteger selectedIndex) {
        
        if (selectedIndex == 0) {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_SetCall"];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:UserVideoCall]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:UserVideoCall];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:UserVideoCall];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        } else {
            
            [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_videoCall"];
            
            NSLog(@"Start video chat");
            [[UserInfo shareInstant] showCallOutView:self->_profileModel];
        }
    } dismissBlock:^{
    }];
    
}

- (void)inputController:(TUIInputController *)inputController didSendMessage:(TUIMessageCellData *)msg {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ChatView_sendMessage"];
    [self sendMessage:msg];
}

- (void)messageController:(TUIMessageController *)controller tapReportMenuInCell:(NSString *)msgId {
    [DataTool reportBlockUserName:@"The Selected User" andBlockUserId:msgId reportCallback:^(id  _Nullable obj) {
    } blockCallback:^(id  _Nullable obj) {
        [self backAction];
    }];
    
}


#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navView.backgroundColor = BGBlackColor;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        infoBt = [[UIButton alloc] initWithFrame:CGRectMake( (Screen_Width - ZoomSize(140) )/2.0, StatusBarHeight, ZoomSize(140), ZoomSize(30))];
        [_navView addSubview:infoBt];
        [infoBt addTarget:self action:@selector(infoAction) forControlEvents:UIControlEventTouchUpInside];
        
        logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(0), 0, ZoomSize(30), ZoomSize(30))];
        logoIV.layer.cornerRadius = ZoomSize(15);
        logoIV.layer.masksToBounds = YES;
        logoIV.image = [UIImage imageNamed:@"common_message_male"];
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        [infoBt addSubview:logoIV];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(30), 0, ZoomSize(110), ZoomSize(30))];
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(20)];
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.adjustsFontSizeToFitWidth = YES;
        [infoBt addSubview:titleLb];
        
        UIButton *moreBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(40), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
        [_navView addSubview:moreBt];
        [moreBt setImage:[UIImage imageNamed:@"messages_more"] forState:UIControlStateNormal];
        [moreBt addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navView;
}

@end

//
//  MessageViewController.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "MessageViewController.h"
#import "LikeMeListViewController.h"

#import "OfficialChatListViewController.h"
#import "ChatViewController.h"
#import "ChatRoomViewController.h"

#import <TIMManager.h>
#import <TIMConversation.h>
#import "V2TIMManager+Conversation.h"
#import "V2TIMManager+Message.h"
#import "TUIKit.h"

#import "MessageTableViewCell.h"
#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"MessageTableViewCell";

@interface MessageViewController ()<V2TIMSDKListener, V2TIMConversationListener, UITableViewDelegate, UITableViewDataSource> {
    V2TIMConversation *systemConv;
    bool needShowHeader;
    
    UIButton *chatRoomBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray * uiConvList;

@property (nonatomic, strong) UIView *tableHeaderView;
@property (nonatomic, strong) NSMutableArray *getLikeArray;
@property (nonatomic, strong) NSArray *blockArray;

@end

@implementation MessageViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ChatRoom]) { //使用存储的关键字信息
        chatRoomBt.hidden = YES;
    } else {
        chatRoomBt.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.tableView];
    
    chatRoomBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(166), Screen_Height - TabBarHeight - ZoomSize(80), ZoomSize(166), ZoomSize(58))];
    [self.view addSubview:chatRoomBt];
    [chatRoomBt addTarget:self action:@selector(chatRoomAction) forControlEvents:UIControlEventTouchUpInside];
    [chatRoomBt setBackgroundImage:[UIImage imageNamed:@"message_group_chat"] forState:UIControlStateNormal];
    chatRoomBt.hidden = YES;
    
    UILabel *chatRoomTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(16), ZoomSize(100), ZoomSize(20))];
    [chatRoomBt addSubview:chatRoomTitleLb];
    chatRoomTitleLb.textColor = WhiteColor;
    chatRoomTitleLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
    chatRoomTitleLb.text = @"ChatRoom";
    
    UIImageView *chatRoomUserIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(120), ZoomSize(12), ZoomSize(30), ZoomSize(30))];
    [chatRoomBt addSubview:chatRoomUserIV];
    chatRoomUserIV.image = [UIImage imageNamed:@"message_fuzzy_portrait"];
    chatRoomUserIV.layer.cornerRadius = ZoomSize(15);
    chatRoomUserIV.layer.masksToBounds = YES;
    chatRoomUserIV.layer.borderWidth = ZoomSize(1);
    chatRoomUserIV.layer.borderColor = WhiteColor.CGColor;
}

- (void)setData {
    
    needShowHeader = NO;
    self.uiConvList = [NSMutableArray array];
    
    if ([[TIMManager sharedInstance] getLoginStatus] == 1) {
        [[V2TIMManager sharedInstance] setConversationListener:self];//添加消息监听
        [self requestData];
    } else {
        [UserInfo shareInstant].timLoginSuccBlock = ^(id  _Nullable obj) {
            [[V2TIMManager sharedInstance] setConversationListener:self];//添加消息监听
            [self requestData];
        };
    }
    
    V2TIMSDKConfig *config = [V2TIMSDKConfig alloc]; //需要初始化，否则不能监听
    config.logLevel = V2TIM_LOG_NONE;
    [[V2TIMManager sharedInstance] initSDK:[TXIMSDK_APP_KEY intValue] config:config listener:self];
    
    [self getLikeMeNumber];
}

- (void) getLikeMeNumber {
    
    self.getLikeArray = [NSMutableArray array];
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:1] };
    
    [[NetTool shareInstance] startRequest:API_user_relationLoveMe method:GetMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        
        self.getLikeArray = [UserProfileDataModel mj_objectArrayWithKeyValuesArray:obj];
        if (self.getLikeArray.count > 0) {
            self->needShowHeader = YES;
        } else {
            self->needShowHeader = NO;
        }
        [self.tableView reloadData];
    }];
}

#pragma mark - Function

- (void)requestData {
    __weak __typeof(self) weakSelf = self;
    [[V2TIMManager sharedInstance] getConversationList:0 count:50
                                                  succ:^(NSArray<V2TIMConversation *> *list, uint64_t nextSeq, BOOL isFinished) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        
        //重新获取一次黑名单信息
        [[V2TIMManager sharedInstance] getBlackList:^(NSArray<V2TIMFriendInfo *> *infoList) {
            self->_blockArray = infoList;
            [strongSelf updateConversation:list]; // 获取成功
        } fail:^(int code, NSString *desc) {
            [strongSelf updateConversation:list]; // 获取失败
        }];
    } fail:^(int code, NSString *msg) { // 拉取会话列表失败
    }];
    
}

// 更新 UI 会话列表
- (void)updateConversation:(NSArray *)convList {
    
    for (int i = 0 ; i < convList.count ; ++ i) { //     如果 UI 会话列表有更新的会话，就替换，如果没有，就新增
        V2TIMConversation *conv = convList[i];
        BOOL isExit = NO;
        
        for (int j = 0; j < self.uiConvList.count; ++ j) {
            V2TIMConversation *uiConv = self.uiConvList[j];
            if ([uiConv.conversationID isEqualToString:conv.conversationID]) { // UI 会话列表有更新的会话，直接替换
                [self.uiConvList replaceObjectAtIndex:j withObject:conv];
                isExit = YES;
                break;
            }
        }
        
        if (!isExit) { // UI 会话列表没有更新的会话，直接新增
            [self.uiConvList addObject:conv];
        }
    }
    
    if (self.uiConvList.count > 0) {
        
        // 重新按照会话 lastMessage 的 timestamp 对 UI 会话列表做排序
        [self.uiConvList sortUsingComparator:^NSComparisonResult(V2TIMConversation *obj1, V2TIMConversation *obj2) {
            return [obj2.lastMessage.timestamp compare:obj1.lastMessage.timestamp];
        }];
        
        //将系统的数据放在第一个
        BOOL needShowSystemToFirst = false;
        for (V2TIMConversation *obj in self.uiConvList) { //将系统的数据放在第一个
            if ([obj.userID isEqualToString:OfficialUserID]) {
                needShowSystemToFirst = YES;
                systemConv = obj;
                break;
            }
        }
        if (needShowSystemToFirst) {
            [self.uiConvList removeObject:systemConv];
            [self.uiConvList insertObject:systemConv atIndex:0];
        }
        
        //删除黑名单数据
        if (self.blockArray.count > 0) {
            for (V2TIMFriendInfo *friendInfo in self.blockArray) {
                BOOL needDelete = NO;
                NSUInteger blockIndex = 0;
                for (V2TIMConversation *obj in self.uiConvList) {
                    if ([obj.userID isEqualToString:friendInfo.userID]) {
                        blockIndex = [self.uiConvList indexOfObject:obj];
                        needDelete = YES;
                        break;
                    }
                }
                if (needDelete) {
                    [self.uiConvList removeObjectAtIndex:blockIndex];
                }
            }
        }
        
        //删除群消息
        BOOL needDeleteChatRoom = NO;
        NSUInteger chatRoomIndex = 0;
        for (V2TIMConversation *obj in self.uiConvList) {
            if (obj.type == 2) {
                needDeleteChatRoom = YES;
                chatRoomIndex = [self.uiConvList indexOfObject:obj];
                break;
            }
        }
        if (needDeleteChatRoom) {
            [self.uiConvList removeObjectAtIndex:chatRoomIndex];
        }
        
        //重新刷新列表
        [self.tableView reloadData];
        
        
        NSString *bodgeString = nil;
        NSUInteger unreadNum = 0;
        for (V2TIMConversation *obj in self.uiConvList) {
            unreadNum = unreadNum + obj.unreadCount;
        }
        if (unreadNum > 0) {
            if (unreadNum <= 99) {
                bodgeString = [NSString stringWithFormat:@"%lu", (unsigned long)unreadNum];
            } else {
                bodgeString = @"+99";
            }
        }
        
        self.tabBarItem.badgeValue = bodgeString;
    }
}

- (void) likeInfoAction {
//    if (![[UserInfo shareInstant] checkUserIsVip]) {
//        [DataTool showBuyVipView];
//        return;
//    }
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Message_like"];
    
    LikeMeListViewController *vc = [[LikeMeListViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) chatRoomAction {
    NSLog(@"chatRoomAction");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Message_chatRoom"];
    
    [[V2TIMManager sharedInstance] joinGroup:ChatRoomId msg:@"hi" succ:^{
        TIMConversation *conv = [[TIMManager sharedInstance] getConversation:2 receiver:ChatRoomId];
        ChatRoomViewController *vc = [[ChatRoomViewController alloc] initWithConversation:conv];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    } fail:^(int code, NSString *desc) {
        TIMConversation *conv = [[TIMManager sharedInstance] getConversation:2 receiver:ChatRoomId];
        ChatRoomViewController *vc = [[ChatRoomViewController alloc] initWithConversation:conv];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];
    
}

#pragma mark - delegate

// 收到会话新增的回调
- (void)onNewConversation:(NSArray<V2TIMConversation*> *) conversationList {
//    [self updateConversation:conversationList];
    [self requestData];
}

// 收到会话更新的回调
- (void)onConversationChanged:(NSArray<V2TIMConversation*> *) conversationList {
//    [self updateConversation:conversationList];
    [self requestData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.uiConvList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (needShowHeader) {
        return ZoomSize(90);
    } else {
        return ZoomSize(0);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.tableHeaderView;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    V2TIMConversation *uiConv = self.uiConvList[indexPath.row];
    cell.conv = uiConv; //设置数据
    
    if(cell == nil) {
        cell = [[MessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
    
    V2TIMConversation *uiConv = self.uiConvList[indexPath.row];
    
    if ([uiConv.userID isEqualToString:OfficialUserID]) {
        
        [[NetTool shareInstance] sendFireBaseWithEventName:@"Message_official"];
        
        OfficialChatListViewController *vc = [[OfficialChatListViewController alloc] init];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
        [[V2TIMManager sharedInstance] markC2CMessageAsRead:uiConv.userID succ:^{
        } fail:^(int code, NSString *desc) { //设置消息已读
        }];
        
    } else {
        [[NetTool shareInstance] sendFireBaseWithEventName:@"Message_chat"];
        
//        if ( ![[UserInfo shareInstant] checkUserIsVip]) {
//            [DataTool showBuyVipView];
//            return;
//        }
        
        UserProfileDataModel *profileModel = [[UserProfileDataModel alloc] init];
        profileModel.userId = uiConv.userID;
        profileModel.nickName = uiConv.showName;
        profileModel.headImgUrl = uiConv.faceUrl;
        [DataTool showChatViewControllerWithType:TIM_C2C receiver:profileModel.userId receiverProfileModel:profileModel];
    }
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navBgView.backgroundColor = BGBlackColor;
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), StatusBarHeight, ZoomSize(175), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Message";
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(26) weight:UIFontWeightMedium];
        
    }
    return _navBgView;
}

- (UIView *)tableHeaderView {
    if (!_tableHeaderView) {
        _tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(90))];
        _tableHeaderView.backgroundColor = BGBlackColor;
        
        UIButton *likeInfoBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(6), SCREEN_Width - ZoomSize(30), ZoomSize(78))];
        [_tableHeaderView addSubview:likeInfoBt];
        likeInfoBt.backgroundColor = RGB(39, 46, 47);
        likeInfoBt.layer.cornerRadius = ZoomSize(39);
        likeInfoBt.layer.masksToBounds = YES;
        [likeInfoBt addTarget:self action:@selector(likeInfoAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *userIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(27), ZoomSize(23), ZoomSize(39), ZoomSize(39))];
        [_tableHeaderView addSubview:userIV];
        userIV.image = [UIImage imageNamed:@"message_fuzzy_portrait"];
        userIV.layer.cornerRadius = ZoomSize(19.5);
        userIV.layer.masksToBounds = YES;
        
        UILabel *tableHeaderViewTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(85), ZoomSize(18), ZoomSize(240), ZoomSize(21))];
        [_tableHeaderView addSubview:tableHeaderViewTitleLb];
        tableHeaderViewTitleLb.textColor = WhiteColor;
        tableHeaderViewTitleLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        tableHeaderViewTitleLb.text = [NSString stringWithFormat:@"You got %lu likes, check it out!", self.getLikeArray.count];
        
        UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(85), ZoomSize(49), ZoomSize(55), ZoomSize(20))];
        [_tableHeaderView addSubview:tipLb];
        tipLb.text = @"See all";
        tipLb.textColor = RGB(250, 180, 22);
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        
        UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(142), ZoomSize(52), ZoomSize(22), ZoomSize(15))];
        [_tableHeaderView addSubview:tipIV];
        tipIV.image = [UIImage imageNamed:@"message_see_all"];
        
    }
    return _tableHeaderView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.rowHeight = ZoomSize(86);
        
        _tableView.backgroundColor = BGBlackColor;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
    }
    return _tableView;
}

@end

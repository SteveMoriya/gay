//
//  UserInfoCollectionViewCell.h
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RelationUserDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) RelationUserDataModel *relationDataModel;

@end

NS_ASSUME_NONNULL_END

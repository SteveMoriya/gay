//
//  BlockTableViewCell.m
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "BlockTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface BlockTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *headIV;
@property (weak, nonatomic) IBOutlet UIButton *unblockBt;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@end

@implementation BlockTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _unblockBt.layer.borderColor = RGB(250, 180, 22).CGColor;
    _unblockBt.layer.borderWidth = 1;
    
}

- (IBAction)unblockAction:(id)sender {
    if (_unblockBlock) {
        _unblockBlock();
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)setProfileModel:(UserProfileDataModel *)profileModel {
//
//    _profileModel = profileModel;
//
//    _nameLb.text = [NSString stringWithFormat:@"%@%@", profileModel.nickName, _profileModel.age];
////    _addressLb.text = profileModel.address;
//
//    [_headIV sd_setImageWithURL:[NSURL URLWithString:profileModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
//
//}

//- (void)setUserInfo:(V2TIMUserFullInfo *)userInfo {
//    _userInfo = userInfo;
//
//    _nameLb.text = userInfo.nickName;
//    [_headIV sd_setImageWithURL:[NSURL URLWithString:userInfo.faceURL] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
//
//}

- (void)setFriendInfo:(V2TIMFriendInfo *)friendInfo {
    _friendInfo = friendInfo;
    _nameLb.text = friendInfo.userFullInfo.nickName;
    [_headIV sd_setImageWithURL:[NSURL URLWithString:friendInfo.userFullInfo.faceURL] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
}

@end

//
//  FollowTableViewCell.h
//  gay
//
//  Created by steve on 2020/11/13.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RelationUserDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FollowTableViewCell : UITableViewCell

@property (nonatomic, strong) RelationUserDataModel *relationDataModel;

@property (nonatomic, assign) BOOL needHideBt;

@end

NS_ASSUME_NONNULL_END

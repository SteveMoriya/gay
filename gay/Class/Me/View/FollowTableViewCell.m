//
//  FollowTableViewCell.m
//  gay
//
//  Created by steve on 2020/11/13.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "FollowTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FollowTableViewCell() {
}

@property (weak, nonatomic) IBOutlet UIImageView *headIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@property (weak, nonatomic) IBOutlet UIImageView *countryIV;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;
@property (weak, nonatomic) IBOutlet UIButton *funtionBt;

@end

@implementation FollowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void) checkFuntionBtState {
    if ([_relationDataModel.relationType isEqualToString: @"follow" ]) {
        [_funtionBt setTitle:@"Follow" forState:UIControlStateNormal];
        [_funtionBt setTitleColor:ThemeYellow forState:UIControlStateNormal];
        _funtionBt.backgroundColor = UIColor.clearColor;
        
    } else {
        [_funtionBt setTitle:@"UNFollow" forState:UIControlStateNormal];
        [_funtionBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        _funtionBt.backgroundColor = ThemeYellow;
    }
}

- (IBAction)followAction:(id)sender {
    
    if ([_relationDataModel.relationType isEqualToString: @"follow" ]) {
        
        NSDictionary *paramsDic = @{@"like": [NSNumber numberWithBool:true],  @"id": _relationDataModel.userId };
        [[NetTool shareInstance] startRequest:API_user_like method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            self->_relationDataModel.relationType = @"following";
            [self checkFuntionBtState];
        }];
        
    } else {
        
        NSDictionary *paramsDic = @{@"like": [NSNumber numberWithBool:false],  @"id": _relationDataModel.userId };
        [[NetTool shareInstance] startRequest:API_user_like method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            self->_relationDataModel.relationType = @"follow";
            [self checkFuntionBtState];
        }];
    }
    
}

- (void)setRelationDataModel:(RelationUserDataModel *)relationDataModel {
    _relationDataModel = relationDataModel;
    [_headIV sd_setImageWithURL:[NSURL URLWithString:_relationDataModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    _nameLb.text = [NSString stringWithFormat:@"%@", _relationDataModel.nickName];
    
    [_countryIV sd_setImageWithURL:[NSURL URLWithString:_relationDataModel.countryIcon] ];
    _addressLb.text = _relationDataModel.country;
    
    [self checkFuntionBtState];
}

- (void)setNeedHideBt:(BOOL)needHideBt {
    _needHideBt = needHideBt;
    
    _funtionBt.hidden = NO;
    if (_needHideBt) {
        _funtionBt.hidden = YES;
    }
}

@end

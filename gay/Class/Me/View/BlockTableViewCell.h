//
//  BlockTableViewCell.h
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BlockTableViewCell : UITableViewCell

//@property (nonatomic, strong) UserProfileDataModel *profileModel;

@property (nonatomic, strong) V2TIMFriendInfo *friendInfo;

@property (nonatomic, copy) ClickBlock unblockBlock;


@end

NS_ASSUME_NONNULL_END

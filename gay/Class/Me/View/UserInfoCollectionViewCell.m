//
//  UserInfoCollectionViewCell.m
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserInfoCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageDownloader.h>

@interface UserInfoCollectionViewCell(){
}

@property (weak, nonatomic) IBOutlet UIImageView *headIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@end

@implementation UserInfoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setRelationDataModel:(RelationUserDataModel *)relationDataModel {
    _relationDataModel = relationDataModel;
    
    _nameLb.text = [NSString stringWithFormat:@"%@", _relationDataModel.nickName];
    
//    if (_relationDataModel.needMasker && ![[UserInfo shareInstant] checkUserIsVip]) {
//        _headIV.image = [UIImage imageNamed:@"common_message_male"];
//        
//        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:_relationDataModel.headImgUrl] completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
//            
//            [DataTool compressSizeImageToSize:image setSize:self->_headIV.frame.size finishblock:^(id  _Nullable obj) {
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    [DataTool tg_blurryImage:obj withBlurLevel:5 finishblock:^(id  _Nullable obj) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            self->_headIV.image = obj;
//                        });
//                    }];
//                });
//            }];
//        }];
//    } else {
//        [_headIV sd_setImageWithURL:[NSURL URLWithString:_relationDataModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
//    }
    
    [_headIV sd_setImageWithURL:[NSURL URLWithString:_relationDataModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    
}


@end

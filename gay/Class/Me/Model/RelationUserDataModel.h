//
//  RelationUserDataModel.h
//  gay
//
//  Created by steve on 2020/11/13.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RelationUserDataModel : NSObject

@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *countryIcon;
@property (nonatomic, strong) NSString *headImgUrl;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *online;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *userId;

@property (nonatomic, strong) NSString *relationType; //follow, followed
@property (nonatomic, assign) BOOL needMasker;

//country = "<null>";
//countryIcon = "https://yt-bullet.oss-cn-shenzhen.aliyuncs.com/icon/country/icon_us.png";
//headImgUrl = "https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/image/headImg/image1.jpeg";
//nickName = Guest8144;
//online = "<null>";
//sex = 1;
//time = "2020-10-23T08:26:01.000+0000";
//userId = 198570;


@end

NS_ASSUME_NONNULL_END

//
//  SettingViewController.m
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "SettingViewController.h"
#import "FeedbackViewController.h"
#import "BlockListViewController.h"

@interface SettingViewController (){
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation SettingViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)logOutAction {
    [[UserInfo shareInstant] userLogout];
}

- (UIButton *) createFunctionBtWithFrame:(CGRect) frame titleString:(NSString *) titleString btnTag:(int) btnTag btEnabled:(BOOL) btEnabled  {
    
    UIButton *funcBt = [[UIButton alloc] initWithFrame:frame];
    funcBt.backgroundColor = RGB(32, 36, 37);
    [funcBt setTitle:titleString forState:UIControlStateNormal];
    [funcBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    funcBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(17)];
    funcBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    funcBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(9), 0, 0);
    funcBt.tag = btnTag;
    [funcBt addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    funcBt.layer.cornerRadius = ZoomSize(6);
    funcBt.layer.masksToBounds = YES;
    funcBt.layer.borderColor = RGB(38, 43, 44).CGColor;
    funcBt.layer.borderWidth = ZoomSize(1);
    
    if (btEnabled) {
        UIImageView *tipImagView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - ZoomSize(14), (frame.size.height - ZoomSize(13))/2.0 , ZoomSize(8), ZoomSize(13))];
        tipImagView.image = [UIImage imageNamed:@"nav_right"];
        [funcBt addSubview:tipImagView];
    }
    
    return funcBt;
}


- (void)switchFunctionAction:(id ) sender {
    
    UIView *getView = sender;
    switch (getView.tag) {
            
        case 111: {
            
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_setCall"];
            
            UISwitch *callSwitch = sender;
            if (callSwitch.isOn == YES) {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:UserVideoCall];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:UserVideoCall];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            break;
        }
            
        case 112: {
            
//            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_setCall"];
            
            UISwitch *callSwitch = sender;
            
            if (callSwitch.isOn == YES) {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:ChatRoom];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:ChatRoom];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            break;
        }
            
            
        case 120: {
            
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_blockList"];
            
            BlockListViewController *vc = [[BlockListViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 130: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_feedback"];
            FeedbackViewController *vc = [[FeedbackViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 140: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_feedback"];
            [DataTool openWebVC:TermURLString];
            break;
        }
        case 150: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_privacy"];
            [DataTool openWebVC:PolicyURLString];
            break;
        }
        case 160: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Setting_terms"];
            [DataTool openWebVC:AboutUSURLString];
            break;
        }
            
        default: break;
    }
}


#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight, Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Settings";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        
        
#pragma mark - videoCall
        UIButton *videoCallBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(10), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"Video Call" btnTag:110 btEnabled: NO];
        [_contentScrollView addSubview:videoCallBt];
        
        UISwitch *callSwitch = [[UISwitch alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(30) - 57 , (ZoomSize(60) - 32)/2.0, 57, 32 )];
        callSwitch.onTintColor = RGB(250, 180, 22);
        [callSwitch addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventValueChanged];
        callSwitch.tag = 111;
        [videoCallBt addSubview:callSwitch];

        callSwitch.on = NO;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:UserVideoCall]) {
            callSwitch.on = YES;
        }
        
        
#pragma mark - chatRoomBt
        UIButton *chatRoomBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(80), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"Hide ChatRoom" btnTag:110 btEnabled: NO];
        [_contentScrollView addSubview:chatRoomBt];
        
        UISwitch *chatRoomSwitch = [[UISwitch alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(30) - 57 , (ZoomSize(60) - 32)/2.0, 57, 32 )];
        chatRoomSwitch.onTintColor = RGB(250, 180, 22);
        [chatRoomSwitch addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventValueChanged];
        chatRoomSwitch.tag = 112;
        [chatRoomBt addSubview:chatRoomSwitch];

        chatRoomSwitch.on = NO;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:ChatRoom]) {
            chatRoomSwitch.on = YES;
        }
        
        
#pragma mark - BlockList
        UIButton *blockListBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(160), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"BlockList" btnTag:120 btEnabled: YES];
        blockListBt.layer.cornerRadius = ZoomSize(0);
        
        UIBezierPath *blockListBtMaskPath = [UIBezierPath bezierPathWithRoundedRect:blockListBt.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(6),ZoomSize(6))];
        CAShapeLayer *blockListBtMaskLayer = [[CAShapeLayer alloc] init];
        blockListBtMaskLayer.frame = blockListBt.bounds;
        blockListBtMaskLayer.path = blockListBtMaskPath.CGPath;
        blockListBt.layer.mask = blockListBtMaskLayer;
        
        [_contentScrollView addSubview:blockListBt];
        
#pragma mark - FeedbackBt
        UIButton *feedBackBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(220), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"Feedback" btnTag:130 btEnabled: YES];
        feedBackBt.layer.cornerRadius = ZoomSize(0);
        [_contentScrollView addSubview:feedBackBt];
        
#pragma mark - privacyPolicyBt
        UIButton *privacyPolicyBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(280), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"Privacy Policy" btnTag:140 btEnabled: YES];
        privacyPolicyBt.layer.cornerRadius = ZoomSize(0);
        [_contentScrollView addSubview:privacyPolicyBt];
        
#pragma mark - termOfUseBt
        UIButton *termOfUseBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(340), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"Term Of Use" btnTag:150 btEnabled: YES];
        termOfUseBt.layer.cornerRadius = ZoomSize(0);
        
        UIBezierPath *termOfUseBtMaskPath = [UIBezierPath bezierPathWithRoundedRect:termOfUseBt.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(ZoomSize(6),ZoomSize(6))];
        CAShapeLayer *termOfUseBtMaskLayer = [[CAShapeLayer alloc] init];
        termOfUseBtMaskLayer.frame = termOfUseBt.bounds;
        termOfUseBtMaskLayer.path = termOfUseBtMaskPath.CGPath;
        termOfUseBt.layer.mask = termOfUseBtMaskLayer;
        
        [_contentScrollView addSubview:termOfUseBt];
        
#pragma mark - Aboutus
        UIButton *aboutUsBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(15), ZoomSize(420), SCREEN_Width - ZoomSize(30), ZoomSize(60)) titleString:@"About Us" btnTag:160 btEnabled: YES];
        [_contentScrollView addSubview:aboutUsBt];
        
        
#pragma logouBt
        UIButton *logOutBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(15), ZoomSize(510) , SCREEN_Width - ZoomSize(30), ZoomSize(50))];
        [_contentScrollView addSubview:logOutBt];
        logOutBt.layer.cornerRadius = ZoomSize(8);
        logOutBt.layer.masksToBounds = YES;
        [logOutBt setTitle:@"Log Out" forState:UIControlStateNormal];
        logOutBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [logOutBt setTitleColor:ThemeYellow forState:UIControlStateNormal];
        [logOutBt setBackgroundImage:[DataTool imageWithColor:RGB(40, 46, 47)] forState:UIControlStateNormal];
        [logOutBt addTarget:self action:@selector(logOutAction) forControlEvents:UIControlEventTouchUpInside];
        
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(aboutUsBt.frame) + ZoomSize(50));
    }
    return _contentScrollView;
}


@end

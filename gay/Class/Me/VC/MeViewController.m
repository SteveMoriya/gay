//
//  MeViewController.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "MeViewController.h"
#import "EditInfoViewController.h"
#import "SelectPhotoManager.h"
#import "SettingViewController.h"
#import "FollowedViewController.h"
#import "FollowingViewController.h"
#import "MyVisitorViewController.h"
#import "MyLikeListViewController.h"
#import "UserPostsViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

@interface MeViewController () {
    UIButton *headBt;
    UIImage *getPlaceHolderImg;
    NSString *headImgUrlString;
    
    UILabel *nameLb;
    UILabel *followersNumLb;
    UILabel *followingNumLb;
    UILabel *postNumbLb;
    
    UIButton *buyVIPBt;
    UIView *functionView;
    UILabel *profileNumLb;
}

@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation MeViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
    [self setPageInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
    _photoManager = [[SelectPhotoManager alloc] init];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    
    NSDictionary *userParams = @{};
    [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:NO callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;

        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [self setPageInfo];
        }];
    }];

}

#pragma function

- (void) setPageInfo {
    [headBt sd_setImageWithURL:[NSURL URLWithString:[UserInfo shareInstant].user.headImgUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    
    nameLb.text = [UserInfo shareInstant].user.nickName;
    followersNumLb.text = ([DataTool isEmptyString:[UserInfo shareInstant].user.fansNumber] ? @"0" : [UserInfo shareInstant].user.fansNumber);
    followingNumLb.text = ([DataTool isEmptyString:[UserInfo shareInstant].user.concernNumber] ? @"0" : [UserInfo shareInstant].user.concernNumber);
    postNumbLb.text = ([DataTool isEmptyString:[UserInfo shareInstant].user.postNumber] ? @"0" : [UserInfo shareInstant].user.postNumber);
    
    
    int finishPercent = 0;
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.intro]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.identity]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.role]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.age]) {
        finishPercent = finishPercent + 10;
    }
    if (![[UserInfo shareInstant].user.height isEqualToString:@"0"]) {
        finishPercent = finishPercent + 10;
    }
    
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.bodyType]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.languages]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.ethnicity]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.lookFor]) {
        finishPercent = finishPercent + 10;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.address]) {
        finishPercent = finishPercent + 10;
    }
    
    profileNumLb.text = [NSString stringWithFormat:@"%d %%",finishPercent];
    
}

- (UIButton *) createFunctionBtWithFrame:(CGRect) frame  titleString:(NSString *) titleString imageString:(NSString *) imageString btnTag:(int) btnTag  {
    
    UIButton *funcBt = [[UIButton alloc] initWithFrame:frame];
    [funcBt setTitle:titleString forState:UIControlStateNormal];
    [funcBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    funcBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
    funcBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    funcBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(41), 0, 0);
    funcBt.tag = btnTag;
    [funcBt addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width , ZoomSize(1))];
    [funcBt addSubview:topLineView];
    topLineView.backgroundColor = RGB(36, 42, 43);
    
    UIView *buttonLineView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height, frame.size.width , ZoomSize(1))];
    [funcBt addSubview:buttonLineView];
    buttonLineView.backgroundColor = RGB(36, 42, 43);
    
    if (![DataTool isEmptyString:imageString]) {
        UIImageView *infoImagView = [[UIImageView alloc] initWithFrame:CGRectMake( ZoomSize(16), (frame.size.height - ZoomSize(18))/2.0 , ZoomSize(18), ZoomSize(18))];
        infoImagView.image = [UIImage imageNamed:imageString];
        [funcBt addSubview:infoImagView];
    }
    
    UIImageView *tipImagView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - ZoomSize(23), (frame.size.height - ZoomSize(13))/2.0 , ZoomSize(8), ZoomSize(13))];
    tipImagView.image = [UIImage imageNamed:@"nav_right"];
    [funcBt addSubview:tipImagView];
    
    return funcBt;
}

- (void)switchFunctionAction:(UIButton *) sender {
    
    switch (sender.tag) {
        case 100: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_myVisitor"];
            MyVisitorViewController *vc = [[MyVisitorViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 110: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_myLike"];
            MyLikeListViewController *vc = [[MyLikeListViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 120: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_edit"];
            EditInfoViewController *vc = [[EditInfoViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 130: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_setting"];
            SettingViewController *vc = [[SettingViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
            
        default: {
        }
    };
}

- (void) editAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_edit"];
    
    EditInfoViewController *vc = [[EditInfoViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) buyVipAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_buyvip"];
    if ( ![[UserInfo shareInstant] checkUserIsVip] ) {
        [DataTool showBuyVipView];
    }
}

- (void) followerAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_follower"];
    FollowedViewController *vc = [[FollowedViewController alloc] init];
    vc.userId = [UserInfo shareInstant].user.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) followingAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_following"];
    FollowingViewController *vc = [[FollowingViewController alloc] init];
    vc.userId = [UserInfo shareInstant].user.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) postAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Post_normalPosts"];
    UserPostsViewController *vc = [[UserPostsViewController alloc] init];
    vc.userId = [UserInfo shareInstant].user.userId;
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

#pragma Lazy
- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        
#pragma userInfo
        headBt = [[UIButton alloc] initWithFrame:CGRectMake( (Screen_Width - ZoomSize(80))/2.0,   ZoomSize(10), ZoomSize(80), ZoomSize(80))];
        [_contentScrollView addSubview:headBt];
        headBt.backgroundColor = RGB(38, 44, 45);
        [headBt setImage:[UIImage imageNamed:@"common_edit_add_black"] forState:UIControlStateNormal];
        headBt.layer.cornerRadius = ZoomSize(40);
        headBt.layer.masksToBounds = YES;
        headBt.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [headBt.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [headBt addTarget:self action:@selector(editAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *editBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - ZoomSize(42) ,ZoomSize(10), ZoomSize(27), ZoomSize(27))];
        editBt.layer.cornerRadius = ZoomSize(13.5);
        editBt.layer.masksToBounds = YES;
        [editBt setBackgroundImage:[UIImage imageNamed:@"me_edit"] forState:UIControlStateNormal];
        [_contentScrollView addSubview:editBt];
        [editBt addTarget:self action:@selector(editAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:headBt.bounds];
        [headBt addSubview:indicatorView];
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        indicatorView.tag = 120;
        
        nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(100), Screen_Width - ZoomSize(30), ZoomSize(30))];
        [_contentScrollView addSubview:nameLb];
        nameLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        nameLb.textAlignment = NSTextAlignmentCenter;
        nameLb.textColor = WhiteColor;
        
        UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(130), Screen_Width - ZoomSize(30), ZoomSize(20))];
        [_contentScrollView addSubview:tipLb];
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        tipLb.textAlignment = NSTextAlignmentCenter;
        tipLb.textColor = RGB(133, 136, 137);
        tipLb.text = @"View and edit profile - Edit";
        
#pragma followView
        UIView *followView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(160), Screen_Width - ZoomSize(30), ZoomSize(63))];
        [_contentScrollView addSubview:followView];
        followView.layer.cornerRadius = ZoomSize(6);
        followView.layer.masksToBounds = YES;
        followView.backgroundColor = RGB(40, 46, 47);
        
        followersNumLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(5), Screen_Width/3.0 - ZoomSize(10), ZoomSize(40))];
        [followView addSubview:followersNumLb];
        followersNumLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        followersNumLb.textAlignment = NSTextAlignmentCenter;
        followersNumLb.textColor = WhiteColor;
        
        UILabel *followersNumTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(41), Screen_Width/3.0 - ZoomSize(10), ZoomSize(16))];
        [followView addSubview:followersNumTipLb];
        followersNumTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        followersNumTipLb.textAlignment = NSTextAlignmentCenter;
        followersNumTipLb.textColor = WhiteColor;
        followersNumTipLb.text = @"Follows";
        
        UIButton *followersBt = [[UIButton alloc] initWithFrame:CGRectMake(0,   ZoomSize(0), Screen_Width/3.0 - ZoomSize(10), ZoomSize(63))];
        [followView addSubview:followersBt];
        [followersBt addTarget:self action:@selector(followerAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *followLineView = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width/3.0 - ZoomSize(10) , ZoomSize(10), ZoomSize(1) , ZoomSize(40))];
        [followView addSubview:followLineView];
        followLineView.backgroundColor = RGB(54, 62, 63);
        
        followingNumLb = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width/3.0 - ZoomSize(10) , ZoomSize(5), Screen_Width/3.0 - ZoomSize(10), ZoomSize(40))];
        [followView addSubview:followingNumLb];
        followingNumLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        followingNumLb.textAlignment = NSTextAlignmentCenter;
        followingNumLb.textColor = WhiteColor;
        
        UILabel *followingNumTipLb = [[UILabel alloc] initWithFrame:CGRectMake( Screen_Width/3.0 - ZoomSize(10), ZoomSize(41), Screen_Width/3.0 - ZoomSize(10), ZoomSize(16))];
        [followView addSubview:followingNumTipLb];
        followingNumTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        followingNumTipLb.textAlignment = NSTextAlignmentCenter;
        followingNumTipLb.textColor = WhiteColor;
        followingNumTipLb.text = @"Following";
        
        UIButton *followingBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width/3.0 - ZoomSize(10), ZoomSize(0), Screen_Width/3.0 - ZoomSize(10), ZoomSize(63))];
        [followView addSubview:followingBt];
        [followingBt addTarget:self action:@selector(followingAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *followingLineView = [[UIView alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)) , ZoomSize(10), ZoomSize(1) , ZoomSize(40))];
        [followView addSubview:followingLineView];
        followingLineView.backgroundColor = RGB(54, 62, 63);
        
        postNumbLb = [[UILabel alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)) , ZoomSize(5), Screen_Width/3.0 - ZoomSize(10), ZoomSize(40))];
        [followView addSubview:postNumbLb];
        postNumbLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        postNumbLb.textAlignment = NSTextAlignmentCenter;
        postNumbLb.textColor = WhiteColor;
        
        UILabel *postNumTipLb = [[UILabel alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)), ZoomSize(41), Screen_Width/3.0 - ZoomSize(10), ZoomSize(16))];
        [followView addSubview:postNumTipLb];
        postNumTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        postNumTipLb.textAlignment = NSTextAlignmentCenter;
        postNumTipLb.textColor = WhiteColor;
        postNumTipLb.text = @"Posts";
        
        UIButton *postBt = [[UIButton alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)), ZoomSize(0), Screen_Width/3.0 - ZoomSize(10), ZoomSize(63))];
        [followView addSubview:postBt];
        [postBt addTarget:self action:@selector(postAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma buyVIPBt
        buyVIPBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(230), Screen_Width - ZoomSize(30), Screen_Width *(88.0/335) )];
        [buyVIPBt setBackgroundImage:[UIImage imageNamed:@"vip_personal_banner"] forState:UIControlStateNormal];
        [_contentScrollView addSubview:buyVIPBt];
        [buyVIPBt addTarget:self action:@selector(buyVipAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma functionView
        functionView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(buyVIPBt.frame) + ZoomSize(10), SCREEN_Width, ZoomSize(250))];
        [_contentScrollView addSubview:functionView];
        
#pragma visitorsList
        UIButton *myVisitorsBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0), ZoomSize(0), SCREEN_Width, ZoomSize(50)) titleString:@"Visitors" imageString:@"me_visitors" btnTag:100];
        [functionView addSubview:myVisitorsBt];
        
#pragma likesList
        UIButton *myLikesBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0),  ZoomSize(50), SCREEN_Width, ZoomSize(50)) titleString:@"My Favorites" imageString:@"me_like" btnTag:110];
        [functionView addSubview:myLikesBt];
        
#pragma complete profile
        UIButton *profileBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0),   ZoomSize(100), SCREEN_Width, ZoomSize(50)) titleString:@"Completion" imageString:@"me_profile" btnTag:120];
        [functionView addSubview:profileBt];
        
        profileNumLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(110), ZoomSize(4), ZoomSize(80), ZoomSize(40))];
        [profileBt addSubview:profileNumLb];
        profileNumLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        profileNumLb.textAlignment = NSTextAlignmentRight;
        profileNumLb.textColor = WhiteColor;
        
#pragma mySettingBt
        UIButton *mySettingBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0), ZoomSize(150), SCREEN_Width, ZoomSize(50)) titleString:@"My Setting" imageString:@"me_settings" btnTag:130];
        [functionView addSubview:mySettingBt];
        
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(functionView.frame) + ZoomSize(20));
    }
    return _contentScrollView;
}

@end

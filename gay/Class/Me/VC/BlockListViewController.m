//
//  BlockListViewController.m
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "BlockListViewController.h"
#import "BlockTableViewCell.h"

#import "MJRefresh.h"
#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"BlockTableViewCell";

@interface BlockListViewController ()<UITableViewDelegate, UITableViewDataSource> {
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation BlockListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.tableView];
}

- (void)setData {
    self.array = [NSMutableArray array];
    [self getHeaderData];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getHeaderData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"BlockList_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"BlockList_down"];
}

- (void)requestData {
    
    [[V2TIMManager sharedInstance] getBlackList:^(NSArray<V2TIMFriendInfo *> *infoList) {
        
        self.array = [NSMutableArray arrayWithArray:infoList];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
    } fail:^(int code, NSString *desc) {
    }];
    
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BlockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(cell == nil) {
        cell = [[BlockTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    V2TIMFriendInfo *friendInfo = self.array[indexPath.row];
    
    cell.friendInfo = friendInfo;
    cell.unblockBlock = ^{
        
        if ([DataTool checkUserIsShare:friendInfo.userID]) {
            
            [self.array removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
            NSArray *blackList = [NSArray arrayWithObject:friendInfo.userID];
            [[V2TIMManager sharedInstance] deleteFromBlackList:blackList succ:^(NSArray<V2TIMFriendOperationResult *> *resultList) {
            } fail:^(int code, NSString *desc) {
            }];
            
        } else {
            
            NSDictionary *paramsDic = @{@"isReject": [NSNumber numberWithBool:false],  @"uid": friendInfo.userID };
            [[NetTool shareInstance] startRequest:API_user_reject method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                
                [self.array removeObjectAtIndex:indexPath.row];
                [self.tableView reloadData];
                NSArray *blackList = [NSArray arrayWithObject:friendInfo.userID];
                [[V2TIMManager sharedInstance] deleteFromBlackList:blackList succ:^(NSArray<V2TIMFriendOperationResult *> *resultList) {
                } fail:^(int code, NSString *desc) {
                }];
                
            }];
            
        }
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
    
//    UserProfileDataModel *profileModel = self.array[indexPath.row];
    
}


#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight, Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Block List";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
    }
    return _navBgView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.backgroundColor = BGBlackColor;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
        _tableView.rowHeight = ZoomSize(90);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];
        
//        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
//            [self getFooterData];
//        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
        
        
    }
    return _tableView;
}

@end

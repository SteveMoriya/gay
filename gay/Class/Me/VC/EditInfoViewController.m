//
//  EditInfoViewController.m
//  gay
//
//  Created by steve on 2020/9/16.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "EditInfoViewController.h"
#import "SelectPhotoManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <MJExtension/MJExtension.h>

#import "JKPickView.h"
#import "ChooseBirthdayView.h"

#define textLength 140
#define textPlaceHolder @"Something about you..."

@interface EditInfoViewController ()<UITextViewDelegate> {
    
    UIButton *headBt;
    UIImage *getPlaceHolderImg;//上传的占位图片
    
    UITextField *nameTF;
    UITextView *aboutTV;
    
    UILabel *myIdentityLb;
    UILabel *myRoleLb;
    UILabel *myBirthLb;
    UILabel *myHeightLb;
    UILabel *myBodyTypeLb;
    
    UILabel *myLanguageLb;
    UILabel *myEnthnicityLb;
    UILabel *myLookingforLb;
    UILabel *myAddressLb;
    NSString *headImgUrlString;
    
    NSString *getLatitude;
    NSString *getLongitude;
    UserProfileDataModel *setProfileDataModel;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation EditInfoViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    _photoManager = [[SelectPhotoManager alloc] init];
    setProfileDataModel = [[UserProfileDataModel alloc] init];
    setProfileDataModel.userId = [UserInfo shareInstant].user.userId; //设置默认数据，用于preview
    setProfileDataModel.fansNumber = [UserInfo shareInstant].user.fansNumber;
    setProfileDataModel.concernNumber = [UserInfo shareInstant].user.concernNumber;
    setProfileDataModel.postNumber = [UserInfo shareInstant].user.postNumber;
    
    headImgUrlString = [UserInfo shareInstant].user.headImgUrl;
    getLatitude = [UserInfo shareInstant].user.latitude;
    getLongitude = [UserInfo shareInstant].user.longitude;
    
    [headBt sd_setImageWithURL:[NSURL URLWithString:headImgUrlString] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    
    nameTF.text = [UserInfo shareInstant].user.nickName;
    aboutTV.text = [UserInfo shareInstant].user.intro;
    myIdentityLb.text = [UserInfo shareInstant].user.identity;
    myRoleLb.text = [UserInfo shareInstant].user.role;
    myBirthLb.text = [UserInfo shareInstant].user.age;
    
    if (![[UserInfo shareInstant].user.height isEqualToString:@"0"]) {
        myHeightLb.text = [UserInfo shareInstant].user.height;
    }
    
    myBodyTypeLb.text = [UserInfo shareInstant].user.bodyType;
    myLanguageLb.text = [UserInfo shareInstant].user.languages;
    myEnthnicityLb.text = [UserInfo shareInstant].user.ethnicity;
    myLookingforLb.text = [UserInfo shareInstant].user.lookFor;
    myAddressLb.text = [UserInfo shareInstant].user.address;
    
}

#pragma mark - Function

- (void) getSetData {
    
    setProfileDataModel.headImgUrl = headImgUrlString;
    setProfileDataModel.nickName = nameTF.text;
    if (![DataTool isEmptyString:aboutTV.text]) {
        setProfileDataModel.intro = aboutTV.text;
    }
    setProfileDataModel.identity = myIdentityLb.text;
    setProfileDataModel.role = myRoleLb.text;
    setProfileDataModel.age = myBirthLb.text;
    
    if (![DataTool isEmptyString:myHeightLb.text]) {
        setProfileDataModel.height = myHeightLb.text;
    }
    
    setProfileDataModel.bodyType = myBodyTypeLb.text;
    setProfileDataModel.languages = myLanguageLb.text;
    setProfileDataModel.ethnicity = myEnthnicityLb.text;
    setProfileDataModel.lookFor = myLookingforLb.text;
    setProfileDataModel.address = myAddressLb.text;
    setProfileDataModel.latitude = getLatitude;
    setProfileDataModel.longitude = getLongitude;
}

- (void)backAction {
    
    NSDictionary *userDic = [[UserInfo shareInstant].user mj_keyValues];
    
    setProfileDataModel = [UserInfo shareInstant].user;
    [self getSetData];
    NSDictionary *paramDic = [setProfileDataModel mj_keyValues];
    
    NSLog(@"backAction %@, %@", userDic, paramDic);
    
    
    if ([userDic isEqualToDictionary:paramDic]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[NetTool shareInstance] startRequest:API_user method:PostMethod params:paramDic needShowHUD:YES callback:^(id  _Nullable obj) {
            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    }
}

- (void) previewAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_preview"];
    [self getSetData];
    [DataTool openUserProfileView:setProfileDataModel andClickImgIndex:0 isFromShare:NO];
}

- (void) photoAction:(UIButton *)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Edit Photo" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    if (IS_IPad) {
        alertController = [UIAlertController alertControllerWithTitle:@"Edit Photo" message:nil preferredStyle: UIAlertControllerStyleAlert];
    }
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Change" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self addPhoto];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
    
}

- (void)addPhoto {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setphoto"];
    
    UIActivityIndicatorView *indicatorView = [headBt viewWithTag:120];
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(SelectPhotoManager * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        
        __strong __typeof(self) strongSelf = weakSelf;
        self->getPlaceHolderImg = resizeImage;
        [strongSelf->headBt setImage:resizeImage forState:UIControlStateNormal];
        [indicatorView startAnimating];
        
        NSData *compressResizeImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:resizeImage], 0.25);
        NSMutableArray *dataArray = [NSMutableArray arrayWithObjects:compressResizeImageData,  nil];
        [[NetTool shareInstance] putDatas:dataArray fileType:@"jpg" needShowHUD:YES callback:^(id  _Nullable obj) {
            
            NSMutableArray *getArray = obj;
            strongSelf->headImgUrlString = getArray.firstObject;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [strongSelf->headBt sd_setImageWithURL:[NSURL URLWithString:getArray.firstObject] forState:UIControlStateNormal placeholderImage:strongSelf->getPlaceHolderImg];
                [indicatorView stopAnimating];
                [DataTool showBannerViewWithTitle:@"Need To Be Review"];
            });
        }];
    };
}

- (UIView *) createFuntionViewWithFrame:(CGRect) frame andTitleString:(NSString *) titleString {
    UIView *getView = [[UIView alloc] initWithFrame:frame];
    
    UIView *tipView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(10))];
    tipView.backgroundColor = RGB(30, 34, 35);
    [getView addSubview:tipView];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(20), ZoomSize(200), ZoomSize(25))];
    [getView addSubview:titleLb];
    titleLb.text = titleString;
    titleLb.textColor = WhiteColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    
    return getView;
}

- (UIButton *) createChooseBt:(CGRect) frame andTitleString:(NSString *) titleString andBtTag:(int) btTag {
    
    UIButton *chooseBt = [[UIButton alloc] initWithFrame:frame];
    [chooseBt setTitle:titleString forState:UIControlStateNormal];
    [chooseBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    [chooseBt addTarget:self action:@selector(chooseAction:) forControlEvents:UIControlEventTouchUpInside];
    chooseBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
    chooseBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
    chooseBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(15), 0, 0);
    chooseBt.tag = btTag;
    
    UIView *tipView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height, SCREEN_Width, ZoomSize(1))];
    tipView.backgroundColor = RGB(38, 44, 45);
    [chooseBt addSubview:tipView];
    
    UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake( frame.size.width - ZoomSize(23), (frame.size.height - ZoomSize(13))/2, ZoomSize(8), ZoomSize(13))];
    tipIV.image = [UIImage imageNamed:@"nav_more"];
    [chooseBt addSubview:tipIV];
    
    UILabel *setInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake( frame.size.width - ZoomSize(180), (frame.size.height - ZoomSize(25))/2, ZoomSize(150), ZoomSize(25))];
    [chooseBt addSubview:setInfoLabel];
    setInfoLabel.textAlignment = NSTextAlignmentRight;
    setInfoLabel.textColor = WhiteColor;
    setInfoLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
    setInfoLabel.tag = btTag + 10;
    
    return chooseBt;
}

- (void) chooseAction:(UIButton *) sender {
    
    [self.view endEditing:YES];
    
    switch (sender.tag) {
        case 100: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setIdentity"];
            [self setChooseLabelWithArray:IdentityArray andInfoLabel:myIdentityLb];
        }
            break;
        case 200: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setRole"];
            [self setChooseLabelWithArray:RoleTypeArray andInfoLabel:myRoleLb];
        }
            break;
        case 300: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setBirthDay"];
            [self setChooseLabelWithArray:AgeArray andInfoLabel:myBirthLb];
            
        }
            break;
        case 400: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setHeight"];
            [self setChooseLabelWithArray:HeightArray andInfoLabel:myHeightLb];
        }
            break;
        case 500: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setBodyType"];
            [self setChooseLabelWithArray:BodyTypeArray andInfoLabel:myBodyTypeLb];
        }
            break;
        case 600: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setLanguage"];
            [self setChooseLabelWithArray:LanguageArray andInfoLabel:myLanguageLb];
        }
            break;
        case 700: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setEnth"];
            [self setChooseLabelWithArray:EthnicityArray andInfoLabel:myEnthnicityLb];
        }
            break;
        case 800: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setLookfor"];
            [self setChooseLabelWithArray:LookforArray andInfoLabel:myLookingforLb];
        }
            break;
        case 900: {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"Edit_setLocation"];
            [[UserInfo shareInstant] setLocationInfo];
            [UserInfo shareInstant].addressCallBack = ^(id  _Nullable obj) {
                NSDictionary *addressDic = obj;
                if (![DataTool isEmptyString:addressDic[@"addressString"]]) {
                    self->myAddressLb.text = addressDic[@"addressString"];
                    
                    NSArray *addressArray = addressDic[@"addressArray"];
                    self->getLatitude = addressArray.firstObject;
                    self->getLongitude = addressArray.lastObject;
                } else {
                    self->myAddressLb.text = @"No address";
                    self->getLatitude = @"0";
                    self->getLongitude = @"0";
                }
            };
        }
            break;
        default:
            break;
    }
    
}

- (void) setChooseLabelWithArray:(NSArray *) chooseArray andInfoLabel:(UILabel *) infoLabel {
    
    JKPickView *pickview = [[JKPickView alloc] initWithFrame:self.view.frame];
    [pickview.dataArray addObjectsFromArray:chooseArray];
    NSInteger defaultIndex = 0;
    if ([chooseArray isEqualToArray:HeightArray]) {
        defaultIndex = 35; //height默认 175
    }
    if (![DataTool isEmptyString:infoLabel.text]) {
        defaultIndex = [chooseArray indexOfObject:infoLabel.text];
    }
    
    pickview.selectedRow = [NSString stringWithFormat:@"%ld", (long)defaultIndex];
    pickview.back = ^(NSInteger getIndex) {
        infoLabel.text = chooseArray[getIndex];
    };
    [pickview show];
}

- (void) textFieldChange :(UITextField *) sender {
    if (nameTF.text.length > 20) {
        nameTF.text = [nameTF.text substringToIndex:20];
    }
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView*)textView {
    if([textView.text isEqualToString:textPlaceHolder]) {
        textView.text = @"";
        textView.textColor = WhiteColor;
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    if(textView.text.length<1) {
        textView.text = textPlaceHolder;
        textView.textColor= TextGray;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *tempStr = textView.text;
    if (tempStr.length > textLength) {
        tempStr = [tempStr substringToIndex:textLength];
        textView.text = tempStr;
    }
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight, Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Edit";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
        UIButton *previewBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(75), StatusBarHeight , ZoomSize(60), ZoomSize(30))];
        [_navBgView addSubview:previewBt];
        [previewBt setTitle:@"preview" forState:UIControlStateNormal];
        previewBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [previewBt addTarget:self action:@selector(previewAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = BGBlackColor;
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, ZoomSize(1000));
        
#pragma mark - photoView
        UIView *photoView = [self createFuntionViewWithFrame:CGRectMake(0, ZoomSize(0) , SCREEN_Width, ZoomSize(300)) andTitleString:@"Photo"];
        [_contentScrollView addSubview:photoView];
        
        headBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(50), ZoomSize(160), ZoomSize(220))];
        [photoView addSubview:headBt];
        headBt.backgroundColor = RGB(38, 44, 45);
        [headBt setImage:[UIImage imageNamed:@"common_edit_add_black"] forState:UIControlStateNormal];
        [headBt addTarget:self action:@selector(photoAction:) forControlEvents:UIControlEventTouchUpInside];
        headBt.layer.cornerRadius = ZoomSize(4);
        headBt.layer.masksToBounds = YES;
        headBt.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [headBt.imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(130), ZoomSize(190), ZoomSize(25), ZoomSize(25))];
        [headBt addSubview:tipIV];
        tipIV.contentMode = UIViewContentModeScaleAspectFit;
        tipIV.image = [UIImage imageNamed:@"common_edit_replace"];
        tipIV.tag = 110;
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:headBt.bounds];
        [headBt addSubview:indicatorView];
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        indicatorView.tag = 120;
        
#pragma mark - basicInfoView
        UIView *basicInfoView = [self createFuntionViewWithFrame:CGRectMake(0, ZoomSize(300) , SCREEN_Width, ZoomSize(200)) andTitleString:@"Basic info"];
        [_contentScrollView addSubview:basicInfoView];
        
        nameTF = [[UITextField alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(60), SCREEN_Width - ZoomSize(15), ZoomSize(20))];
        [basicInfoView addSubview:nameTF];
        nameTF.font = [UIFont systemFontOfSize:ZoomSize(16)];
        nameTF.textColor = TextWhite;
        nameTF.tintColor = RGB(250, 180, 22);
        nameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes: @{NSForegroundColorAttributeName:RGB(133, 136, 137) }];
        [nameTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
        
        UIView *nameTFLineView = [[UIView alloc] init];
        nameTFLineView.frame = CGRectMake(ZoomSize(15), ZoomSize(85), SCREEN_Width - ZoomSize(30) , ZoomSize(1));
        [basicInfoView addSubview:nameTFLineView];
        nameTFLineView.backgroundColor = RGB(38, 44, 45);
        
        aboutTV = [[UITextView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(100), SCREEN_Width - ZoomSize(30), ZoomSize(80)) ];
        [basicInfoView addSubview:aboutTV];
        aboutTV.layer.borderColor = RGB(38, 44, 45).CGColor;
        aboutTV.layer.borderWidth = ZoomSize(1);
        aboutTV.layer.cornerRadius = ZoomSize(6);
        aboutTV.backgroundColor = BGBlackColor;
        aboutTV.tintColor = RGB(250, 180, 22);
        aboutTV.font = [UIFont systemFontOfSize:ZoomSize(16)];
        aboutTV.textColor = TextWhite;
        aboutTV.delegate = self;
        
        aboutTV.text = textPlaceHolder;
        aboutTV.textColor= TextGray;
        
#pragma mark - DetailInfoView
        UIView *detailInfoView = [self createFuntionViewWithFrame:CGRectMake(0, ZoomSize(500) , SCREEN_Width, ZoomSize(500)) andTitleString:@"Detail info"];
        [_contentScrollView addSubview:detailInfoView];
        
        UIView *myIdentityBt = [self createChooseBt:CGRectMake(0, ZoomSize(50), SCREEN_Width, ZoomSize(50)) andTitleString:@"My Identity" andBtTag:100];
        [detailInfoView addSubview:myIdentityBt];
        myIdentityLb = [myIdentityBt viewWithTag:110];
        
        UIView *myRoleBt = [self createChooseBt:CGRectMake(0, ZoomSize(100), SCREEN_Width, ZoomSize(50)) andTitleString:@"My Role" andBtTag:200];
        [detailInfoView addSubview:myRoleBt];
        myRoleLb = [myRoleBt viewWithTag:210];
        
        UIView *myBirthBt = [self createChooseBt:CGRectMake(0, ZoomSize(150), SCREEN_Width, ZoomSize(50)) andTitleString:@"Date Of Birth" andBtTag:300];
        [detailInfoView addSubview:myBirthBt];
        myBirthLb = [myBirthBt viewWithTag:310];
        
        UIView *myHeightBt = [self createChooseBt:CGRectMake(0, ZoomSize(200), SCREEN_Width, ZoomSize(50)) andTitleString:@"Height" andBtTag:400];
        [detailInfoView addSubview:myHeightBt];
        myHeightLb = [myHeightBt viewWithTag:410];
        
        UIView *myBodyTypeBt = [self createChooseBt:CGRectMake(0, ZoomSize(250), SCREEN_Width, ZoomSize(50)) andTitleString:@"Body Type" andBtTag:500];
        [detailInfoView addSubview:myBodyTypeBt];
        myBodyTypeLb = [myBodyTypeBt viewWithTag:510];
        
        UIView *myLanguageBt = [self createChooseBt:CGRectMake(0, ZoomSize(300), SCREEN_Width, ZoomSize(50)) andTitleString:@"Languages" andBtTag:600];
        [detailInfoView addSubview:myLanguageBt];
        myLanguageLb = [myLanguageBt viewWithTag:610];
        
        UIView *myEnthnicityBt = [self createChooseBt:CGRectMake(0, ZoomSize(350), SCREEN_Width, ZoomSize(50)) andTitleString:@"Enthnicity" andBtTag:700];
        [detailInfoView addSubview:myEnthnicityBt];
        myEnthnicityLb = [myEnthnicityBt viewWithTag:710];
        
        UIView *myLookingforBt = [self createChooseBt:CGRectMake(0, ZoomSize(400), SCREEN_Width, ZoomSize(50)) andTitleString:@"Looking For" andBtTag:800];
        [detailInfoView addSubview:myLookingforBt];
        myLookingforLb = [myLookingforBt viewWithTag:810];
        
        UIView *myAddressBt = [self createChooseBt:CGRectMake(0, ZoomSize(450), SCREEN_Width, ZoomSize(50)) andTitleString:@"Where I Live" andBtTag:900];
        [detailInfoView addSubview:myAddressBt];
        myAddressLb = [myAddressBt viewWithTag:910];
        
    }
    return _contentScrollView;
}

@end

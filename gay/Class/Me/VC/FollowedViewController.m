//
//  FollowedViewController.m
//  gay
//
//  Created by steve on 2020/10/29.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "FollowedViewController.h"
#import "FollowTableViewCell.h"

#import "MJRefresh.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"
#import "RelationUserDataModel.h"

static NSString * const reuseIdentifier = @"FollowTableViewCell";

@interface FollowedViewController ()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
//    int totalPages;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation FollowedViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.tableView];
}

- (void)setData {
    self.array = [NSMutableArray array];
    [self getHeaderData];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getHeaderData {
    self->pageIndex = 1;
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Followed_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Followed_down"];
}

- (void)requestData {
    
    NSDictionary *paramsDic =  @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:pageIndex], @"userId":_userId };
    NSString *urlString = API_followingList;
//    if ([_userId isEqualToString:[UserInfo shareInstant].user.userId]) {
//        paramsDic = @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:pageIndex] };
//        urlString = API_user_relation_like;
//    }
    
    [[NetTool shareInstance] startRequest:urlString method:GetMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
        }
        self->pageIndex ++;
        NSArray *userArray = [RelationUserDataModel mj_objectArrayWithKeyValuesArray:obj];
        [self.array addObjectsFromArray:userArray];

        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];

    
    
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FollowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(cell == nil) {
        cell = [[FollowTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    RelationUserDataModel *dataModel = self.array[indexPath.row];
    dataModel.relationType = @"follow";
    cell.relationDataModel = dataModel;
    
    if ([_userId isEqualToString:[UserInfo shareInstant].user.userId]) {
        cell.needHideBt = NO;
    } else {
        cell.needHideBt = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Followed_openUserProfile"];
    
    RelationUserDataModel *dataModel = self.array[indexPath.row];
    
    if ([DataTool checkUserIsShare:dataModel.userId]) {
        UserProfileDataModel *userProfileDataModel = [[UserProfileDataModel alloc]init];
        userProfileDataModel.userId = dataModel.userId;
        userProfileDataModel.nickName = dataModel.nickName;
        userProfileDataModel.headImgUrl = dataModel.headImgUrl;
        [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:YES];
        
    } else {
        NSDictionary *userParams = @{@"id":dataModel.userId};
        [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:NO];
        }];
    }
    
}


#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight , Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Follower";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
    }
    return _navBgView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.backgroundColor = BGBlackColor;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
        _tableView.rowHeight = ZoomSize(90);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
        
    }
    return _tableView;
}

@end

//
//  FollowingViewController.h
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FollowingViewController : UIViewController

@property (nonatomic, strong) NSString *userId;

@end

NS_ASSUME_NONNULL_END

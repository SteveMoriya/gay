//
//  FeedbackViewController.m
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "FeedbackViewController.h"

#define textLength 250
#define textPlaceHolder @"Please write your feedback..."

@interface FeedbackViewController ()<UITextViewDelegate>{
    UITextView *feedbackTV;
    UILabel *feedbackTipLb;
    UILabel *textNumLb;
    
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;


@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Feedback_submit"];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    NSDictionary *paramsDic = @{@"content":feedbackTV.text, @"phoneSystem":app_version };
    [[NetTool shareInstance] startRequest:API_feedback method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        NSLog(@"obj %@", obj);
        [DataTool showToast:@"submit success"];
        [self backAction];
    }];
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView*)textView {
    if([textView.text isEqualToString:textPlaceHolder]) {
        textView.text = @"";
        textView.textColor = WhiteColor;
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    if(textView.text.length<1) {
        textView.text = textPlaceHolder;
        textView.textColor= TextGray;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *tempStr = textView.text;
    if (tempStr.length > textLength) {
        tempStr = [tempStr substringToIndex:textLength];
        textView.text = tempStr;
    }
    
    textNumLb.text = [NSString stringWithFormat:@"( %lu/%d )", + textView.text.length, textLength];
    if (textView.text.length > 0) {
        [self->continueBt setBackgroundColor:ThemeYellow];
        self->continueBt.enabled = YES;
    } else {
        [self->continueBt setBackgroundColor:TextGray];
        self->continueBt.enabled = NO;
    }
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight ,Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Feedback";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = BGBlackColor;
        
        feedbackTV = [[UITextView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(31), SCREEN_Width - ZoomSize(30), ZoomSize(201)) ];
        [_contentScrollView addSubview:feedbackTV];
        feedbackTV.layer.borderColor = RGB(120, 120, 120).CGColor;
        feedbackTV.layer.borderWidth = ZoomSize(1);
        feedbackTV.layer.cornerRadius = ZoomSize(6);
        feedbackTV.backgroundColor = BGBlackColor;
        feedbackTV.tintColor = RGB(250, 180, 22);
        feedbackTV.font = [UIFont systemFontOfSize:ZoomSize(16)];
        feedbackTV.textContainerInset = UIEdgeInsetsMake(ZoomSize(15), ZoomSize(15), ZoomSize(15), ZoomSize(15));
        feedbackTV.delegate = self;
        
        feedbackTV.text = textPlaceHolder;
        feedbackTV.textColor= TextGray;
        
        
        textNumLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(115), ZoomSize(250) , ZoomSize(90), ZoomSize(20))];
        [_contentScrollView addSubview:textNumLb];
        textNumLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        textNumLb.text = [NSString stringWithFormat:@"0/%d", textLength];
        textNumLb.textColor = TextGray;
        textNumLb.textAlignment = NSTextAlignmentRight;
        
        continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), ZoomSize(313),Screen_Width - ZoomSize(72), ZoomSize(48)) ];
        [_contentScrollView addSubview:continueBt];
        continueBt.layer.cornerRadius = ZoomSize(24);
        [continueBt setTitle:@"Submit" forState:UIControlStateNormal];
        [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        continueBt.backgroundColor = TextGray;
        [continueBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
        continueBt.enabled = NO;
        
    }
    return _contentScrollView;
}

@end

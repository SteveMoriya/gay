//
//  MyLikeListViewController.m
//  gay
//
//  Created by steve on 2020/10/23.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "MyLikeListViewController.h"
#import "UserInfoCollectionViewCell.h"

#import "MJRefresh.h"

#import "UICollectionView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"
#import "RelationUserDataModel.h"

static NSString * const reuseIdentifier = @"UserInfoCollectionViewCell";

@interface MyLikeListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource> {
    int pageIndex;
//    int totalPages;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation MyLikeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.collectionView];
}

- (void)setData {
    self.array = [NSMutableArray array];
    [self getHeaderData];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getHeaderData {
    self->pageIndex = 1;
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"MyLike_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"MyLike_down"];
}

- (void)requestData {
    
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:pageIndex] };
    
    [[NetTool shareInstance] startRequest:API_user_relationILove method:GetMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
        }
        
        self->pageIndex ++;
        NSArray *userArray = [RelationUserDataModel mj_objectArrayWithKeyValuesArray:obj];
        [self.array addObjectsFromArray:userArray];
        [self.collectionView reloadData];
        
    }];
    
}

#pragma mark - deleDate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UserInfoCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    RelationUserDataModel *dataModel = self.array[indexPath.row];
    cell.relationDataModel = dataModel;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"MyLike_openUserProfile"];
    
    RelationUserDataModel *dataModel = self.array[indexPath.row];
    
    if ([DataTool checkUserIsShare:dataModel.userId]) {
        UserProfileDataModel *userProfileDataModel = [[UserProfileDataModel alloc]init];
        userProfileDataModel.userId = dataModel.userId;
        userProfileDataModel.nickName = dataModel.nickName;
        userProfileDataModel.headImgUrl = dataModel.headImgUrl;
        [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:YES];
        
    } else {
        NSDictionary *userParams = @{@"id":dataModel.userId};
        [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:NO];
        }];
    }
    
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight, Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"My likes";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
    }
    return _navBgView;
}

- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        
        flowLayout.minimumInteritemSpacing = ZoomSize(15);
        flowLayout.minimumLineSpacing = ZoomSize(15);
        flowLayout.sectionInset = UIEdgeInsetsMake(ZoomSize(15), ZoomSize(15), ZoomSize(15), ZoomSize(15));
        flowLayout.itemSize = CGSizeMake( (SCREEN_Width - ZoomSize(48))/2.0, (SCREEN_Width - ZoomSize(35))/1.5);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight ) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = BGBlackColor;
        [_collectionView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO ;
        _collectionView.showsVerticalScrollIndicator = NO ;
        
        _collectionView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];

        _collectionView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        _collectionView.enablePlaceHolderView = YES;
        _collectionView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_collectionView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
    }
    return _collectionView;
}


@end

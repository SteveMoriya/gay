//
//  ActivitesJoinUserCollectionViewCell.h
//  gay
//
//  Created by steve on 2020/11/2.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ActivitesJoinUserDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivitesJoinUserCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) ActivitesJoinUserDataModel *joinUserDataModel;

@end

NS_ASSUME_NONNULL_END

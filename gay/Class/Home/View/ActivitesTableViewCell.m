//
//  ActivitesTableViewCell.m
//  gay
//
//  Created by steve on 2020/10/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ActivitesTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ActivitesTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *event_IV;

@end

@implementation ActivitesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setActivitesModel:(ActivitesDataModel *)activitesModel {
    _activitesModel = activitesModel;
    
    [_event_IV sd_setImageWithURL:[NSURL URLWithString:activitesModel.imgUrl] placeholderImage:[UIImage imageNamed:@"events_image"]];
    
}

@end

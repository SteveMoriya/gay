//
//  ActivitesTableViewCell.h
//  gay
//
//  Created by steve on 2020/10/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivitesDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivitesTableViewCell : UITableViewCell

@property (nonatomic, strong) ActivitesDataModel *activitesModel;

@end

NS_ASSUME_NONNULL_END

//
//  ActivitesJoinUserCollectionViewCell.m
//  gay
//
//  Created by steve on 2020/11/2.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ActivitesJoinUserCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ActivitesJoinUserCollectionViewCell() {
}

@property (weak, nonatomic) IBOutlet UIImageView *headIV;

@end

@implementation ActivitesJoinUserCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _headIV.layer.cornerRadius = ZoomSize(20);
}

- (void)setJoinUserDataModel:(ActivitesJoinUserDataModel *)joinUserDataModel {
    
    _joinUserDataModel = joinUserDataModel;
    
    [_headIV sd_setImageWithURL:[NSURL URLWithString:joinUserDataModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    
}

@end

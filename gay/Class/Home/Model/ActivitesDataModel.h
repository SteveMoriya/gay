//
//  ActivitesDataModel.h
//  gay
//
//  Created by steve on 2020/10/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActivitesJoinUserDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivitesDataModel : NSObject

@property (nonatomic, strong) NSString *activityEndTime;
@property (nonatomic, strong) NSString *activityStartTime;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *activitesId;
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSString *isJoin;

@property (nonatomic, strong) NSMutableArray *joinUsers;
@property (nonatomic, strong) NSString *title;

@end

NS_ASSUME_NONNULL_END

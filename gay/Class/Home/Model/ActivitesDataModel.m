//
//  ActivitesDataModel.m
//  gay
//
//  Created by steve on 2020/10/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ActivitesDataModel.h"

@implementation ActivitesDataModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"activitesId":@"id"};
}

+ (NSDictionary *)mj_objectClassInArray {
    return @{
        @"joinUsers":[ActivitesJoinUserDataModel class]
    };
}

@end

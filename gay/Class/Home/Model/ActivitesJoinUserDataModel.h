//
//  ActivitesJoinUserDataModel.h
//  gay
//
//  Created by steve on 2020/11/2.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivitesJoinUserDataModel : NSObject

@property (nonatomic, strong) NSString *activityId;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *headImgUrl;

@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *userId;


@end

NS_ASSUME_NONNULL_END

//
//  ActivitesCountryViewController.m
//  gay
//
//  Created by steve on 2020/11/5.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ActivitesCountryViewController.h"

#import "TagListView.h"
#import "TagView.h"

@interface ActivitesCountryViewController () {
    TagListView *currentLocationTagListView;
    TagListView *hotCountriesTagListView;
    
//    NSMutableArray *tagArray;
    NSString *tagString;
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation ActivitesCountryViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    
//    tagArray = [NSMutableArray array];
    
    tagString = @"";
    [[currentLocationTagListView addTag:@"United States"] setOnTap:^(TagView *tagView) {
        
        for(TagView *tagView in [self->hotCountriesTagListView tagViews]) {
            tagView.borderWidth = 1;
            tagView.textColor = RGB(214, 215, 215);
            tagView.backgroundColor = [UIColor clearColor];
        }
        
        tagView.borderWidth = 0;
        tagView.textColor = UIColor.whiteColor;
        tagView.backgroundColor = ThemeYellow;
        
//        [self->tagArray removeAllObjects];
//        [self->tagArray addObject:tagView.titleLabel.text];
        
        self->tagString = tagView.titleLabel.text;
        
        [self->continueBt setBackgroundColor:ThemeYellow];
        self->continueBt.enabled = YES;
    }];
    
    for (NSString *textString in HotCountriesArray) {
        [[hotCountriesTagListView addTag:textString] setOnTap:^(TagView *tagView) {
            
            for(TagView *tagView in [self->currentLocationTagListView tagViews]) {
                tagView.borderWidth = 1;
                tagView.textColor = RGB(214, 215, 215);
                tagView.backgroundColor = [UIColor clearColor];
            }
            
            [self setTagViewStyle:self->hotCountriesTagListView];
            
            tagView.borderWidth = 0;
            tagView.textColor = UIColor.whiteColor;
            tagView.backgroundColor = ThemeYellow;
            
//            [self->tagArray removeAllObjects];
//            [self->tagArray addObject:tagView.titleLabel.text];
            
            self->tagString = tagView.titleLabel.text;
            
            [self->continueBt setBackgroundColor:ThemeYellow];
            self->continueBt.enabled = YES;
        }];
    }
    
}

#pragma mark - Function

- (void)backAction {
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)continueAction {
    NSLog(@"continueAction");
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ActivitesCountry_continue"];
    
    [self backAction];
    
    if (_getCountryBlock) {
        if (![DataTool isEmptyString:tagString]) {
            _getCountryBlock(tagString);
        }
    }
}

- (void) setTagViewStyle:(TagListView *)tagListView {

    for(TagView *tagView in [tagListView tagViews]) {
        tagView.borderWidth = 1;
        tagView.textColor = RGB(214, 215, 215);
        tagView.backgroundColor = [UIColor clearColor];
    }
}


#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navBgView.backgroundColor = RGB(38, 44, 45);
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(8), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(50), StatusBarHeight , Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Select Countriy";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = RGB(38, 44, 45);
//        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, ZoomSize(850));
        
        
//        UILabel *currentLocationTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(20), ZoomSize(175), ZoomSize(20))];
//        [_contentScrollView addSubview:currentLocationTipLb];
//        currentLocationTipLb.text = @"Current location";
//        currentLocationTipLb.textColor = WhiteColor;
//        currentLocationTipLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
//
//        UIView *currentLocationLineView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(50), SCREEN_Width - ZoomSize(30) , ZoomSize(1))];
//        [_contentScrollView addSubview:currentLocationLineView];
//        currentLocationLineView.backgroundColor = RGB(54, 62, 63);
//
//
//        currentLocationTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(70), ZoomSize(345), ZoomSize(40))];
//        [_contentScrollView addSubview:currentLocationTagListView];
//        currentLocationTagListView.cornerRadius = ZoomSize(6);
//        currentLocationTagListView.borderWidth = ZoomSize(1);
//        currentLocationTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
//        currentLocationTagListView.paddingX = ZoomSize(12);
//        currentLocationTagListView.paddingY = ZoomSize(6);
//        currentLocationTagListView.marginX = ZoomSize(8);
//        currentLocationTagListView.marginY = ZoomSize(12);
//        currentLocationTagListView.tagViewHeight = ZoomSize(30);
//        currentLocationTagListView.textColor = RGB(214, 215, 215);
//        currentLocationTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        
        UILabel *hotCountriesTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(20), ZoomSize(175), ZoomSize(20))];
        [_contentScrollView addSubview:hotCountriesTipLb];
        hotCountriesTipLb.text = @"Hot countires ";
        hotCountriesTipLb.textColor = WhiteColor;
        hotCountriesTipLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        
        UIView *hotCountriesLineView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(50), SCREEN_Width - ZoomSize(30) , ZoomSize(1))];
        [_contentScrollView addSubview:hotCountriesLineView];
        hotCountriesLineView.backgroundColor = RGB(54, 62, 63);
        
        hotCountriesTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(70), Screen_Width - ZoomSize(30), ZoomSize(240))];
        [_contentScrollView addSubview:hotCountriesTagListView];
        hotCountriesTagListView.cornerRadius = ZoomSize(6);
        hotCountriesTagListView.borderWidth = ZoomSize(1);
        hotCountriesTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
        hotCountriesTagListView.paddingX = ZoomSize(6);
        hotCountriesTagListView.paddingY = ZoomSize(8);
        hotCountriesTagListView.marginX = ZoomSize(6);
        hotCountriesTagListView.marginY = ZoomSize(8);
        hotCountriesTagListView.tagViewHeight = ZoomSize(30);
        hotCountriesTagListView.textColor = RGB(214, 215, 215);
        hotCountriesTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        
        continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), ZoomSize(500), Screen_Width - ZoomSize(72), ZoomSize(48)) ];
        [_contentScrollView addSubview:continueBt];
        continueBt.layer.cornerRadius = ZoomSize(24);
        [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
        [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        continueBt.backgroundColor = TextGray;
        [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
        continueBt.enabled = NO;
        
    }
    return _contentScrollView;
}


@end

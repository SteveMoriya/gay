//
//  ActivitesCountryViewController.h
//  gay
//
//  Created by steve on 2020/11/5.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivitesCountryViewController : UIViewController

@property (nonatomic, copy) CallBack getCountryBlock;

@end

NS_ASSUME_NONNULL_END

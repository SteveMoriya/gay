//
//  ActivitesDetailViewController.m
//  gay
//
//  Created by steve on 2020/10/24.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ActivitesDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "ActivitesJoinUserCollectionViewCell.h"
#import "ActivitesJoinUserDataModel.h"

#import "UICollectionView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"ActivitesJoinUserCollectionViewCell";

@interface ActivitesDetailViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>{
    UIButton *joinBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation ActivitesDetailViewController


- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    
    if ([self.activitesModel.isJoin isEqualToString:@"1"]) {
        [joinBt setTitle:@"Leave" forState:UIControlStateNormal];
    } else {
        [joinBt setTitle:@"Join" forState:UIControlStateNormal];
    }
    
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) joinAction {
    
    NSDictionary *paramsDic = @{@"activityId": self.activitesModel.activitesId };
    
    if ([self.activitesModel.isJoin isEqualToString:@"1"]) {
        [[NetTool shareInstance] sendFireBaseWithEventName:@"ActivitesDetail_join"];
        
        [[NetTool shareInstance] startRequest:API_activitiesJoinUsers_leave method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            
            self.activitesModel.isJoin = @"0";
            [self->joinBt setTitle:@"Join" forState:UIControlStateNormal];
            
            BOOL needDelete = false;
            NSInteger needDeleteIndex = 0;
            for (ActivitesJoinUserDataModel *userModel in self.activitesModel.joinUsers) {
                if ([userModel.userId isEqualToString:[UserInfo shareInstant].user.userId ]) {
                    needDelete = YES;
                    needDeleteIndex = [self.activitesModel.joinUsers indexOfObject:userModel];
                    break;
                }
            }
            
            if (needDelete) {
                [self.activitesModel.joinUsers removeObjectAtIndex:needDeleteIndex];
                [self.collectionView reloadData];
            }
        }];
        
    } else {
        [[NetTool shareInstance] sendFireBaseWithEventName:@"ActivitesDetail_leave"];
        
        [[NetTool shareInstance] startRequest:API_activitiesJoinUsers_join method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            
            self.activitesModel.isJoin = @"1";
            [self->joinBt setTitle:@"Leave" forState:UIControlStateNormal];
            
            ActivitesJoinUserDataModel *userModel = [[ActivitesJoinUserDataModel alloc] init];
            userModel.userId = [UserInfo shareInstant].user.userId;
            userModel.headImgUrl = [UserInfo shareInstant].user.headImgUrl;
            
            [self.activitesModel.joinUsers insertObject:userModel atIndex:0];
            [self.collectionView reloadData];
        }];
    }
    
}

#pragma mark - deleDate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.activitesModel.joinUsers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ActivitesJoinUserCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    ActivitesJoinUserDataModel *userModel = self.activitesModel.joinUsers[indexPath.row];
    cell.joinUserDataModel = userModel;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"ActivitesDetail_userProfile"];
    
    ActivitesJoinUserDataModel *userModel = self.activitesModel.joinUsers[indexPath.row];    
    NSDictionary *userParams = @{@"id":userModel.userId};
    [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:NO];
    }];
    
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight , Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = self.activitesModel.title;
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        titleLb.adjustsFontSizeToFitWidth = YES;
        
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        
        UIImageView *activitesIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(8), SCREEN_Width - ZoomSize(30), Screen_Width * (160.0/340.0) )];
        [_contentScrollView addSubview:activitesIV];
        activitesIV.layer.masksToBounds = YES;
        activitesIV.layer.cornerRadius = ZoomSize(6);
        [activitesIV sd_setImageWithURL:[NSURL URLWithString:_activitesModel.imgUrl] placeholderImage:[UIImage imageNamed:@"events_image"]];
        activitesIV.contentMode = UIViewContentModeScaleAspectFill;
        
        UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(15), CGRectGetMaxY(activitesIV.frame) + ZoomSize(10), ZoomSize(17), ZoomSize(16))];
        [_contentScrollView addSubview:tipIV];
        tipIV.image = [UIImage imageNamed:@"events_tip"];
        tipIV.contentMode = UIViewContentModeScaleAspectFill;

        UILabel *addressLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(38), CGRectGetMaxY(activitesIV.frame) + ZoomSize(10) , Screen_Width - ZoomSize(60), ZoomSize(16))];
        [_contentScrollView addSubview:addressLb];
        addressLb.text = self.activitesModel.address;
        addressLb.textColor = WhiteColor;
//        addressLb.textAlignment = NSTextAlignmentRight;
        addressLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        
        UILabel *timeLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), CGRectGetMaxY(addressLb.frame) + ZoomSize(10) , Screen_Width - ZoomSize(30), ZoomSize(16))];
        [_contentScrollView addSubview:timeLb];
        timeLb.text = [NSString stringWithFormat:@"%@ %@", self.activitesModel.title, @"2-9 DEC"];
        timeLb.textColor = WhiteColor;
        timeLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        
        CGFloat contentHeight = [DataTool getTextHeightWithFont:[UIFont systemFontOfSize:18] getTextString:self.activitesModel.content getFrameWidth:Screen_Width - ZoomSize(30)] + ZoomSize(20);
        
        UILabel *contentLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), CGRectGetMaxY(timeLb.frame) + ZoomSize(10), Screen_Width - ZoomSize(30), contentHeight)];
        contentLb.text = self.activitesModel.content;
        contentLb.textColor = WhiteColor;
        contentLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        contentLb.numberOfLines = 0;
        [_contentScrollView addSubview:contentLb];
        
        UILabel *groupTipLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(30), CGRectGetMaxY(contentLb.frame) + ZoomSize(20) , Screen_Width - ZoomSize(60), ZoomSize(20))];
        [_contentScrollView addSubview:groupTipLb];
        groupTipLb.text = @"EVENT GROUP";
        groupTipLb.textColor = WhiteColor;
        groupTipLb.textAlignment = NSTextAlignmentCenter;
        groupTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        
        UIView *groupView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), CGRectGetMaxY(groupTipLb.frame) + ZoomSize(10), SCREEN_Width - ZoomSize(30), ZoomSize(230))];
        [_contentScrollView addSubview:groupView];
        groupView.layer.borderColor = RGB(120, 120, 120).CGColor;
        groupView.layer.borderWidth = ZoomSize(1);
        groupView.layer.cornerRadius = ZoomSize(6);
        [groupView addSubview:self.collectionView];
        
        joinBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - ZoomSize(140), ZoomSize(185), ZoomSize(95), ZoomSize(37)) ];
        [groupView addSubview:joinBt];
        joinBt.layer.cornerRadius = ZoomSize(18.5);
        [joinBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        joinBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
        joinBt.backgroundColor = RGB(250, 180, 22);
        [joinBt addTarget:self action:@selector(joinAction) forControlEvents:UIControlEventTouchUpInside];
        
//        重试显示大小
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(groupView.frame) + ZoomSize(20));
        
    }
    return _contentScrollView;
}

- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        
        flowLayout.minimumInteritemSpacing = ZoomSize(5);
        flowLayout.minimumLineSpacing = ZoomSize(5);
        flowLayout.sectionInset = UIEdgeInsetsMake(ZoomSize(5), ZoomSize(5), ZoomSize(5), ZoomSize(5));
        flowLayout.itemSize = CGSizeMake( ZoomSize(40), ZoomSize(40));
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width - ZoomSize(30), ZoomSize(180) ) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = BGBlackColor;
        [_collectionView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO ;
        _collectionView.showsVerticalScrollIndicator = NO ;
        
//        _collectionView.enablePlaceHolderView = YES;
//        _collectionView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_collectionView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
        
    }
    return _collectionView;
}

@end

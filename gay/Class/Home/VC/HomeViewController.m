//
//  HomeViewController.m
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "HomeViewController.h"
#import "ActivitesTableViewCell.h"
#import "ActivitesDetailViewController.h"
#import "ActivitesCountryViewController.h"
#import "ChatRoomViewController.h"

#import "MJRefresh.h"
#import "ActivitesDataModel.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"ActivitesTableViewCell";

@interface HomeViewController ()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
    NSString *countryString;
    
    UIButton *chatRoomBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation HomeViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ChatRoom]) { //使用存储的关键字信息
        chatRoomBt.hidden = YES;
    } else {
        chatRoomBt.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.tableView];
    
    chatRoomBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(166), Screen_Height - TabBarHeight - ZoomSize(80), ZoomSize(166), ZoomSize(58))];
    [self.view addSubview:chatRoomBt];
    [chatRoomBt addTarget:self action:@selector(chatRoomAction) forControlEvents:UIControlEventTouchUpInside];
    [chatRoomBt setBackgroundImage:[UIImage imageNamed:@"message_group_chat"] forState:UIControlStateNormal];
    chatRoomBt.hidden = YES;
    
    UILabel *chatRoomTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(16), ZoomSize(100), ZoomSize(20))];
    [chatRoomBt addSubview:chatRoomTitleLb];
    chatRoomTitleLb.textColor = WhiteColor;
    chatRoomTitleLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
    chatRoomTitleLb.text = @"ChatRoom";
    
    UIImageView *chatRoomUserIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(120), ZoomSize(12), ZoomSize(30), ZoomSize(30))];
    [chatRoomBt addSubview:chatRoomUserIV];
    chatRoomUserIV.image = [UIImage imageNamed:@"message_fuzzy_portrait"];
    chatRoomUserIV.layer.cornerRadius = ZoomSize(15);
    chatRoomUserIV.layer.masksToBounds = YES;
    chatRoomUserIV.layer.borderWidth = ZoomSize(1);
    chatRoomUserIV.layer.borderColor = WhiteColor.CGColor;
}

- (void)setData {
    self.array = [NSMutableArray array];
    [self setDefaultData];
    [self getHeaderData];
}

#pragma function

- (void) chatRoomAction {
    NSLog(@"chatRoomAction");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Message_chatRoom"];
    
    [[V2TIMManager sharedInstance] joinGroup:ChatRoomId msg:@"hi" succ:^{
        TIMConversation *conv = [[TIMManager sharedInstance] getConversation:2 receiver:ChatRoomId];
        ChatRoomViewController *vc = [[ChatRoomViewController alloc] initWithConversation:conv];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    } fail:^(int code, NSString *desc) {
        TIMConversation *conv = [[TIMManager sharedInstance] getConversation:2 receiver:ChatRoomId];
        ChatRoomViewController *vc = [[ChatRoomViewController alloc] initWithConversation:conv];
        [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];
    
}

- (void) setDefaultData {
    self->pageIndex = 1;
    countryString = @"";
}

- (void)getHeaderData {
    self->pageIndex = 1;
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Home_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Home_down"];
}

- (void)requestData {
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:pageIndex] ,@"country":countryString };
    
    [[NetTool shareInstance] startRequest:API_activities_list method:GetMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
        }
        
        self->pageIndex ++;
        
        NSDictionary *responseDic = obj;
        NSArray *userArray = [ActivitesDataModel mj_objectArrayWithKeyValuesArray:responseDic[@"list"]];
        [self.array addObjectsFromArray:userArray];

        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];
}

- (void)filterAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Home_filter"];
    
    ActivitesCountryViewController *vc = [[ActivitesCountryViewController alloc] init];
    vc.getCountryBlock = ^(id  _Nullable obj) {
        [self setDefaultData];
        self->countryString = obj;
        [self requestData];
    };
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActivitesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(cell == nil) {
        cell = [[ActivitesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    ActivitesDataModel *activitesModel = self.array[indexPath.row];
    cell.activitesModel = activitesModel;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ActivitesDataModel *activitesModel = self.array[indexPath.row];
    ActivitesDetailViewController *vc = [[ActivitesDetailViewController alloc] init];
    vc.activitesModel = activitesModel;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navBgView.backgroundColor = BGBlackColor;
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), StatusBarHeight, ZoomSize(175), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Events & Pride";
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(26) weight:UIFontWeightMedium];
        
        UIButton *filterBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(45), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:filterBt];
        [filterBt setImage:[UIImage imageNamed:@"events_notice"] forState:UIControlStateNormal];
        [filterBt addTarget:self action:@selector(filterAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navBgView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.backgroundColor = BGBlackColor;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
        _tableView.rowHeight = Screen_Width * (156.0/345.0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self setDefaultData];
            [self getHeaderData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        ListPlaceHoderActionView *listPlaceHolderActionView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"No Data" tipsInfo:@"" btTitle:@"Search again"];
        listPlaceHolderActionView.actionBlock = ^{
            [self setDefaultData];
            [self getHeaderData];
        };
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = listPlaceHolderActionView;
        
    }
    return _tableView;
}


#pragma function


@end

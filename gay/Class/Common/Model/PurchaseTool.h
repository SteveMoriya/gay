//
//  PurchaseTool.h
//  gay
//
//  Created by steve on 2020/10/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseTool : NSObject

+ (instancetype)shareInstance;

- (void) addTranObserver;
- (void) startCreateOrderWithPID:(NSString *)productIdentifier;

- (void) restorePurchase :(BOOL) needHub;
//- (void) resendPaymentTransaction;

@end

NS_ASSUME_NONNULL_END

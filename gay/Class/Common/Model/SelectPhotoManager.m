//
//  SelectPhotoManager.m
//  gay
//
//  Created by steve on 2020/9/16.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "SelectPhotoManager.h"
#import "EditPhotoViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface SelectPhotoManager() {
    UIImagePickerController *ipVC;
}

@end

@implementation SelectPhotoManager

- (void)startSelectPhotoWithImageName:(NSString *)imageName{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Photo" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    if (IS_IPad) {
        alertController = [UIAlertController alertControllerWithTitle:@"Select Photo" message:nil preferredStyle: UIAlertControllerStyleAlert];
    }
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Take a Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoWithType:0];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Choose from Library" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoWithType:1];
    }]];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    
    [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
}

- (void)openPhoto {
    [self selectPhotoWithType:0];
}

- (void)chooseLibrary {
    [self selectPhotoWithType:1];
}

#pragma mark function
-(void)selectPhotoWithType:(int)type {
    
    ipVC = [[UIImagePickerController alloc] init];
    ipVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    ipVC.preferredContentSize = CGSizeMake(SCREEN_Width,  SCREEN_Width);
    ipVC.delegate = self;
    
    if (type == 0) {
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
        if (!isCamera) {
            return ;
        }else{
            ipVC.sourceType = UIImagePickerControllerSourceTypeCamera;
            ipVC.cameraDevice = UIImagePickerControllerCameraDeviceFront;// 此处是关键。
        }
    }else{
        ipVC.sourceType = UIImagePickerControllerCameraCaptureModePhoto;
        
    }
    [self presentAction:ipVC];
}

- (void)presentAction:(id)vc{
    [[DataTool getCurrentVC] presentViewController:vc animated:YES completion:nil];
}

#pragma mark
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSURL* url;
    UIImage * image;
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType]; //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    
    if ([mediaType isEqualToString:@"public.image"]) {
        if (@available(iOS 11.0, *)) {
            url = [info objectForKey:UIImagePickerControllerImageURL];

            if (url != nil) { //解决ios 13相册选择错误
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                image = [UIImage imageWithData:imageData];
            } else {

                UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
                if (originalImage != nil) {
                    image = originalImage;
                }

                UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
                if (editedImage != nil) {
                    image = editedImage;
                }

            }
        } else {

            UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
            if (originalImage != nil) {
                image = originalImage;
            }

            UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
            if (editedImage != nil) {
                image = editedImage;
            }
        }
    }
    
    if (image == nil) {
        [DataTool showHUDWithString:@"The picture is not available, please change it"];
        return;
    }
    
    EditPhotoViewController *VC = [[EditPhotoViewController alloc] init];
    VC.getImage = image;
    [ipVC pushViewController:VC animated:YES];
    
    VC.callbackImg = ^(id  _Nullable obj) {
        UIImage *getImage = obj;
        if (getImage == nil) {
            getImage = image;
        }
        if (self->_successHandle) {
            //            self->_successHandle(self, getImage);
            self->_successHandle(self, image, getImage);
        }
    };
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[DataTool getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
}

@end

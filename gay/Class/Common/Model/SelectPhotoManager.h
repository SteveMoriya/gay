//
//  SelectPhotoManager.h
//  gay
//
//  Created by steve on 2020/9/16.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PhotoCamera = 0,
    PhotoAlbum,
}SelectPhotoType;

NS_ASSUME_NONNULL_BEGIN

@interface SelectPhotoManager : NSObject<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property(nonatomic, strong)void (^successHandle)(SelectPhotoManager *manager, UIImage *originalImage, UIImage *resizeImage);

- (void)startSelectPhotoWithImageName:(NSString *)imageName;

- (void)openPhoto;

- (void)chooseLibrary;

@end

NS_ASSUME_NONNULL_END

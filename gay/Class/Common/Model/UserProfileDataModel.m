//
//  UserProfileDataModel.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserProfileDataModel.h"

@implementation UserProfileDataModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"userId":@"id"};
}

//+ (NSDictionary *)mj_objectClassInArray {
//    return @{
//        @"photos":[UserResourceDataModel class],
//        @"videos":[UserResourceDataModel class]
//    };
//}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.aboutMe forKey:@"aboutMe"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.age forKey:@"age"];
//    [aCoder encodeObject:self.birthday forKey:@"birthday"];

    [aCoder encodeObject:self.bodyType forKey:@"bodyType"];
    [aCoder encodeObject:self.concernNumber forKey:@"concernNumber"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.countryIcon forKey:@"countryIcon"];
//    [aCoder encodeObject:self.dimension forKey:@"dimension"];
    
    [aCoder encodeObject:self.ethnicity forKey:@"ethnicity"];
    [aCoder encodeObject:self.fansNumber forKey:@"fansNumber"];
    [aCoder encodeObject:self.followed forKey:@"followed"];
    [aCoder encodeObject:self.following forKey:@"following"];
    [aCoder encodeObject:self.forbidden forKey:@"forbidden"];
    [aCoder encodeObject:self.headImgUrl forKey:@"headImgUrl"];
    
    [aCoder encodeObject:self.height forKey:@"height"];
    [aCoder encodeObject:self.hiv forKey:@"hiv"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.identity forKey:@"identity"];
    [aCoder encodeObject:self.intro forKey:@"intro"];
    [aCoder encodeObject:self.latitude forKey:@"latitude"];
    [aCoder encodeObject:self.like forKey:@"like"];
    
    [aCoder encodeObject:self.longitude forKey:@"longitude"];
    [aCoder encodeObject:self.lookFor forKey:@"lookFor"];
    [aCoder encodeObject:self.love forKey:@"love"];
    [aCoder encodeObject:self.nickName forKey:@"nickName"];
    [aCoder encodeObject:self.notification forKey:@"notification"];
    
    [aCoder encodeObject:self.online forKey:@"online"];
    [aCoder encodeObject:self.postNumber forKey:@"postNumber"];
    [aCoder encodeObject:self.resources forKey:@"resources"];
    [aCoder encodeObject:self.role forKey:@"role"];
    [aCoder encodeObject:self.sex forKey:@"sex"];
    
    [aCoder encodeObject:self.state forKey:@"state"];
    [aCoder encodeObject:self.style forKey:@"style"];
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:self.vipOverTime forKey:@"vipOverTime"];
    [aCoder encodeObject:self.weight forKey:@"weight"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        
        self.aboutMe = [aDecoder decodeObjectForKey:@"aboutMe"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        self.age = [aDecoder decodeObjectForKey:@"age"];
//        self.birthday = [aDecoder decodeObjectForKey:@"birthday"];
        
        self.bodyType = [aDecoder decodeObjectForKey:@"bodyType"];
        self.concernNumber = [aDecoder decodeObjectForKey:@"concernNumber"];
        self.country = [aDecoder decodeObjectForKey:@"country"];
        self.countryIcon = [aDecoder decodeObjectForKey:@"countryIcon"];
//        self.dimension = [aDecoder decodeObjectForKey:@"dimension"];
        
        self.ethnicity = [aDecoder decodeObjectForKey:@"ethnicity"];
        self.fansNumber = [aDecoder decodeObjectForKey:@"fansNumber"];
        self.followed = [aDecoder decodeObjectForKey:@"followed"];
        self.following = [aDecoder decodeObjectForKey:@"following"];
        self.forbidden = [aDecoder decodeObjectForKey:@"forbidden"];
        self.headImgUrl = [aDecoder decodeObjectForKey:@"headImgUrl"];
        
        self.height = [aDecoder decodeObjectForKey:@"height"];
        self.hiv = [aDecoder decodeObjectForKey:@"hiv"];
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.identity = [aDecoder decodeObjectForKey:@"identity"];
        self.intro = [aDecoder decodeObjectForKey:@"intro"];
        self.latitude = [aDecoder decodeObjectForKey:@"latitude"];
        self.like = [aDecoder decodeObjectForKey:@"like"];
        
        self.longitude = [aDecoder decodeObjectForKey:@"longitude"];
        self.lookFor = [aDecoder decodeObjectForKey:@"lookFor"];
        self.love = [aDecoder decodeObjectForKey:@"love"];
        self.nickName = [aDecoder decodeObjectForKey:@"nickName"];
        self.notification = [aDecoder decodeObjectForKey:@"notification"];
        
        self.online = [aDecoder decodeObjectForKey:@"online"];
        self.postNumber = [aDecoder decodeObjectForKey:@"postNumber"];
        self.resources = [aDecoder decodeObjectForKey:@"resources"];
        self.role = [aDecoder decodeObjectForKey:@"role"];
        self.sex = [aDecoder decodeObjectForKey:@"sex"];
        
        self.state = [aDecoder decodeObjectForKey:@"state"];
        self.style = [aDecoder decodeObjectForKey:@"style"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
        self.vipOverTime = [aDecoder decodeObjectForKey:@"vipOverTime"];
        self.weight = [aDecoder decodeObjectForKey:@"weight"];
        
    }
    return self;
}


@end

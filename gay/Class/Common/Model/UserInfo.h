//
//  UserInfo.h
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfileDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfo : NSObject

@property (nonatomic, strong) UserProfileDataModel *user;
@property (nonatomic, strong) NSString *imSig;
@property (nonatomic, strong) NSString *token;

@property (nonatomic, copy) CallBack timLoginSuccBlock;
@property (nonatomic, copy) CallBack addressCallBack;

+ (instancetype) shareInstant;

- (void)checkUserDataModel:(id) obj callBackInfo:(CallBack) callback;
- (void)setupWithUserProfileInfoModel: (UserProfileDataModel *) profile;

- (void)loadUserinfoFromSadeBox; //获取沙盒数据
- (void)saveUserInfoToSadeBox; //存储用户数据
//- (void)cleancurrentUserinfo; //清理当前用户数据
- (void)deleteAllInfo; //清除全部数据

- (void)userLogin;
- (void)userLogout;
- (void)setLocationInfo;
- (BOOL)checkUserIsVip;

- (void) showCallOutView:(UserProfileDataModel *) profileModel;

- (void) showCallInViewWithProfileModel:(UserProfileDataModel *)profileModel getThoughClickBlock:(ClickBlock) getThoughClickBlock hungUpClickBlock:(ClickBlock) hungUpClickBlock;

@end

NS_ASSUME_NONNULL_END

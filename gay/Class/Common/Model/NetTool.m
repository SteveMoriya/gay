//
//  NetTool.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "NetTool.h"
#import <AFNetworking.h>
#import <AliyunOSSiOS/OSSService.h>
#import "Firebase.h"

#define     OSS_AccessKey           @"LTAI4Fj7dYQVN6fwMnbEfu47"
#define     OSS_SecretKey           @"N8VJykbZPJnSCNsIqU1inns8omZtdq"
#define     OSS_Endpoint            @"https://oss-cn-hangzhou.aliyuncs.com"

static id _shareInstance = nil;

@implementation NetTool {
    AFHTTPSessionManager *manager;
    OSSClient * client;
}

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[NetTool alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    
    //    // 推荐使用OSSAuthCredentialProvider，token过期后会自动刷新。
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:OSS_AccessKey secretKey:OSS_SecretKey];
    client = [[OSSClient alloc] initWithEndpoint:OSS_Endpoint credentialProvider:credential];
    
    manager = [[AFHTTPSessionManager alloc] init];
}


#pragma 图片上传
- (void)putDatas:(NSMutableArray *)Datas fileType:(NSString *)fileTypeString needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback {
    
    if (needShowHUD) {
        [DataTool startShowHUD];
    }
    
    NSMutableArray *resultArray = [NSMutableArray array]; //结果集合
    dispatch_queue_t q = dispatch_queue_create("fs", DISPATCH_QUEUE_CONCURRENT);
    for (NSData *imageData in Datas) {
        
        dispatch_async(q, ^{
            
            OSSPutObjectRequest * put = [OSSPutObjectRequest new];
            put.bucketName = OSS_BucketName;
            NSString *objectKeys = [NSString stringWithFormat:@"User/%@.%@",[DataTool getTimeNow], fileTypeString];
            put.objectKey = objectKeys;
            put.uploadingData = imageData;
            put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            };
            
            OSSTask * putTask = [self->client putObject:put];
            [putTask continueWithBlock:^id(OSSTask *task) {
                task = [self->client presignPublicURLWithBucketName:OSS_BucketName withObjectKey:objectKeys];
                [resultArray addObject:[NSString stringWithFormat:@"%@/%@", OSS_ImageHeader, put.objectKey]];
                if (resultArray.count == Datas.count) { //判断满足条件就返回
                    
                    if (needShowHUD) {
                        [DataTool dismissHUD];
                    }
                    
                    callback(resultArray);
                }
                return nil;
            }];
        });
    }
}


- (void)startRequest:(NSString *)url method:(NSString *)method params:(id) params needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback {
    
    NSLog(@"startRequest %@ params %@   [UserInfo shareInstant].user.token %@", url, params, [UserInfo shareInstant].token);
    
    if (needShowHUD) {
        [DataTool startShowHUD];
    }
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:method URLString:url parameters:params error:nil]; //method 为时post请求还是get请求
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
//    UILabel *versionLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(deleteAccountBt.frame), SCREEN_Width, ZoomSize(20))];
//    [contentScrollView addSubview:versionLb];
//    versionLb.text = [NSString stringWithFormat:@"Version %@",app_Version];
    
    [request setValue:app_Version forHTTPHeaderField:@"app-version"];
    [request setValue:@"1.0" forHTTPHeaderField:@"api-version"];
    [request setValue:[DataTool getTimeStampString] forHTTPHeaderField:@"timeStamp"];
    [request setValue:@"ios" forHTTPHeaderField:@"device"];
    //    [request setValue:[DataTool getDeviceUUID] forHTTPHeaderField:@"uuid"];
    [request setValue:@"EN_US" forHTTPHeaderField:@"language"];
    [request setValue:[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode] forHTTPHeaderField:@"countryCode"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    request.timeoutInterval = 30.f;
    
    if (![DataTool isEmptyString:[UserInfo shareInstant].token]) { //由于退出后需要重设，这里每次都重设一次
        [request setValue:[UserInfo shareInstant].token forHTTPHeaderField:@"x-access-token"];
    }
    
    //    //进行网络请求
    [[manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
    } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
    } completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error) {
        
        if (responseObject) {
            [self checkDataSuccess:responseObject url:url needShowHUD:needShowHUD callback:^(id  _Nullable obj) {
                callback(obj);
            }];
        } else {
            [self checkDatafailure:error url:url callback:^(id  _Nullable obj) {
                callback(obj);
            }];
        }
        
    }] resume];
    
}

- (void)checkDataSuccess:(id)responseObject url:(NSString *)url needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback {
    
    NSLog(@"responseObject %@  url %@", responseObject, url);
    if (needShowHUD) {
        [DataTool dismissHUD];
    }
    
    NSDictionary *responseDic = [[NSDictionary alloc] init];
    responseDic = responseObject;
    
    int resultCode = [responseDic[@"code"] intValue];
    NSString *msgString = responseDic[@"msg"];
    NSDictionary *resultDic = [[NSDictionary alloc] init];
    
    if (resultCode == 200 ) {
        resultDic = responseDic[@"data"];
        callback(resultDic);
    } else if (resultCode == 501) {
        [[UserInfo shareInstant] userLogout]; //token 过期
    }   else {
        [DataTool showHUDWithString:msgString];
    }
    
    
    //    int statusCode = [responseDic[@"status"] intValue];
    //    if (statusCode == 500) {
    //        if ([url isEqualToString:API_V2_verify_Charge]) { //验证订单超时时，需要重新验证
    //            [[PurchaseTool shareInstance] restorePurchase:YES];
    //            return;
    //        }
    //    }
    
}


- (void)checkDatafailure:(id)responseObject url:(NSString *)url  callback:(CallBack) callback {
    NSLog(@"responseObject %@", responseObject);
    [DataTool showHUDWithString:@"please retry later"];
    
    //    NSDictionary *resultDic = [[NSDictionary alloc] init];
    //    callback(resultDic);
}

- (void) sendFireBaseWithEventName:(NSString *)EventNameString {
//    [FIRAnalytics logEventWithName:kFIREventSelectContent
//                        parameters:@{
//                            kFIRParameterItemID:@"userId",
//                            kFIRParameterItemName:@"name",
//                            kFIRParameterContentType:@"image"
//                        }];
    
//    [FIRAnalytics logEventWithName:@"" parameters:@{}];
    
    [FIRAnalytics logEventWithName:EventNameString parameters:nil];
}

#pragma mark - send notification

- (void) sendRefleshProfileInfoNotification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NeedRefleshProfilePage object:nil userInfo:nil];
    });
}

- (void) sendBlockUserNotification:(id)blockUserId {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:blockUserNotification object:blockUserId userInfo:nil];
    });
}


@end

//
//  SocketTool.h
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SocketRocket/SRWebSocket.h>

NS_ASSUME_NONNULL_BEGIN

@interface SocketTool : NSObject

@property (nonatomic, assign) int reConnectTime;
@property (nonatomic, strong) SRWebSocket *webSocket;

+ (instancetype)shareInstance;

#pragma socket

- (void)setupWebSocket;
- (void)openSocket;
- (void)closeSocket;
- (void)reconnectSocket;

- (void)sendInfoToSocket :(NSDictionary *)infoDic;

@end

NS_ASSUME_NONNULL_END

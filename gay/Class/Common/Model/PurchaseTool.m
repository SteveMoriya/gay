//
//  PurchaseTool.m
//  gay
//
//  Created by steve on 2020/10/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "PurchaseTool.h"
#import <StoreKit/StoreKit.h>
#import <SVProgressHUD.h>
#import "HUBView.h"

static id _shareInstance = nil;

@interface PurchaseTool()<SKPaymentTransactionObserver,SKProductsRequestDelegate> {
    NSMutableArray *payTransaction; //需要处理的账单
    HUBView *hubView; //占位视图
    BOOL needShowHub;
}

@end

@implementation PurchaseTool

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[PurchaseTool alloc] init];
    });
    return _shareInstance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        [self setupPurchaseTool];
    }
    return self;
}

- (void) setupPurchaseTool {
    payTransaction = [NSMutableArray array];
    hubView = [[HUBView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
    needShowHub = YES;
}

- (void) addTranObserver {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self]; //添加支付结果监听
}

- (void) startCreateOrderWithPID:(NSString *)productIdentifier {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"PurchaseTool_createOrder"];
    [hubView show];
    
    NSDictionary *paramsDic = @{@"description":productIdentifier}; //创建订单
    [[NetTool shareInstance] startRequest:API_BuyCoins method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        [[NetTool shareInstance] sendFireBaseWithEventName:@"BuyVip_createOrder"];
        [self getProductInfo:productIdentifier];
    }];
}

- (void)getProductInfo:(NSString *)productIdentifier{
    
    if (productIdentifier.length > 0){
        NSArray * product = [[NSArray alloc] initWithObjects:productIdentifier, nil];
        NSSet *set = [NSSet setWithArray:product];
        SKProductsRequest * request = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
        request.delegate = self;
        [request start];//开始请求
    } else {
        [hubView dismiss];
    }
}

//恢复购买
- (void)restorePurchase :(BOOL) needHub {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    needShowHub = needHub;
    if (needShowHub) {
        [hubView show];
    }
}

- (void) addTranToCheck: (SKPaymentTransaction *)tran {
    BOOL canAdd = YES;
    for (SKPaymentTransaction *hasTran in self->payTransaction) {
        if ([hasTran.transactionIdentifier isEqualToString:tran.transactionIdentifier]) {
            canAdd = NO;
            return;
        }
    }
    if (canAdd) {
        [payTransaction addObject:tran];
    }
}

//内购数据给后台验证
-(void)buyAppleStoreProductSucceedWithPaymentTransactionp :(SKPaymentTransaction *)tran {
    
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    NSString *transactionReceiptString = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSString *userIdString = [UserInfo shareInstant].user.userId;
    if (![DataTool isEmptyString:tran.payment.applicationUsername]) {
        userIdString = tran.payment.applicationUsername;
    }
    
    NSDictionary *paramsDic = @{@"ticket":transactionReceiptString, @"userId":userIdString }; //    //获取用户资料
    
    [[NetTool shareInstance] startRequest:API_verify_Charge method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        [[NetTool shareInstance] sendFireBaseWithEventName:@"BuyVip_verify_charge"];
        
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic[@"user"]];
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [[NetTool shareInstance] sendFireBaseWithEventName:@"PurchaseTool_buySuccess"];
            [[NetTool shareInstance] sendRefleshProfileInfoNotification];
        }];
    }];
}

- (void) finishTransactionAction {

    for (SKPaymentTransaction *tran in self->payTransaction) {
        if (tran.transactionState == SKPaymentTransactionStatePurchased) {
            [[SKPaymentQueue defaultQueue]finishTransaction:tran];
        } else if (tran.transactionState == SKPaymentTransactionStateRestored) {
            [[SKPaymentQueue defaultQueue]finishTransaction:tran];
        }
    }
}

#pragma product
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
//    NSLog(@"didReceiveResponse");
    NSArray *product = response.products;
    if([product count] == 0){
        [hubView dismiss];
        return;
    }
    
    SKProduct *p = nil;
    for (SKProduct *pro in product) {
        p = pro;
        SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:p];
        payment.applicationUsername = [UserInfo shareInstant].user.userId;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
        break;
    }
}

//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError");
    [hubView dismiss];
}

//请求完成
- (void)requestDidFinish:(SKRequest *)request{
    NSLog(@"requestDidFinish");
}

#pragma payment 监听购买结果
- (BOOL)paymentQueue:(SKPaymentQueue *)queue shouldAddStorePayment:(SKPayment *)payment forProduct:(SKProduct *)product {
    return YES;
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    
    NSLog(@"updatedTransactions");
    [payTransaction removeAllObjects]; //清除数据
    
    for (SKPaymentTransaction *tran in transactions) {
        
        switch (tran.transactionState) {
            case SKPaymentTransactionStatePurchased:  { //交易完成
                [self addTranToCheck:tran];
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStatePurchasing: {
            }
                break;
            case SKPaymentTransactionStateRestored:  { //购买过
                [self addTranToCheck:tran];
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
                
                if (needShowHub) {
                    [DataTool showHUDWithString:@"Restored Success"];
                }
            }
                break;
            case SKPaymentTransactionStateFailed:  { //交易失败
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
            }
                break;
            default: {
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
            }
                break;
        }
    }
    
    if (payTransaction.count > 0) {
        
        for (SKPaymentTransaction *tran in self->payTransaction) {
            if (tran.transactionState == SKPaymentTransactionStatePurchased || tran.transactionState == SKPaymentTransactionStateRestored) {
                [self buyAppleStoreProductSucceedWithPaymentTransactionp:tran]; //调用后台验证
//                break;
            }
        }
    }
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    NSLog(@"removedTransactions");
    [hubView dismiss];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error  {
    NSLog(@"restoreFaild");
    [hubView dismiss];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"restoreFinish");
    [hubView dismiss];
}

@end

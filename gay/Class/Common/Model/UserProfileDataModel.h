//
//  UserProfileDataModel.h
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserProfileDataModel : NSObject

@property (nonatomic, strong) NSString *aboutMe;
@property (nonatomic, strong) NSString *address; //地址AA
@property (nonatomic, strong) NSString *age; //年龄
//@property (nonatomic, strong) NSString *birthday; //生日
 
@property (nonatomic, strong) NSString *bodyType;
@property (nonatomic, strong) NSString *concernNumber;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *countryIcon;
//@property (nonatomic, strong) NSString *dimension;

@property (nonatomic, strong) NSString *ethnicity;
@property (nonatomic, strong) NSString *fansNumber;
@property (nonatomic, strong) NSString *followed; //被多少人关注
@property (nonatomic, strong) NSString *following; //关注多少人
@property (nonatomic, strong) NSString *forbidden;
@property (nonatomic, strong) NSString *headImgUrl;

@property (nonatomic, strong) NSString *height;
@property (nonatomic, strong) NSString *hiv;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *identity;
@property (nonatomic, strong) NSString *intro;

@property (nonatomic, strong) NSString *languages;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *like; //我follow了这个人

@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *lookFor;
@property (nonatomic, strong) NSString *love; //我like了这个人
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *notification;

@property (nonatomic, strong) NSString *online;
@property (nonatomic, strong) NSString *postNumber;
@property (nonatomic, strong) NSArray *resources;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *sex;

@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *style;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *vipOverTime;
@property (nonatomic, strong) NSString *weight;


#pragma 用于im信息
@property (nonatomic, strong) NSString *roomId; //
@property (nonatomic, strong) NSString *messageText; //

@end



NS_ASSUME_NONNULL_END

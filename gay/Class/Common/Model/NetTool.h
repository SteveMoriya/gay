//
//  NetTool.h
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetTool : NSObject

+ (instancetype)shareInstance;

- (void)putDatas:(NSMutableArray *)Datas fileType:(NSString *)fileTypeString needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback;

- (void)startRequest:(NSString *)url method:(NSString *)method params:(id) params needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback;

- (void) sendRefleshProfileInfoNotification; //重新刷新profile的通知，用于coin，vip购买
- (void) sendBlockUserNotification:(id)blockUserId;
- (void) sendFireBaseWithEventName:(NSString *)EventNameString;

@end

NS_ASSUME_NONNULL_END

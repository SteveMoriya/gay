//
//  UserInfo.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserInfo.h"

#import <CoreLocation/CoreLocation.h>
#import "TRTCVideoCallViewController.h"
#import "ImSDK.h"
#import "TUIBubbleMessageCellData.h"
#import "TUITextMessageCellData.h"
#import "TUIKitConfig.h"
#import "TUIKit.h"
#import "PurchaseTool.h"

#import "FTPopOverMenu.h"
#import "UserCallInView.h"
#import "UserCallOutView.h"
#import "EBCustomBannerView.h"

static id _shareInstance = nil;

@interface UserInfo() <CLLocationManagerDelegate> {
    CLLocationManager *locationmanager;
    BOOL canShowCallIn; //可以显示callIn, 用于防止重复弹出
}

@property (nonatomic ,strong) EBCustomBannerView *callIn_CustomBannerView;
@property (nonatomic ,strong) UserCallOutView *callOutView;

@end

@implementation UserInfo

+ (instancetype)shareInstant {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[UserInfo alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self loadUserinfoFromSadeBox];
        canShowCallIn = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationStatusAction)name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

#pragma profile

- (void)setupWithUserProfileInfoModel: (UserProfileDataModel *) profile {
    [UserInfo shareInstant].user = profile;
}

- (void)checkUserDataModel: (id) obj callBackInfo : (CallBack) callback {
    UserProfileDataModel *userProfileInfoModel = obj;
    [[UserInfo shareInstant] setupWithUserProfileInfoModel:userProfileInfoModel]; //设置信息，并保存信息
    [[UserInfo shareInstant] saveUserInfoToSadeBox];
    
//    //同步IM信息
//    if ([[TIMManager sharedInstance] getLoginStatus] == 1) {
//        [self setIMInfo];
//    } else {
//        [UserInfo shareInstant].timLoginSuccBlock = ^(id  _Nullable obj) {
//            [self setIMInfo];
//        };
//    }
    
    callback(obj);
}


#pragma UserDefault

- (void)loadUserinfoFromSadeBox {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    self.token = [userDefaults objectForKey:UserToken];
    self.imSig = [userDefaults objectForKey:UserImSig];
    
    NSData *userData = [userDefaults objectForKey:UserProfile];
    UserProfileDataModel *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    self.user = userProfile;
}

- (void)saveUserInfoToSadeBox {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.token forKey:UserToken];
    [userDefaults setObject:self.imSig forKey:UserImSig];
    
    NSData *profileData = [NSKeyedArchiver archivedDataWithRootObject: self.user];
    [userDefaults setObject:profileData forKey:UserProfile];
    [userDefaults synchronize];
}

- (void)cleancurrentUserinfo {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; //用户退出，切换时必须删除的数据
    NSString *pathBundle = [[NSBundle mainBundle]pathForResource:@"UserDefault" ofType:@"plist"];
    NSArray *userDefaultArray = [NSArray arrayWithContentsOfFile:pathBundle];
    
    for(NSString* key in userDefaultArray){
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}

- (void)deleteAllInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [userDefaults dictionaryRepresentation];
    for(NSString* key in [dictionary allKeys]){
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
    
    
    [UserInfo shareInstant].token = @"";
    [UserInfo shareInstant].imSig = @"";
}

#pragma userAction

- (void)userLogin {
    
    if (![DataTool isEqual:[UserInfo shareInstant].imSig]) { //登录Im
        
        [[V2TIMManager sharedInstance] login:[UserInfo shareInstant].user.userId userSig:[UserInfo shareInstant].imSig succ:^{
            
            [[NetTool shareInstance] sendFireBaseWithEventName:@"userInfo_imLogin"];
            
            [self setIMInfo];
            
            NSData *deviceTokenData = [[NSUserDefaults standardUserDefaults] dataForKey:DeviceToken];
            
            V2TIMAPNSConfig *confg = [[V2TIMAPNSConfig alloc] init];
            // 企业证书 ID，上传证书到 IM 控制台后生成
            confg.businessID = [PushBusinessID intValue];
            confg.token = deviceTokenData;
            [[V2TIMManager sharedInstance] setAPNS:confg succ:^{
                NSLog(@"-----> 设置 APNS 成功");
                [[NetTool shareInstance] sendFireBaseWithEventName:@"userInfo_pushRegist"];
                
            } fail:^(int code, NSString *msg) {
                NSLog(@"-----> 设置 APNS 失败");
            }];
            
            
            if (self->_timLoginSuccBlock) {
                self->_timLoginSuccBlock(@"Succ");
            }
            
            TUIKitConfig *config = [TUIKitConfig defaultConfig];
            config.defaultAvatarImage = [UIImage imageNamed:@"message_vido"];
            
            [TUIBubbleMessageCellData setOutgoingBubble:[UIImage imageNamed:@"messages_dialog_out"]];
            [TUIBubbleMessageCellData setOutgoingHighlightedBubble:[UIImage imageNamed:@"messages_dialog_out"]];
            [TUIBubbleMessageCellData setIncommingBubble:[UIImage imageNamed:@"messages_dialog_in"]];
            [TUIBubbleMessageCellData setIncommingHighlightedBubble:[UIImage imageNamed:@"messages_dialog_in"]];
            
            [TUITextMessageCellData setIncommingNameFont:[UIFont systemFontOfSize:16]];
            [TUITextMessageCellData setIncommingTextColor:WhiteColor];
            [TUITextMessageCellData setOutgoingTextFont:[UIFont systemFontOfSize:16]];
            [TUITextMessageCellData setOutgoingTextColor:WhiteColor];
            
            [TUIMessageCellLayout incommingMessageLayout].avatarSize = CGSizeMake(0, 0); // 设置发送头像大小；设置接收的方法类似
            [TUIMessageCellLayout incommingMessageLayout].avatarInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [TUIMessageCellLayout incommingMessageLayout].bubbleInsets = UIEdgeInsetsMake(6, 12, 2, 12);
            
            [TUIMessageCellLayout outgoingMessageLayout].avatarSize = CGSizeMake(0, 0);
            [TUIMessageCellLayout outgoingMessageLayout].avatarInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [TUIMessageCellLayout outgoingMessageLayout].bubbleInsets = UIEdgeInsetsMake(6, 12, 2, 12);
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMessage:) name:TUIKitNotification_TIMMessageListener object:nil];
            
            
        } fail:^(int code, NSString *desc) {

        }];
    }
    
    [[PurchaseTool shareInstance] addTranObserver]; //添加支付监听
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"userInfo_socketInit"];
    [[SocketTool shareInstance] setupWebSocket]; //设置socket
    
    [DataTool showHomePage]; //显示首页
}

- (void)onNewMessage:(NSNotification *)notification {
    NSArray *msgs = notification.object;
    //    for (TIMMessage *msg in msgs) {
    //    }
    
    TIMMessage *msg = msgs.lastObject;
    for (int i = 0; i < msg.elemCount; ++i) {
        TIMElem *elem = [msg getElem:i];
        if ([DataTool getTimeDiffFromDate:msg.timestamp] > 60) { //添加时间检查，用于防止重复弹出
            return;
        }
        
        if([elem isKindOfClass:[TIMCustomElem class]]){
            TIMCustomElem *dataElem = (TIMCustomElem *)elem;
            NSString *getDicString = [dataElem.data mj_JSONString];
            UserProfileDataModel *otherUserModel = [UserProfileDataModel mj_objectWithKeyValues:getDicString]; //对方
            
            if ([otherUserModel.messageText isEqualToString:@"[VideoCall_CallOut]"]) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:UserVideoCall]) { //使用存储的关键字信息
                    canShowCallIn = YES;
                } else {
                    canShowCallIn = NO;
                }
                
                if (canShowCallIn) {
                    canShowCallIn = NO;
                    [self showCallInViewWithProfileModel:otherUserModel getThoughClickBlock:^{
                        self->canShowCallIn = YES;
                        [self->_callIn_CustomBannerView hide];
                        
                        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
                        NSTimeInterval timeInterval = [date timeIntervalSince1970];
                        NSString *getRoomID = [NSString stringWithFormat:@"%.0f", timeInterval];
                        
                        [[UserInfo shareInstant] sendImMessageToOther:otherUserModel.userId messgeTextString:@"[VideoCall_Accept]" roomId:getRoomID];
                        
                        //                        getRoomID 取值范围：1 - 4294967294。
                        [self showTrtcPage:otherUserModel roomId:getRoomID];
                        
                    } hungUpClickBlock:^{
                        self->canShowCallIn = YES;
                        [self->_callIn_CustomBannerView hide];
                        [[UserInfo shareInstant] sendImMessageToOther:otherUserModel.userId messgeTextString:@"[VideoCall_Denny]" roomId:@""];
                        
                    }];
                }
                
            } else if ([otherUserModel.messageText isEqualToString:@"[VideoCall_Cancel]"]) {
                canShowCallIn = YES;
                [self->_callIn_CustomBannerView hide];
                [DataTool showBannerViewWithTitle: [NSString stringWithFormat:@"%@ Cancel Call",otherUserModel.nickName] ];
            } else if ([otherUserModel.messageText isEqualToString:@"[VideoCall_Denny]"]) {
                [self->_callOutView dismiss];
                [DataTool showBannerViewWithTitle: [NSString stringWithFormat:@"%@ Denny Call",otherUserModel.nickName] ];
            } else if ([otherUserModel.messageText isEqualToString:@"[VideoCall_Accept]"]) {
                [self->_callOutView dismiss];
                
                
                [self showTrtcPage:otherUserModel roomId:otherUserModel.roomId];
            }
        }
    }
    
}


//[VideoCall_CallOut]  a拨打b; 1，a发送callout消息 2，a弹出 播出视图， 3，b弹出 呼入弹窗
//[VideoCall_Cancel] a放弃 ，1,a发送cancel消息，2，a消失 播出视图 3，b消失 呼入弹窗
//[VideoCall_Denny] b拒绝 1，b发送denny消息， 2，a消失 播出视图 3，b消失 呼入弹窗
//[VideoCall_Accept] b接受； 1，b创建创建roomID，进入视频界面 2，b发送accept消息（包含roomID）3，a进入视频界面

- (void) sendImMessageToOther:(NSString *)otherUserId messgeTextString:(NSString *) messageTextString roomId:(NSString *)roomId {
    
    NSString *headImageUrlString = @"";
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headImgUrl]) {
        headImageUrlString = [UserInfo shareInstant].user.headImgUrl;
    }
    
    NSDictionary *userInfoDic = @{ @"messageText":messageTextString, @"id":self.user.userId, @"nickName":self.user.nickName, @"headImgUrl": headImageUrlString, @"roomId": roomId};
    NSData *userInfoData =  [userInfoDic mj_JSONData];
    V2TIMMessage *msg = [[V2TIMManager sharedInstance] createCustomMessage:userInfoData];
    
    [[V2TIMManager sharedInstance] sendMessage:msg receiver:otherUserId groupID:nil priority:V2TIM_PRIORITY_DEFAULT
                                onlineUserOnly:NO offlinePushInfo:nil progress:^(uint32_t progress) {
    } succ:^{
        NSLog(@"msg succ");
    } fail:^(int code, NSString *msg) {
        NSLog(@"msg fail %@", msg);
    }];
    
}


- (void) showCallInViewWithProfileModel:(UserProfileDataModel *)profileModel getThoughClickBlock:(ClickBlock) getThoughClickBlock hungUpClickBlock:(ClickBlock) hungUpClickBlock {
    
    UserCallInView *view = [[UserCallInView alloc] initWithFrame:CGRectMake(ZoomSize(11), StatusBarHeight + ZoomSize(10), ZoomSize(353), ZoomSize(95))];
    view.profileModel = profileModel;
    
    _callIn_CustomBannerView = [EBCustomBannerView customView:view block:^(EBCustomBannerViewMaker *make) {
        make.portraitMode = EBCustomViewAppearModeTop;
        make.stayDuration = 60.0 ;
    }];
    
    view.getThoughBlock = ^{
        if (getThoughClickBlock) {
            getThoughClickBlock();
        }
        
    };
    view.hungUpBlock  = ^{
        if (hungUpClickBlock) {
            hungUpClickBlock();
        }
    };
    
    [_callIn_CustomBannerView show];
}

- (void) showCallOutView:(UserProfileDataModel *) profileModel  {
    
    [[UserInfo shareInstant] sendImMessageToOther:profileModel.userId messgeTextString:@"[VideoCall_CallOut]" roomId:@""];
    
    self->_callOutView = [[UserCallOutView alloc] initWithFrame:[DataTool getCurrentVC].view.frame profileModel:profileModel];
    self->_callOutView.cancelBlock = ^{
        [[UserInfo shareInstant] sendImMessageToOther:profileModel.userId messgeTextString:@"[VideoCall_Cancel]" roomId:@""];
    };
    [self->_callOutView show];
}

- (void) showTrtcPage:(UserProfileDataModel *)profileModel roomId:(NSString *)roomId{
    
    [FTPopOverMenu dismiss];
    
    TRTCVideoCallViewController *vc = [[TRTCVideoCallViewController alloc] init];
    vc.profileModel = profileModel;
    vc.roomId = roomId;
    
    //    vc.callType = videoCallIn;
    //    vc.dismissBLock = ^{
    //        if (dismissBlock) {
    //            dismissBlock(@"dismiss");
    //        }
    //    };
    
    vc.modalPresentationStyle = 0;
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

- (void)userLogout {
    
    if (![DataTool isEqual:[UserInfo shareInstant].imSig]) { //退出Im
        
        [[V2TIMManager sharedInstance] logout:^{
            NSLog(@"TIMManager logout success");
        } fail:^(int code, NSString *desc) {
            NSLog(@"TIMManager logout fail ");
        }];
    }
    
    [self deleteAllInfo];
    
    [[SocketTool shareInstance] closeSocket];
    [DataTool showLoginPage];
}

- (void) setIMInfo {
    //            同步IM数据
    if (![DataTool isEmptyString:self.user.headImgUrl]) {
        NSDictionary *userProfileParams = @{TIMProfileTypeKey_FaceUrl:self.user.headImgUrl};
        [[TIMFriendshipManager sharedInstance] modifySelfProfile:userProfileParams succ:^{
        } fail:^(int code, NSString *msg) {
        }];
    }
    if (![DataTool isEmptyString:self.user.nickName]) {
        NSDictionary *userProfileParams = @{TIMProfileTypeKey_Nick:self.user.nickName};
        [[TIMFriendshipManager sharedInstance] modifySelfProfile:userProfileParams succ:^{
        } fail:^(int code, NSString *msg) {
        }];
    }
    
}

- (void) applicationStatusAction {
    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] > 2) {
        locationmanager.desiredAccuracy = kCLLocationAccuracyKilometer;
        [locationmanager startUpdatingLocation];
    } else {
        [locationmanager requestWhenInUseAuthorization];
    }
}

- (BOOL) checkUserIsVip {
    
//#warning 这里的时间需要修改为服务器时间
    double currentUTCTime = [[NSDate date] timeIntervalSince1970] * 1000;
    long vipTime = [[UserInfo shareInstant].user.vipOverTime longLongValue];
    if (vipTime > currentUTCTime) {
        return YES;
    }
    return NO;
    
}

- (void) setLocationInfo {
    locationmanager = [[CLLocationManager alloc]init];
    locationmanager.delegate = self;
    
    switch ([CLLocationManager authorizationStatus]) {
        case 0:
            [locationmanager requestWhenInUseAuthorization];
            break;
        default:
            locationmanager.desiredAccuracy = kCLLocationAccuracyKilometer;
            [locationmanager startUpdatingLocation];
            break;
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    CLLocation *currentLocation = [locations lastObject];
    double getLatitude = 0;
    getLatitude = currentLocation.coordinate.latitude;
    double getLongitude = 0;
    getLongitude = currentLocation.coordinate.longitude;
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en",nil]
                                              forKey:@"AppleLanguages"];
    //反地理编码
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        [self->locationmanager stopUpdatingLocation];
        NSString *getPlaceString = @"";
        
        if (placemarks.count > 0) {
            CLPlacemark *placeMark = placemarks[0];
            
            if (![DataTool isEmptyString:placeMark.administrativeArea] || ![DataTool isEmptyString:placeMark.country]) {
                getPlaceString = [NSString stringWithFormat:@"%@,%@", placeMark.locality, placeMark.administrativeArea];
            }
            
            if (![DataTool isEmptyString:placeMark.locality] || ![DataTool isEmptyString:placeMark.country]) {
                getPlaceString = [NSString stringWithFormat:@"%@,%@", placeMark.locality, placeMark.country];
            }
        }
        
        NSDictionary *addressDic = @{@"addressString":getPlaceString, @"addressArray":[NSArray arrayWithObjects: [NSString stringWithFormat:@"%.6f", getLatitude], [NSString stringWithFormat:@"%.6f", getLongitude], nil]};
        
        if (self->_addressCallBack) {
            self->_addressCallBack(addressDic);
        }
        
    }];
    
}


@end

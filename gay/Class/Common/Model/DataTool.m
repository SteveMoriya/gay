//
//  DataTool.m
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "DataTool.h"
#import "LoginViewController.h"
#import "TabbarViewController.h"
#import "WebViewController.h"
#import "ChatViewController.h"
#import "UserProfileViewController.h"

#import "BuyVipPopView.h"
#import "JKPickView.h"
#import "EBCustomBannerView.h"

#import "Toast.h"
#import <SVProgressHUD.h>
#import <SAMKeychain/SAMKeychain.h>
#import <IDMPhotoBrowser.h>

@interface DataTool()

@end

@implementation DataTool

+ (void) showLoginPage {
    LoginViewController *vc = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

+ (void) showHomePage {
    TabbarViewController *vc = [[TabbarViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
}

+ (void) showBuyVipView {
    
    BuyVipPopView *view = [[BuyVipPopView alloc] initWithFrame:[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view].frame];
    [view show];
    
}

+ (void) openWebVC:(NSString *) urlString {
    
    WebViewController *vc = [[WebViewController alloc] init];
    vc.url = urlString;
    //    [[self getCurrentVC].navigationController pushViewController:vc animated:YES];
    
    [[self getCurrentVC] presentViewController:vc animated:YES completion:nil];
    
}

+ (void) openUserProfileView:(UserProfileDataModel *)dataModel andClickImgIndex:(int) clickImgIndex isFromShare:(BOOL) isFromShare {
    UserProfileViewController *vc = [[UserProfileViewController alloc] init];
    vc.isFromShare = isFromShare;
    vc.clickImgIndex = clickImgIndex;
    vc.profileDataModel = dataModel;
    [[self getCurrentVC].navigationController pushViewController:vc animated:YES];
}

+ (void)showChatViewControllerWithType:(TIMConversationType)type receiver:(NSString*)receiverId receiverProfileModel:(UserProfileDataModel *)profileModel {
    
    TIMConversation *conv = [[TIMManager sharedInstance] getConversation:type receiver:receiverId];
    ChatViewController *vc = [[ChatViewController alloc] initWithConversation:conv];
    vc.type = type;
    vc.receiverId = receiverId;
    vc.profileModel = profileModel;
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

+ (UIViewController *)getCurrentVC {
    UIViewController *vc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *currentShowingVC = [self findCurrentShowingViewControllerFrom:vc];
    return currentShowingVC;
}

+ (UIViewController *)findCurrentShowingViewControllerFrom:(UIViewController *)vc {
    UIViewController *currentShowingVC; //方法1：递归方法 Recursive method
    if ([vc presentedViewController]) { //注要优先判断vc是否有弹出其他视图，如有则当前显示的视图肯定是在那上面
        UIViewController *nextRootVC = [vc presentedViewController]; // 当前视图是被presented出来的
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        UIViewController *nextRootVC = [(UITabBarController *)vc selectedViewController];  // 根视图为UITabBarController
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else if ([vc isKindOfClass:[UINavigationController class]]){
        UIViewController *nextRootVC = [(UINavigationController *)vc visibleViewController]; // 根视图为UINavigationController
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else {
        currentShowingVC = vc; // 根视图为非导航类
    }
    return currentShowingVC;
}

+ (UIImage *)compressSizeImage:(UIImage *)image {
    
    CGSize size = image.size;
    UIImage *resultImage = image;
    if (size.width >= size.height) {
        
        if (size.height > 1080) { //横屏图片，目标为: w 1920, h 1080
            int newWidth = (size.width/size.height) * 1080; //设置，当图片为横屏图片，且图片的高度大于 1080，则图片需要被压缩
            int newHeight = 1080;
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [resultImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            resultImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return resultImage;
        } else {
            return image;
        }
    } else {
        
        if (size.width > 1080) { //竖屏图片，目标为: w 1080, h 1920
            int newWidth = 1080;
            int newHeight = (size.height / size.width) * 1080; //设置，当图片为竖屏图片，且图片的宽度大于1080，则图片需要被压缩
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [resultImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            resultImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return resultImage;
        } else {
            return image;
        }
    }
    return image;
}


+ (UIImage *) imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f,0.0f, 1.0f,1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (BOOL) isEmptyString:(NSString *)string {
    if (string == nil || string == NULL || [string isEqual:[NSNull null]] ) {
        return YES;
    }
    NSString * newSelf = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    if(nil == string
       || string.length ==0
       || [string isEqualToString:@""]
       || [string isEqualToString:@"<null>"]
       || [string isEqualToString:@"(null)"]
       || [string isEqualToString:@"null"]
       || newSelf.length ==0
       || [newSelf isEqualToString:@""]
       || [newSelf isEqualToString:@"<null>"]
       || [newSelf isEqualToString:@"(null)"]
       || [newSelf isEqualToString:@"null"]
       || [self isKindOfClass:[NSNull class]] ){
        return YES;
    }else{
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimedString = [string stringByTrimmingCharactersInSet:set];
        return [trimedString isEqualToString: @""];
    }
    return NO;
}

+ (BOOL) isValidateEmail:(NSString *)email {
    
    NSString *userRegexp = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,10}";   //邮箱格式
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userRegexp];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL) checkUserIsShare:(NSString *)userID {
    BOOL checkResult = NO;
    
    int userIDIndex = (int) [userID length] - 1;
    NSString *subUserID = [userID substringFromIndex:userIDIndex];
    if (![subUserID isEqualToString:@"1"]) {
        checkResult = YES;
    }
    return checkResult;
}

+ (NSString *) lowercaseInputString:(NSString *) inputString {
    return [inputString lowercaseString];
}

+ (NSString *) getDeviceUUID {
    NSString * deviceUUID = [self getContentFromKeyChain:@"DeviceUUID"];
    if ([DataTool isEmptyString:deviceUUID ] || [deviceUUID isEqualToString:@"00000000000000000000000000000000"]) {
        NSString *getAdID = [[NSUUID UUID] UUIDString];
        NSString *adId = [getAdID stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [self saveContentToKeyChain:adId forKey:@"DeviceUUID" service:@"DeviceUUID"];
        deviceUUID = adId;
    }
    //    NSLog(@"deviceUUID %@", deviceUUID);
    return deviceUUID;
}

+ (NSString *)getTimeNow {
    NSString* date;
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"YYYYMMddhhmmssSSS"];
    date = [formatter stringFromDate:[NSDate date]];
    
    int last = arc4random() % 10000;//取出个随机数
    NSString *timeNow = [[NSString alloc] initWithFormat:@"%@-%i", date,last];
    return timeNow;
}

+ (NSString *) getTimeStampString {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    NSString *timeStampString  = [NSString stringWithFormat:@"%0.f", timeInterval];
    return timeStampString;
}

+ (NSString *) getTimTimeFromString :(NSDate *)timDate {
    
    //    当天显示时间，昨天显示yesterday
    //    昨天之前就显示日期02/03
    //    去年  2019/02/23
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString* dateString = @"";
    
    NSDateComponents *dateComponentYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:timDate toDate:[NSDate date] options:0];
    NSDateComponents *dateComponentDay = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:timDate toDate:[NSDate date] options:0];
    
    if (dateComponentYear.year > 1 ) {
        [formatter setDateFormat:@"yyyy-MM-dd"];
    } else {
        if (dateComponentDay.day > 1){
            [formatter setDateFormat:@"MM-dd"];
        } else {
            [formatter setDateFormat:@"hh:mm"];
        }
    }
    
    dateString = [formatter stringFromDate:timDate];
    if (dateComponentDay.day == 1) {
        dateString = @"yesterday";
    }
    
    return dateString;
}

//获取时间差
+ (int ) getTimeDiffFromDate :(NSDate *) dateString {
    int timeDiff = 0;
   
    NSDate *date1 = dateString;
    NSDate *date2 = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit type = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *cmps = [calendar components:type fromDate:date1 toDate:date2 options:0];
    timeDiff = (int) cmps.second;
    
    return timeDiff;
}

+ (long) getUserAge:(NSString *) ageString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *birdayDate =[dateFormatter dateFromString:ageString ];
    NSDateComponents *date = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:birdayDate toDate:[NSDate date] options:0];
    return date.year;
}

//+ (NSString *) getVersionString {
//    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    NSString *app_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
//    return app_version;
//}


+ (CGFloat) getTextHeightWithFont :(UIFont *) textFont getTextString:(NSString *) textString getFrameWidth:(CGFloat) frameWidth {
    
    if ([DataTool isEmptyString:textString]) {
        return 0;
    }
    NSDictionary *attributes = @{NSFontAttributeName:textFont};
    CGSize textSize = [textString boundingRectWithSize:CGSizeMake(frameWidth, SCREEN_Height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;;
    
    return textSize.height;
}


#pragma keyChain
+ (NSString *) getContentFromKeyChain:(NSString *) key {
    return [SAMKeychain passwordForService:@"DeviceUUID" account:key];
}

+ (BOOL) saveContentToKeyChain: (NSString *) content forKey: (NSString *) key service: (NSString *) service {
    return [SAMKeychain setPassword:content forService:service account:key];
}

#pragma HUD
+ (void) startShowHUD {
    [SVProgressHUD dismissWithDelay:60.0f];
    [SVProgressHUD show];
}

+ (void) dismissHUD {
    [SVProgressHUD dismiss];
}

+ (void) showHUDWithString:(NSString *)string {
    [SVProgressHUD showInfoWithStatus:string];
    [SVProgressHUD dismissWithDelay:2.0f];
}

+ (void) showToast:(NSString *)getString {
    [[UIApplication sharedApplication].delegate.window makeToast:getString duration:2.0 position:CSToastPositionBottom];
}

+ (void) showPhotoViewWithArray:(NSMutableArray *) photoArray andChooseIndex:(NSUInteger )chooseIndex {
    
    NSMutableArray *showPhotoArray = [NSMutableArray array];
    for (NSString *imgURLString in photoArray) {
        IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:imgURLString]];
        [showPhotoArray addObject:photo];
    }
    NSArray *photos = [NSArray arrayWithArray:showPhotoArray];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
    [browser setInitialPageIndex:chooseIndex];
    browser.displayToolbar = NO;
    browser.displayDoneButton = NO;
    browser.dismissOnTouch = YES;
    browser.usePopAnimation = NO;
    [[DataTool getCurrentVC] presentViewController:browser animated:YES completion:nil];
    
}

+ (void) showBannerViewWithTitle:(NSString *) titleString {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(16), StatusBarHeight + ZoomSize(10), ZoomSize(343), ZoomSize(80))];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(17), ZoomSize(311), ZoomSize(50))];
    tipLb.numberOfLines = 0;
    [view addSubview:tipLb];
    
    tipLb.text = titleString;
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
    tipLb.textColor = WhiteColor;
    tipLb.textAlignment = NSTextAlignmentCenter;
    
    view.layer.backgroundColor = RGB(58, 66, 68).CGColor;
    view.layer.cornerRadius = ZoomSize(16);
    
    EBCustomBannerView *customView = [EBCustomBannerView customView:view block:^(EBCustomBannerViewMaker *make) {
        make.portraitMode = EBCustomViewAppearModeTop;
        make.stayDuration = 3.0 ;
    }];
    
    [customView show];
}


+ (void) reportBlockUserName:(NSString *) userName andBlockUserId:(NSString *) BlockUserId reportCallback:(CallBack) reportCallback blockCallback:(CallBack) blockCallback {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Block & Report User" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    if (IS_IPad) {
        alertController = [UIAlertController alertControllerWithTitle:@"Block & Report User" message:nil preferredStyle: UIAlertControllerStyleAlert];
    }
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Report" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIAlertController *reportController = [UIAlertController alertControllerWithTitle:@"Select Reason" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
        if (IS_IPad) {
            reportController = [UIAlertController alertControllerWithTitle:@"Select Reason" message:nil preferredStyle: UIAlertControllerStyleAlert];
        }
        
        [reportController addAction:[UIAlertAction actionWithTitle:@"Harassment" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSDictionary *paramsDic = @{@"imgUrl": @"", @"description": @"Harassment", @"userId": BlockUserId };
            [[NetTool shareInstance] startRequest:API_user_report method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                [DataTool showBannerViewWithTitle:@"Your report has been submited successfully"];
//                [DataTool showToast:@"Your report has been submited successfully"];
                reportCallback(@"report success");
            }];
        }]];
        
        [reportController addAction:[UIAlertAction actionWithTitle:@"Fake gender" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSDictionary *paramsDic = @{@"imgUrl": @"", @"description": @"Fake gender", @"userId": BlockUserId };
            [[NetTool shareInstance] startRequest:API_user_report method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                [DataTool showBannerViewWithTitle:@"Your report has been submited successfully"];
//                [DataTool showToast:@"Your report has been submited successfully"];
                reportCallback(@"report success");
            }];
        }]];
        
        [reportController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [[DataTool getCurrentVC] presentViewController:reportController animated:YES completion:nil];
    }]];
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Block" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Are you sure you wanna block %@?",userName] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAlert = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        UIAlertAction *sureAlert = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSDictionary *paramsDic = @{@"isReject": [NSNumber numberWithBool:true],  @"uid": BlockUserId };
            
            if ([DataTool checkUserIsShare:BlockUserId]) {
                
                [DataTool showBannerViewWithTitle:@"Your Block has been submited successfully"];
                NSArray *blackList = [NSArray arrayWithObject:BlockUserId];
                [[V2TIMManager sharedInstance] addToBlackList:blackList  succ:^(NSArray<V2TIMFriendOperationResult *> *resultList) {
                } fail:^(int code, NSString *desc) {
                }];
                
                [[NetTool shareInstance] sendBlockUserNotification:BlockUserId];
                
                blockCallback(@"block success");
                
            } else {
                [[NetTool shareInstance] startRequest:API_user_reject method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                    
                    [DataTool showBannerViewWithTitle:@"Your Block has been submited successfully"];
                    NSArray *blackList = [NSArray arrayWithObject:BlockUserId];
                    [[V2TIMManager sharedInstance] addToBlackList:blackList  succ:^(NSArray<V2TIMFriendOperationResult *> *resultList) {
                    } fail:^(int code, NSString *desc) {
                    }];
                    
                    [[NetTool shareInstance] sendBlockUserNotification:BlockUserId];
                    
                    blockCallback(@"block success");
                }];
            }
            
        }];
        
        [alertController addAction:cancelAlert];
        [alertController addAction:sureAlert];
        [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[DataTool getCurrentVC].navigationController presentViewController:alertController animated:YES completion:nil];
}

+ (void)tg_blurryImage:(UIImage *)image withBlurLevel:(CGFloat)blur finishblock:(CallBack) finishblock{
    if (image == nil) {
        return ;
    }
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *ciImage = [CIImage imageWithCGImage:image.CGImage];
    
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:ciImage forKey:kCIInputImageKey];
    
    CGFloat setBlur = blur *(image.size.width / 200);
    
    [filter setValue:@(setBlur) forKey: @"inputRadius"]; //设置模糊程度
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    CGImageRef outImage = [context createCGImage: result fromRect:ciImage.extent];
    UIImage * blurImage = [UIImage imageWithCGImage:outImage];
    CGImageRelease(outImage);
    
    if (finishblock) {
        finishblock(blurImage);
    }
//    return blurImage;
}

+ (void)compressSizeImageToSize:(UIImage *)image setSize:(CGSize) setSize finishblock:(CallBack) finishblock {
    
    UIImage *resultImage = image;
    UIGraphicsBeginImageContext(setSize);
    [resultImage drawInRect:CGRectMake(0, 0, setSize.width, setSize.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (finishblock) {
        finishblock(resultImage);
    }
}


@end

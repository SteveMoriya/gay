//
//  SocketTool.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "SocketTool.h"
#import <MJExtension.h>
//#import "SocketRecieveModel.h"

static id _shareInstance = nil;

@interface SocketTool() <SRWebSocketDelegate> {
    //    int heartBeatCheckTime;
    NSTimer *heartBeatTimer;
    CallBack socketCallback;
    UserProfileDataModel *otherUserModel;
    NSDictionary *createRoomParamsDic;
    
    BOOL userInVideo;
}

@property (nonatomic, assign) int openSocketDelay;

@end

@implementation SocketTool

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[SocketTool alloc] init];
    });
    return _shareInstance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - webSocket

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"--------webSocketDidOpen");
    
//    [Flurry logEvent:@"Data_socketConnect_Action"];
//
//    if (userInVideo) {
//        [self sendVideoHeartBeat];
//        [self startVideoHeartBeatTimer];
//    } else {
//        [self sendHeartBeat];
//        [self startHeartBeatTimer]; //发送心跳
//    }
    
    [self sendHeartBeat];
    [self startHeartBeatTimer]; //发送心跳
    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"--------didReceiveMessage message %@",message);
    
    _openSocketDelay = 2;
    _reConnectTime = 0;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(NSString *)string {
    NSLog(@"--------didReceiveMessageWithString string %@",string);
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data {
    NSLog(@"--------didReceiveMessageWithData data %@",data);
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"--------didFailWithError error %@",error);
    [self stopHeartBeatTimer];
    [self reconnectSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(nullable NSString *)reason wasClean:(BOOL)wasClean {
    
    NSLog(@"--------didCloseWithCode code %ld reaseon %@",(long)code,reason);
    [self stopHeartBeatTimer];
    [self reconnectSocket];
}

#pragma socketFunction
- (void) setupWebSocket {
    [self performSelector:@selector(openSocket) withObject:nil afterDelay:1];
}

- (void)openSocket {
    if (self.webSocket.readyState == SR_OPEN) { //已连上就返回
        return;
    }
    
    if ( ![DataTool isEmptyString:[UserInfo shareInstant].token] && ![DataTool isEmptyString:[UserInfo shareInstant].user.userId]) {
        _reConnectTime = 0; //重设尝试次数
        self.webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", Socket_URL, [UserInfo shareInstant].user.userId]]];
        
        NSLog(@"%@", [NSString stringWithFormat:@"%@/%@", Socket_URL, [UserInfo shareInstant].user.userId]);
        
        self.webSocket.delegate = self;
        [self.webSocket open];
    }
}

- (void)reconnectSocket {
    
    if ( ![DataTool isEmptyString:[UserInfo shareInstant].token]) { //判断，当用户没有登录，或者socket没有初始化成功，不执行登录
        if (_reConnectTime < 5) {
            [self performSelector:@selector(openSocket) withObject:nil afterDelay:_openSocketDelay];
            _reConnectTime ++;
            _openSocketDelay = _openSocketDelay + 5;
        }
    }
}

- (void)closeSocket {
    _openSocketDelay = 2;
    [SocketTool shareInstance].reConnectTime = 5;
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket close];
    }
    [self stopHeartBeatTimer];
}

- (void)stopHeartBeatTimer {
    [heartBeatTimer invalidate];
    heartBeatTimer = nil;
}

- (void)startHeartBeatTimer {
    [self stopHeartBeatTimer];
    if (heartBeatTimer == nil) {
        heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(sendHeartBeat) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:heartBeatTimer forMode:NSRunLoopCommonModes];
    }
}

- (void)sendHeartBeat {
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket send:@"0"];
    }
}

- (void)sendInfoToSocket :(NSDictionary *)infoDic {
    
//    if (self.webSocket.readyState == SR_OPEN) {
//        [self.webSocket send:[infoDic mj_JSONString]];
//
//        NSString *typeString = infoDic[@"type"];
//        if ([typeString isEqualToString:@"1"]) { //判断用户发出
//            getUserId = infoDic[@"userId"];
//            getLiveId = infoDic[@"liveId"];
//            getRoomId = infoDic[@"roomId"];
//        }
//
//        NSLog(@"---- %@", infoDic);
//    }
}



@end

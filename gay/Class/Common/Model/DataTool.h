//
//  DataTool.h
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TUIKitConfig.h"
#import "TUIKit.h"

NS_ASSUME_NONNULL_BEGIN

@interface DataTool : NSObject

+ (void) showLoginPage;
+ (void) showHomePage;
+ (void) showBuyVipView;
+ (void) openWebVC:(NSString *) urlString ;
//+ (void) openUserProfileView:(UserProfileDataModel *)dataModel andClickImgIndex:(int) clickImgIndex;
+ (void) openUserProfileView:(UserProfileDataModel *)dataModel andClickImgIndex:(int) clickImgIndex isFromShare:(BOOL) isFromShare;
+ (void) showChatViewControllerWithType:(TIMConversationType)type receiver:(NSString*)receiverId receiverProfileModel:(UserProfileDataModel *)profileModel;

+ (UIViewController *) getCurrentVC;
+ (UIImage *) compressSizeImage:(UIImage *)image;
+ (UIImage *) imageWithColor:(UIColor *)color;

+ (BOOL) isEmptyString:(NSString *)string;
+ (BOOL) isValidateEmail:(NSString *)email;
+ (BOOL) checkUserIsShare:(NSString *)userID;
+ (BOOL) saveContentToKeyChain: (NSString *) content forKey: (NSString *) key service: (NSString *) service;

+ (NSString *) lowercaseInputString:(NSString *) inputString;
+ (NSString *) getDeviceUUID;
+ (NSString *) getTimeNow ;
+ (NSString *) getTimeStampString;
+ (NSString *) getTimTimeFromString :(NSDate *)timDate;
+ (NSString *) getContentFromKeyChain:(NSString *)key;

+ (int) getTimeDiffFromDate :(NSDate *) dateString;
+ (long) getUserAge:(NSString *) ageString;
//+ (NSString *) getVersionString;
+ (CGFloat) getTextHeightWithFont :(UIFont *) textFont getTextString:(NSString *) textString getFrameWidth:(CGFloat) frameWidth;

+ (void) startShowHUD;
+ (void) dismissHUD;
+ (void) showHUDWithString:(NSString *)string;
+ (void) showToast:(NSString *)getString;
+ (void) showBannerViewWithTitle:(NSString *) titleString;
+ (void) showPhotoViewWithArray:(NSMutableArray *) photoArray andChooseIndex:(NSUInteger )chooseIndex;
+ (void) reportBlockUserName:(NSString *) userName andBlockUserId:(NSString *) BlockUserId reportCallback:(CallBack) reportCallback blockCallback:(CallBack) blockCallback;

+ (void)tg_blurryImage:(UIImage *)image withBlurLevel:(CGFloat)blur finishblock:(CallBack) finishblock;
+ (void)compressSizeImageToSize:(UIImage *)image setSize:(CGSize) setSize finishblock:(CallBack) finishblock;

@end

NS_ASSUME_NONNULL_END

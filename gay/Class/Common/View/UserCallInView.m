//
//  UserCallInView.m
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserCallInView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserCallInView() {
    UIImageView *logoIV;
    UILabel *nameLb;
    UILabel *nationLb;
}

@end

@implementation UserCallInView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    self.layer.backgroundColor = RGB(58, 66, 68).CGColor;
    self.layer.cornerRadius = ZoomSize(16);
    self.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, ZoomSize(2));
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = ZoomSize(9);
    
    logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(12), ZoomSize(14), ZoomSize(67), ZoomSize(67))];
    [self addSubview:logoIV];
    logoIV.layer.cornerRadius = ZoomSize(33.5);
    logoIV.layer.masksToBounds = YES;
    logoIV.contentMode = UIViewContentModeScaleAspectFill;
    logoIV.image = [UIImage imageNamed:@"common_message_male"];
    
    nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(91), ZoomSize(23), ZoomSize(130), ZoomSize(21))];
    [self addSubview:nameLb];
    nameLb.textAlignment = NSTextAlignmentLeft;
    nameLb.textColor = WhiteColor;
    nameLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightSemibold];
    nameLb.text = @"William, 24";
    
    nationLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(91), ZoomSize(53), ZoomSize(130), ZoomSize(19))];
    [self addSubview:nationLb];
    nationLb.textColor = WhiteColor;
    nationLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    nationLb.text = @"Los Angles";
    
    UIButton *hungUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(230), ZoomSize(17), ZoomSize(60), ZoomSize(60))];
    [self addSubview:hungUpBt];
    [hungUpBt setImage:[UIImage imageNamed:@"common_hangup"] forState:UIControlStateNormal];
    [hungUpBt addTarget:self action:@selector(hungUpAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *getThoughBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(290), ZoomSize(17), ZoomSize(60), ZoomSize(60))];
    [self addSubview:getThoughBt];
    [getThoughBt setImage:[UIImage imageNamed:@"common_pickup"] forState:UIControlStateNormal];
    [getThoughBt addTarget:self action:@selector(getThoughAction) forControlEvents:UIControlEventTouchUpInside];
    
    CAKeyframeAnimation * keyAnimaion = [CAKeyframeAnimation animation]; //旋转动画
    keyAnimaion.keyPath = @"transform.rotation";
    keyAnimaion.values = @[@(-10 / 180.0 * M_PI),@(10 /180.0 * M_PI),@(-10/ 180.0 * M_PI)];//度数转弧度
    
    keyAnimaion.removedOnCompletion = NO;
    keyAnimaion.fillMode = kCAFillModeForwards;
    keyAnimaion.duration = 0.3;
    keyAnimaion.repeatCount = MAXFLOAT;
    [getThoughBt.layer addAnimation:keyAnimaion forKey:nil];
    
    CALayer *viewLayer = getThoughBt.layer; //    上下动画
    CGPoint position = viewLayer.position;
    CGPoint startPoint = CGPointMake(position.x , position.y + ZoomSize(5));
    CGPoint endPoint = CGPointMake(position.x , position.y - ZoomSize(5));
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [animation setFromValue:[NSValue valueWithCGPoint:startPoint]];
    [animation setToValue:[NSValue valueWithCGPoint:endPoint]];
    [animation setAutoreverses:YES];
    [animation setDuration:.3];
    [animation setRepeatCount:MAXFLOAT];
    [viewLayer addAnimation:animation forKey:nil];
    
}

- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    _profileModel = profileModel;
}

- (void)hungUpAction {
    NSLog(@"hungUpAction");
    if (_hungUpBlock) {
        _hungUpBlock();
    }
}

- (void)getThoughAction {
    NSLog(@"getThoughAction");
    if (_getThoughBlock) {
        _getThoughBlock();
    }
}


@end

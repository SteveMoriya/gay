//
//  BuyVipPopView.h
//  gay
//
//  Created by steve on 2020/10/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyVipPopView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

//
//  JKPickView.m
//  JKOCPickView
//
//  Created by 王冲 on 2018/5/15.
//  Copyright © 2018年 希艾欧科技有限公司. All rights reserved.
//

#import "JKPickView.h"

@interface JKPickView ()<UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate> {
    UIPickerView *pickview;
    UIButton *backView;
    UILabel *titleLb;
    //    NSArray *array1;          
    NSInteger selectRow;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation JKPickView

+(instancetype)setPickViewDate {
    return [[self alloc]init];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    singleFingerOne.numberOfTouchesRequired = 1;
    singleFingerOne.delegate = self;
    
    [self addGestureRecognizer:singleFingerOne];
}

#pragma mark 取消的的方法

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.3f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height - (ZoomSize(270) + BottomSafeAreaHeight)  , SCREEN_Width, ZoomSize(270) + BottomSafeAreaHeight)];
    }];
    
}

- (void)dismiss {
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.0f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, ZoomSize(270) + BottomSafeAreaHeight)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark  确定的方法
-(void)clicksure {
    //获取选中的列中的所在的行
    NSInteger row=[pickview selectedRowInComponent:0];
    
    //然后是获取这个行中的值，就是数组中的值
    if (self.back) {
        self.back(row);
        [self dismiss];
    }
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件包含的列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView {
    return 1; // 返回1表明该控件只包含1列
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件指定列包含多少个列表项
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.dataArray[row];
}

-(UIView*)pickerView:(UIPickerView*)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView*)view {
    
    //设置分割线的颜色
//    for(UIView *singleLine in pickerView.subviews) {
//        if (singleLine.frame.size.height < 1)
//        {
//            singleLine.backgroundColor = UIColor.lightGrayColor;
//        }
//    }
    
    UILabel *genderLabel = (UILabel *)view;
    if (!genderLabel) {
        genderLabel = [[UILabel alloc] init];
        genderLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    genderLabel.text = self.dataArray[row];
    genderLabel.font = [UIFont boldSystemFontOfSize:ZoomSize(26)];
    genderLabel.textColor = UIColor.whiteColor;
    
    return genderLabel;
}

// 自定义行高
-(CGFloat)pickerView:(UIPickerView*)pickerView rowHeightForComponent:(NSInteger)component {
    return ZoomSize(40);
}
// 自定义列宽
-(CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_Width;
}

// 当用户选中UIPickerViewDataSource中指定列和列表项时激发该方法
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    selectRow = row;
    [pickview reloadComponent:component];
}

-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

#pragma mark 选择的哪一个
-(void)setSelectedRow:(NSString *)selectedRow{
    [pickview reloadAllComponents];
    _selectedRow = selectedRow;
    [pickview selectRow:[selectedRow integerValue] inComponent:0 animated:NO];
}

- (void) setTitleString:(NSString *)titleString {
    titleLb.text = titleString;
}

#pragma lazy

- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width , ZoomSize(270) + BottomSafeAreaHeight)];
        _contentView.backgroundColor = RGB(38, 44, 45);
        _contentView.layer.cornerRadius = ZoomSize(10);
        _contentView.layer.masksToBounds = YES;
        
        UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0,0, SCREEN_Width, ZoomSize(50))];
        topView.backgroundColor = RGB(38, 44, 45);
        [_contentView addSubview:topView];
        
//        UIButton *buttonCancle = [[UIButton alloc]initWithFrame:CGRectMake(10,7, 60, 40)];
//        [buttonCancle setTitle:@"Cancel" forState:UIControlStateNormal];
//        if (@available(iOS 8.2, *)) {
//            buttonCancle.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
//        } else {
//            buttonCancle.titleLabel.font = [UIFont systemFontOfSize:16];
//        }
//        [buttonCancle setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
//        [buttonCancle addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
//        [topView addSubview: buttonCancle];
        
//        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width /2 - 80, 7, 160, 40)];
//        //        titleLb.text = @"Sort by";
//        titleLb.textAlignment = NSTextAlignmentCenter;
//        titleLb.textColor = UIColor.blackColor;
//        [topView addSubview: titleLb];
//        if (@available(iOS 8.2, *)) {
//            titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
//        } else {
//            titleLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
//        }
        
        UIButton *buttonSure = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(70), ZoomSize(5), ZoomSize(60), ZoomSize(40))];
        buttonSure.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
        
        [buttonSure setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [buttonSure setTitle:@"Done" forState:UIControlStateNormal];
        [buttonSure setTitleColor:ThemeYellow forState:UIControlStateNormal];
        buttonSure.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        [buttonSure addTarget:self action:@selector(clicksure) forControlEvents:UIControlEventTouchUpInside];
        [topView addSubview: buttonSure];
        
        UIView *headerLineView = [[UIView alloc] initWithFrame:CGRectMake(0,  ZoomSize(50), SCREEN_Width, ZoomSize(2))];
        headerLineView.backgroundColor = RGB(53, 61, 62);
        [topView addSubview:headerLineView];
        
        pickview = [[UIPickerView alloc]initWithFrame:CGRectMake(0, ZoomSize(54), _contentView.frame.size.width, _contentView.frame.size.height - ZoomSize(54))];
        pickview.backgroundColor = RGB(38, 44, 45);
        pickview.delegate = self;
        pickview.dataSource = self;
        pickview.tintColor = WhiteColor;
        [_contentView addSubview:pickview];
        
    }
    return _contentView;
    
}

@end



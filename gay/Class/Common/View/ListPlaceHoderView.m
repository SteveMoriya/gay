//
//  ListPlaceHoderView.m
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import "ListPlaceHoderView.h"

@interface ListPlaceHoderView() {
    NSString *imgNameString;
    NSString *tipsInfoString;
}

@end

@implementation ListPlaceHoderView

- (instancetype) initWithFrame:(CGRect)frame tipImgName:(NSString *)tipImgName tipsInfo:(NSString *)tipsInfo {
    self = [super initWithFrame:frame];
    if (self) {
        imgNameString = tipImgName;
        tipsInfoString = tipsInfo;
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.backgroundColor = BGBlackColor;
    
    UIImageView *centerImgV = [[UIImageView alloc] initWithFrame:CGRectMake(( frame.size.width - ZoomSize(50))/2.0, (frame.size.height)/2.0 - ZoomSize(100), ZoomSize(50), ZoomSize(50))];
    [self addSubview:centerImgV];
    centerImgV.image = [UIImage imageNamed:imgNameString];
    centerImgV.contentMode = UIViewContentModeScaleAspectFill;
    
    UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), CGRectGetMaxY(centerImgV.frame) + ZoomSize(20), SCREEN_Width - ZoomSize(40), ZoomSize(60))];
    [self addSubview:infoLb];
    infoLb.text = tipsInfoString;
    infoLb.numberOfLines = 0;
    infoLb.textAlignment = NSTextAlignmentLeft;
    infoLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
    infoLb.textAlignment = NSTextAlignmentCenter;
//    infoLb.textColor = BGDarkBlueColor;
    
}

@end

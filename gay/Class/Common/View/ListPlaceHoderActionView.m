//
//  ListPlaceHoderActionView.m
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import "ListPlaceHoderActionView.h"

@interface ListPlaceHoderActionView(){
    NSString *viewTitleString;
    NSString *tipsInfoString;
    NSString *btTitleString;
}

@end

@implementation ListPlaceHoderActionView

- (instancetype) initWithFrame:(CGRect)frame viewTitle:(NSString *)viewTitle tipsInfo:(NSString *)tipsInfo btTitle:(NSString *)btTitle {
    
    self = [super initWithFrame:frame];
    if (self) {
//        tipImgNameString = tipImgName;
        viewTitleString = viewTitle;
        tipsInfoString = tipsInfo;
        btTitleString = btTitle;
        [self initSubviews:frame];
    }
    return self;
    
}

- (void)initSubviews:(CGRect) frame {
    
    self.backgroundColor = BGBlackColor;
    
    UIImageView *centerImgV = [[UIImageView alloc] initWithFrame:CGRectMake(( frame.size.width - ZoomSize(92))/2.0, (frame.size.height)/2.0 - ZoomSize(200), ZoomSize(92), ZoomSize(92))];
    [self addSubview:centerImgV];
    centerImgV.image = [UIImage imageNamed:@"common_list_blank"];
    centerImgV.contentMode = UIViewContentModeScaleAspectFill;
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(60), CGRectGetMaxY(centerImgV.frame) + ZoomSize(80), SCREEN_Width - ZoomSize(120), ZoomSize(30))];
    titleLb.text = viewTitleString;
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = WhiteColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightSemibold];
    [self addSubview:titleLb];
    
    UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), CGRectGetMaxY(titleLb.frame), SCREEN_Width - ZoomSize(40), ZoomSize(40))];
    [self addSubview:infoLb];
    infoLb.text = tipsInfoString;
    infoLb.numberOfLines = 0;
    infoLb.textAlignment = NSTextAlignmentLeft;
    infoLb.font = [UIFont systemFontOfSize:ZoomSize(15)];
    infoLb.textAlignment = NSTextAlignmentCenter;
    infoLb.textColor = RGBA(126, 116, 136, 1);
    
    UIButton *actionBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), CGRectGetMaxY(infoLb.frame) + ZoomSize(50), SCREEN_Width - ZoomSize(72), ZoomSize(48))];
    [self addSubview:actionBt];
    actionBt.layer.cornerRadius = ZoomSize(24);
    [actionBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [actionBt setTitle:btTitleString forState:UIControlStateNormal];
    [actionBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    actionBt.backgroundColor = ThemeYellow;
    actionBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightSemibold];
    
    if ([DataTool isEmptyString:btTitleString]) {
        actionBt.hidden = YES;
    }
}

- (void)submitAction {
    
    if (self.actionBlock) {
        self.actionBlock();
    }
}

@end

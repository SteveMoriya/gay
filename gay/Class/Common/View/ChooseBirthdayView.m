//
//  ChooseBirthdayView.m
//  gay
//
//  Created by steve on 2020/10/27.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "ChooseBirthdayView.h"

@interface ChooseBirthdayView ()<UIGestureRecognizerDelegate> {
    UIDatePicker *datePicker;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation ChooseBirthdayView

+(instancetype)setChooseBirthdayView {
    return [[self alloc]init];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    singleFingerOne.numberOfTouchesRequired = 1;
    singleFingerOne.delegate = self;
    
    [self addGestureRecognizer:singleFingerOne];
}

#pragma mark - function

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.3f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height - (ZoomSize(270) + BottomSafeAreaHeight)  , SCREEN_Width, ZoomSize(270) + BottomSafeAreaHeight)];
    }];
    
}

- (void)dismiss {
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.0f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, ZoomSize(270) + BottomSafeAreaHeight)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)clicksure {
//    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"MMMM dd, yyyy"];
//    NSString *dateString = [dateFormat stringFromDate:datePicker.date];
//
//    //然后是获取这个行中的值，就是数组中的值
//    if (self.back) {
//        self.back(dateString);
//        [self dismiss];
//    }
    
    if (self.back) {
        self.back(datePicker.date);
        [self dismiss];
    }
    
}


#pragma mark - lazy

- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width , ZoomSize(270) + BottomSafeAreaHeight)];
        _contentView.backgroundColor = RGB(38, 44, 45);
        _contentView.layer.cornerRadius = ZoomSize(10);
        _contentView.layer.masksToBounds = YES;
        
        UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0,0, SCREEN_Width, ZoomSize(54))];
        topView.backgroundColor = RGB(38, 44, 45);
        [_contentView addSubview:topView];
        
        UIButton *buttonSure = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(70), ZoomSize(5), ZoomSize(60), ZoomSize(40))];
        buttonSure.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        
        [buttonSure setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [buttonSure setTitle:@"Done" forState:UIControlStateNormal];
        [buttonSure setTitleColor:ThemeYellow forState:UIControlStateNormal];
        buttonSure.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        [buttonSure addTarget:self action:@selector(clicksure) forControlEvents:UIControlEventTouchUpInside];
        [topView addSubview: buttonSure];
        
        UIView *headerLineView = [[UIView alloc] initWithFrame:CGRectMake(0,  ZoomSize(50), SCREEN_Width, ZoomSize(2))];
        headerLineView.backgroundColor = RGB(53, 61, 62);
        [topView addSubview:headerLineView];
        
        datePicker = [[UIDatePicker alloc] init];
        if (@available(iOS 13.4, *)) {
            datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.frame = CGRectMake((_contentView.frame.size.width - ZoomSize(280))/2, ZoomSize(40), ZoomSize(280), ZoomSize(230));
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        datePicker.minimumDate = [[NSDate date] dateByAddingTimeInterval: -86400.0 * (365 * 80 + 25)];
        datePicker.maximumDate = [[NSDate date] dateByAddingTimeInterval: -86400.0 * (365 * 18 + 6) ];
        
        datePicker.tintColor = WhiteColor;
        [datePicker setValue:WhiteColor forKey:@"textColor"];
        
        [_contentView addSubview:datePicker];
        
        
    }
    return _contentView;
    
}


@end

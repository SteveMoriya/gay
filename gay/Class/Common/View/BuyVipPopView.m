//
//  BuyVipPopView.m
//  gay
//
//  Created by steve on 2020/10/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "BuyVipPopView.h"
#import "PurchaseTool.h"
#import "VipInfoView.h"

@interface BuyVipPopView()<UITextViewDelegate> {
    int chooseIndex;
}

//@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) VipInfoView *vipInfoView;

@end

@implementation BuyVipPopView

- (instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    [self addSubview:self.contentScrollView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:NeedRefleshProfilePage object:nil]; //购买完消失
    
    //获取购买项目
    //    NSDictionary *paramsDic = @{};
    //    [[NetTool shareInstance] startRequest:API_purchaseProject method:GetMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
    //    }];
    
}

#pragma function

- (void) chooseAction:(UIButton *)sender {
    
    chooseIndex = (int) sender.tag - 100;
    
    for (int i = 0; i <3; i++) {
        UIButton *chooseBt = [_contentScrollView viewWithTag:100 + i];
        UILabel *titleLb  = [_contentScrollView viewWithTag:200 + i];
        UILabel *priceLb = [_contentScrollView viewWithTag:300 + i];
        UILabel *totalLb = [_contentScrollView viewWithTag:400 + i];
        
        titleLb.textColor =RGB(199, 199, 199);
        priceLb.textColor = RGB(199, 199, 199);
        totalLb.textColor = RGB(199, 199, 199);
        totalLb.backgroundColor = UIColor.clearColor;
        chooseBt.layer.borderColor = RGB(120, 120, 120).CGColor;
    }
    
    UIButton *chooseBt = [_contentScrollView viewWithTag:100 + chooseIndex];
    UILabel *titleLb  = [_contentScrollView viewWithTag:200 + chooseIndex];
    UILabel *priceLb = [_contentScrollView viewWithTag:300 + chooseIndex];
    UILabel *totalLb = [_contentScrollView viewWithTag:400 + chooseIndex];
    
    chooseBt.layer.borderColor = RGB(250, 180, 22).CGColor;
    titleLb.textColor = RGB(250, 180, 22);
    priceLb.textColor = RGB(250, 180, 22);
    totalLb.textColor = BlackColor;
    totalLb.backgroundColor = RGB(250, 180, 22);
}

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentScrollView setFrame:CGRectMake(0, 0 , SCREEN_Width, SCREEN_Height)];
    }];
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentScrollView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void) continueAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"BuyVip_submit"];
    
    
    NSString *chooseProduct = product_vip_3_month;
    switch (chooseIndex) {
        case 0: chooseProduct = product_vip_1_week; break;
        case 1: chooseProduct = product_vip_3_month; break;
        case 2: chooseProduct = product_vip_6_year; break;
        default: ; break;
    }
    
    [[PurchaseTool shareInstance] startCreateOrderWithPID:chooseProduct];
}

- (void)restoreAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"BuyVip_restore"];
    [[PurchaseTool shareInstance] restorePurchase:YES];
}

- (NSAttributedString *)getContentLabelAttributedText:(NSString *)text {
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(14)],NSForegroundColorAttributeName:UIColor.grayColor, NSParagraphStyleAttributeName: paragraphStyle}];
    
    NSDictionary *attribtDic1 = @{NSLinkAttributeName: @"Terms://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic1 range:NSMakeRange(text.length - 7, 7)];
    
    NSDictionary *attribtDic2 = @{NSLinkAttributeName: @"Privacy://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic2 range:NSMakeRange(text.length - 26, 14)];
    
    return attrStr;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    
    if ([[URL scheme] isEqualToString:@"Terms"]) {
        [DataTool openWebVC:TermURLString];
    } else if ([[URL scheme] isEqualToString:@"Privacy"]) {
        [DataTool openWebVC:PolicyURLString];
    }
    
    return YES;
}


#pragma Lazy
- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width, SCREEN_Height)];
        _contentScrollView.bounces = NO ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = BlackColor;
        
        if (@available(iOS 11.0, *)) { //设置内容到顶部
            _contentScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } 
        
#pragma vipInfoView
        [_contentScrollView addSubview:self.vipInfoView];
        
        UIImageView *titleBGIV = [[UIImageView alloc] initWithFrame:CGRectMake( 0 , 0, SCREEN_Width, ZoomSize(93))];
        titleBGIV.image = [UIImage imageNamed:@"vip_title_bg"];
        [_contentScrollView addSubview:titleBGIV];
        
        UIButton *closeBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(15), StatusBarHeight + ZoomSize(12), ZoomSize(30), ZoomSize(30))];
        [closeBt addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [closeBt setImage:[UIImage imageNamed:@"nav_close_gray"] forState:UIControlStateNormal];
        [closeBt setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_contentScrollView addSubview:closeBt];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( (SCREEN_Width - ZoomSize(100))/2, StatusBarHeight + ZoomSize(12), ZoomSize(100), ZoomSize(30))];
        [_contentScrollView addSubview:titleLb];
        titleLb.text = @"Get Vip";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
#pragma vip 选择按钮
//        NSArray *timeArray = [NSArray arrayWithObjects:@"1Month",@"3Months",@"6Months", nil];
//        NSArray *priceArray = [NSArray arrayWithObjects:@"$14.99",@"$39.99", @"$69.99", nil];
//        NSArray *totalArray = [NSArray arrayWithObjects:@"$14.99/mo", @"$13.33/mo", @"$11.66/mo", nil];
        
        NSArray *timeArray = [NSArray arrayWithObjects:@"Free 3 Days",@"Free 3 Days",@"Free 3 Days", nil];
        NSArray *priceArray = [NSArray arrayWithObjects:@"$2.99",@"$9.99", @"$99.99", nil];
        NSArray *totalArray = [NSArray arrayWithObjects:@"1 Week", @"1 Month", @"1 Year", nil];
        
        
        CGFloat btWith = (SCREEN_Width - ZoomSize(40))/3.0;
        
        for (int i=0; i< 3; i++) {
            UIButton *chooseBt  = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(15) + (btWith + ZoomSize(5) )*i, ZoomSize(350), btWith, ZoomSize(130))];
            chooseBt.layer.cornerRadius = ZoomSize(8);
            chooseBt.layer.masksToBounds = YES;
            chooseBt.tag = 100 + i;
            
            [_contentScrollView addSubview:chooseBt];
            chooseBt.layer.borderColor = RGB(120, 120, 120).CGColor;
            chooseBt.layer.borderWidth = ZoomSize(1);
            [chooseBt addTarget:self action:@selector(chooseAction:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(15), btWith, ZoomSize(22))];
            titleLb.textAlignment = NSTextAlignmentCenter;
            titleLb.textColor = RGB(199, 199, 199);
            titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
            titleLb.text = timeArray[i];
            [chooseBt addSubview:titleLb];
            titleLb.tag = 200 + i;
            
            UILabel *priceLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(50), btWith, ZoomSize(30))];
            priceLb.textAlignment = NSTextAlignmentCenter;
            priceLb.textColor = RGB(199, 199, 199);
            priceLb.font = [UIFont systemFontOfSize:ZoomSize(26) weight:UIFontWeightBold];
            priceLb.text = priceArray[i];
            [chooseBt addSubview:priceLb];
            priceLb.tag = 300 + i;
            
            UILabel *totalLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(90), btWith, ZoomSize(40))];
            totalLb.textAlignment = NSTextAlignmentCenter;
            totalLb.textColor = RGB(199, 199, 199);;
            totalLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
            totalLb.text = totalArray[i];
            [chooseBt addSubview:totalLb];
            totalLb.tag = 400 + i;
            
            if (i == 1) {
                chooseBt.layer.borderColor = RGB(250, 180, 22).CGColor; //设置默认
                titleLb.textColor = RGB(250, 180, 22);
                priceLb.textColor = RGB(250, 180, 22);
                
                totalLb.textColor = BlackColor;
                totalLb.backgroundColor = RGB(250, 180, 22);
                chooseIndex = 1;
            }
        }
        
#pragma continueBt
        UIButton *continueBt = [[UIButton alloc] initWithFrame:CGRectMake((Screen_Width - ZoomSize(313))/2.0, ZoomSize(500), ZoomSize(313), ZoomSize(48)) ];
        [_contentScrollView addSubview:continueBt];
        continueBt.layer.cornerRadius = ZoomSize(24);
        continueBt.layer.masksToBounds = YES;
        [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
        
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = continueBt.bounds;
        gl.startPoint = CGPointMake(0.05, 0);
        gl.endPoint = CGPointMake(0.96, 1);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:250/255.0 green:180/255.0 blue:22/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:250/255.0 green:214/255.0 blue:22/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:250/255.0 green:180/255.0 blue:22/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(0.5f), @(1.0f)];
        [continueBt.layer addSublayer:gl];
        
        UILabel *continueBtTitleLb = [[UILabel alloc] initWithFrame:continueBt.bounds];
        [continueBt addSubview:continueBtTitleLb];
        continueBtTitleLb.text = @"Start Free Trial";
        continueBtTitleLb.textAlignment = NSTextAlignmentCenter;
        continueBtTitleLb.textColor = BlackColor;
        continueBtTitleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightMedium];
        
#pragma restoreBt
        UIButton *restoreBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(560), SCREEN_Width - ZoomSize(76), ZoomSize(30))];
        [restoreBt setTitle:@"Restore" forState:UIControlStateNormal];
        [restoreBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        restoreBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [_contentScrollView addSubview:restoreBt];
        [restoreBt addTarget:self action:@selector(restoreAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma Terms &Privacy
        NSString *content = @"Recurring billing, cancel anytime.\n By tapping Continue, your payment will be charged to your iTunes account, and your subscription will automatically renewfor the same package length at the same price until you cancel in settings in the iTunes store at least 24 hours prior to the end of the current period. By tapping Continue, you agree to our Privacy Policy and Terms. ";
        UITextView *contentTextView = [[UITextView alloc]  initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(600), SCREEN_Width - ZoomSize(32), ZoomSize(60) )];
        [_contentScrollView addSubview:contentTextView];
        
        contentTextView.text = content;
        contentTextView.attributedText = [self getContentLabelAttributedText:content];
        contentTextView.backgroundColor = UIColor.clearColor;
        contentTextView.linkTextAttributes = @{NSForegroundColorAttributeName:UIColor.whiteColor};
        
        contentTextView.textAlignment = NSTextAlignmentCenter;
        contentTextView.delegate = self;
        contentTextView.editable = NO;
        contentTextView.scrollEnabled = YES;
        
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(contentTextView.frame) + Bottom_SafeHeight);
    }
    return _contentScrollView;
}

- (VipInfoView *)vipInfoView {
    if (!_vipInfoView ) {
        _vipInfoView = [[VipInfoView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(322)) currentIndex:0];
    }
    return  _vipInfoView;
}

@end

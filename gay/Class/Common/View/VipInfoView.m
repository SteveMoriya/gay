//
//  VipInfoView.m
//  gay
//
//  Created by steve on 2020/10/22.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "VipInfoView.h"

@interface VipInfoView() <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, assign) int currentIndex;
@property(strong ,nonatomic) NSTimer *bannerTimer;

@end

@implementation VipInfoView

- (instancetype) initWithFrame:(CGRect)frame currentIndex:(int) currentIndex {
    self = [super initWithFrame:frame];
    if (self) {
        _currentIndex = currentIndex;
        [self initSubviews:frame];
        
        [self createTimer];
    }
    return self;
}

- (void)initSubviews: (CGRect) frame {
    self.frame = frame;
    [self addSubview:self.scrollView];
    [self addSubview:self.pageControl];
}


- (void) createTimer {
    _bannerTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(changeAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_bannerTimer forMode:NSRunLoopCommonModes];
}

- (void) stopTimer {
    [self.bannerTimer invalidate];
    self.bannerTimer = nil;
}

- (void)changeAction {
    
    if (_currentIndex == VipPicArray.count - 1 ) {
        _currentIndex = 0;
        [_scrollView setContentOffset:CGPointMake(self.frame.size.width  * -1 , 0) animated:NO];
    } else {
        _currentIndex ++;
    }
    
    [_scrollView setContentOffset:CGPointMake(self.frame.size.width  * _currentIndex , 0) animated:YES];
    _pageControl.currentPage = _currentIndex;
    
}


-(void)tapAction:(id)tap {
}

#pragma mark - scrollview delegate

/**
 手指开始拖动的时候, 就让计时器停止
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self stopTimer];
    self.bannerTimer = nil ;
}
/**
 手指离开屏幕的时候, 就让计时器开始工作
 */
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self createTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _currentIndex = (_scrollView.contentOffset.x + ZoomSize(1) ) / self.frame.size.width;
    _pageControl.currentPage = _currentIndex;
    if (_scrollAction) {
        _scrollAction(_currentIndex);
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    _currentIndex = (_scrollView.contentOffset.x + ZoomSize(1) ) / self.frame.size.width;
    _pageControl.currentPage = _currentIndex;
    if (_scrollAction) {
        _scrollAction(_currentIndex);
    }
}

#pragma mark - lazy
- (UIScrollView *) scrollView {
    
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];
        _scrollView.bounces = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake( self.frame.size.width * VipPicArray.count, 0);
        
        
        for (int i = 0; i < VipPicArray.count; i++) {
            UIImageView *imageIV = [[UIImageView alloc] initWithFrame:CGRectMake( self.frame.size.width * i , ZoomSize(0), self.frame.size.width, self.frame.size.width *0.86)];
            imageIV.image = [UIImage imageNamed:VipPicArray[i]];
            imageIV.contentMode = UIViewContentModeScaleAspectFill;
            imageIV.layer.masksToBounds = YES;
            [_scrollView addSubview:imageIV];
            
            
            UIImageView *infoBGIV = [[UIImageView alloc] initWithFrame:CGRectMake( self.frame.size.width * i , self.frame.size.width - ZoomSize(153), self.frame.size.width, ZoomSize(153))];
            infoBGIV.image = [UIImage imageNamed:@"vip_info_bg"];
            [_scrollView addSubview:infoBGIV];
            
            UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width * i , ZoomSize(230), self.frame.size.width, ZoomSize(30))];
            titleLb.text = VipTitleArray[i];
            titleLb.textAlignment = NSTextAlignmentCenter;
            titleLb.textColor = WhiteColor;
            titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightSemibold];
            [_scrollView addSubview:titleLb];
            
            UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width * i, ZoomSize(255), self.frame.size.width, ZoomSize(45))];
            tipLb.text = VipInfoArray[i];
            tipLb.textAlignment = NSTextAlignmentCenter;
            tipLb.textColor = WhiteColor;
            tipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
            tipLb.numberOfLines = 0;
            [_scrollView addSubview:tipLb];
            
            [_scrollView setContentOffset:CGPointMake( _currentIndex * self.frame.size.width, 0) animated:NO];
            
//            UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
//            [_scrollView addGestureRecognizer:tapGesturRecognizer];
            
        }
    }
    return _scrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - ZoomSize(15) ,self.frame.size.width, ZoomSize(6))];
        
        _pageControl.numberOfPages = VipPicArray.count;
        _pageControl.currentPage = _currentIndex;
        
        if (@available(iOS 14.0, *)) {
            _pageControl.currentPageIndicatorTintColor = WhiteColor;
            _pageControl.pageIndicatorTintColor = TextGray;
        } else {
            [_pageControl setValue:[UIImage imageNamed:@"pageControl_current"] forKeyPath:@"_currentPageImage"];
            [_pageControl setValue:[UIImage imageNamed:@"pageControl_bg"] forKeyPath:@"_pageImage"];
        }
        
    }
    return _pageControl;
}

@end

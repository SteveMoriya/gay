//
//  UserCallOutView.m
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserCallOutView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserCallOutView() {
    
    UIImageView *logoIV;
    
    UILabel *timeLb;
    UIButton *callOutHangUpBt;
    UIView *localView; //用户预览view
    
    NSTimer *codeTimer;
    int callCheckTime;
}

@property (nonatomic, strong) UserProfileDataModel *profileModel;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *callInfoView;

@end

@implementation UserCallOutView


- (instancetype) initWithFrame:(CGRect)frame profileModel:(UserProfileDataModel *)profileModel {
    self = [super initWithFrame:frame];
    if (self) {
        self.profileModel = profileModel;
        [self addSubview:self.contentView];
        [self setData];
    }
    return self;
}

- (void) setData {
    
    [logoIV sd_setImageWithURL:[NSURL URLWithString:_profileModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    
    timeLb.text = @"60s";
    callOutHangUpBt.hidden = YES;
    [self startTimer];
}
#pragma function
- (void)show {
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( 0, 0, SCREEN_Width ,SCREEN_Height)];
    }];
}

- (void)dismiss {
    [self stopTimer];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( 0, SCREEN_Height, SCREEN_Width ,SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)startTimer {
    callOutHangUpBt.hidden = NO;
    NSDate *senddate = [NSDate date];
    int date = [senddate timeIntervalSince1970];
    callCheckTime = date;
    
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:codeTimer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    [codeTimer invalidate];
    codeTimer = nil;
}

- (void)countDownAction {
    NSDate *senddate = [NSDate date];
    int date = [senddate timeIntervalSince1970];
    int lastTime = callCheckTime + 60 - date;
    
    if (lastTime <= 0) {
        [self dismiss];
    }
    
    timeLb.text = [NSString stringWithFormat:@"%ds",lastTime];
    
}

- (void) hangUpAction{
    NSLog(@"hangUpAction");
    if (_cancelBlock) {
        _cancelBlock();
    }
    
    [self dismiss];
}

#pragma lazy

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake( 0, SCREEN_Height, SCREEN_Width, SCREEN_Height)];
        [_contentView addSubview:self.callInfoView];
    }
    return _contentView;
}

- (UIView *)callInfoView {
    if (!_callInfoView) {
        _callInfoView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, SCREEN_Width, SCREEN_Height)];
        _callInfoView.backgroundColor = BGBlackColor;
        
        UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(100), StatusBarHeight +  ZoomSize(20), ZoomSize(175), ZoomSize(29))];
        [_callInfoView addSubview:tipLb];
        tipLb.text = @"Waiting for response...";
        tipLb.textAlignment = NSTextAlignmentCenter;
        tipLb.textColor = WhiteColor;
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        
        logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(33), StatusBarHeight +  ZoomSize(70), ZoomSize(309), ZoomSize(418))];
        logoIV.layer.cornerRadius = ZoomSize(10);
        logoIV.layer.masksToBounds = YES;
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        [_callInfoView addSubview:logoIV];
        
        timeLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(137), StatusBarHeight +  ZoomSize(505), ZoomSize(100), ZoomSize(20))];
        [_callInfoView addSubview:timeLb];
        timeLb.text = @"";
        timeLb.textAlignment = NSTextAlignmentCenter;
        timeLb.textColor = WhiteColor;
        timeLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
        
        callOutHangUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(129), StatusBarHeight + ZoomSize(520), ZoomSize(115), ZoomSize(115))];
        [_callInfoView addSubview:callOutHangUpBt];
        [callOutHangUpBt setImage:[UIImage imageNamed:@"common_hangup_big"] forState:UIControlStateNormal];
        [callOutHangUpBt addTarget:self action:@selector(hangUpAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _callInfoView;
}


@end

//
//  UserCallOutView.h
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCallOutView : UIView

@property (nonatomic, copy) ClickBlock cancelBlock;

- (instancetype) initWithFrame:(CGRect)frame profileModel:(UserProfileDataModel *)profileModel;

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

//
//  ListPlaceHoderActionView.h
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ListPlaceHoderActionView : UIView

- (instancetype) initWithFrame:(CGRect)frame viewTitle:(NSString *)viewTitle tipsInfo:(NSString *)tipsInfo btTitle:(NSString *)btTitle ;

@property (nonatomic, copy) ClickBlock actionBlock;


@end

NS_ASSUME_NONNULL_END

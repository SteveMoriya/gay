//
//  ChooseBirthdayView.h
//  gay
//
//  Created by steve on 2020/10/27.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChooseBirthdayView : UIView

@property(nonatomic,strong) void(^back)(NSDate *getBirthdayDate);
//@property(nonatomic,strong) void(^back)(NSInteger );

@property (nonatomic, copy) ClickBlock dismissBlock;

//@property(nonatomic,strong) NSMutableArray *dataArray;
//@property(nonatomic,strong) NSString *selectedRow;
//@property(nonatomic,strong) NSString *titleString;

+(instancetype)setChooseBirthdayView;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

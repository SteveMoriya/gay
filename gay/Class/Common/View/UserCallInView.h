//
//  UserCallInView.h
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCallInView : UIView

@property (nonatomic, copy) ClickBlock hungUpBlock;
@property (nonatomic, copy) ClickBlock getThoughBlock;
@property (nonatomic, strong) UserProfileDataModel *profileModel;

@end

NS_ASSUME_NONNULL_END

//
//  VipInfoView.h
//  gay
//
//  Created by steve on 2020/10/22.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VipInfoView : UIView

- (instancetype) initWithFrame:(CGRect)frame currentIndex:(int) currentIndex;

- (void) createTimer;
- (void) stopTimer;

@property (nonatomic ,strong) void(^scrollAction)(int scrollIndex);

@end

NS_ASSUME_NONNULL_END

//
//  PhotoBannerView.m
//  Video
//
//  Created by steve on 2020/4/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import "PhotoBannerView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PhotoBannerView() <UIScrollViewDelegate>

@property (nonatomic, assign) int pozitionNum;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *photoArray;

@end

@implementation PhotoBannerView


- (instancetype) initWithFrame:(CGRect)frame photoArray:(NSMutableArray *)photoArray andPageIndex:(int) pageIndex {
    self = [super initWithFrame:frame];
    if (self) {
        _photoArray = photoArray;
        _pozitionNum = pageIndex;
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    self.backgroundColor = BGBlackColor;
    [self addSubview:self.scrollView];
    [self addSubview:self.pageControl];
    [self.scrollView setContentOffset:CGPointMake(self.frame.size.width  * _pozitionNum , 0) animated:NO];
}

-(void)tapAction:(id)tap {
    
    if (self.clickIndexBlock) {
        self.clickIndexBlock([NSNumber numberWithInt:_pozitionNum]);
    }
    
    
//    UITapGestureRecognizer *tapGesturRecognizer = tap;
//    CGPoint tapPoint = [tapGesturRecognizer locationInView:_scrollView];
//    int pozitionNum = tapPoint.x / _scrollView.frame.size.width ;
    
//    BOOL containVideo = NO;
//    NSMutableArray *showPhotoArray = [NSMutableArray array];
//    for (UserResourceDataModel *model in _photoArray) {
//        if ([model.type isEqualToString:@"VIDEO"]) {
//            containVideo = YES;
//        } else if ([model.type isEqualToString:@"PIC"]) {
//            IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:model.name]];
//            [showPhotoArray addObject:photo];
//        }
//    }
//    NSArray *photos = [NSArray arrayWithArray:showPhotoArray];
//
//    int showPozitionNum = pozitionNum;
//    if (containVideo) { //判断是否包含视频
//        showPozitionNum = pozitionNum - 1;
//    }
//
//    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
//    [browser setInitialPageIndex:showPozitionNum];
//
//    browser.displayToolbar = NO;
//    browser.displayDoneButton = NO;
//    browser.dismissOnTouch = YES;
//    browser.usePopAnimation = NO;
//    [[DataTool getCurrentVC] presentViewController:browser animated:YES completion:nil];
    
}


#pragma mark - scrollview delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pozitionNum = scrollView.contentOffset.x / _scrollView.frame.size.width ;
    _pageControl.currentPage = _pozitionNum;
}

#pragma mark - lazy
- (UIScrollView *) scrollView {
    
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];
        _scrollView.bounces = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake( self.frame.size.width * _photoArray.count, 0);
        
        for (int i = 0; i < _photoArray.count; i++) {
            NSString  *imgURLString = _photoArray[i];
            UIImageView *imageIV = [[UIImageView alloc] initWithFrame:CGRectMake( self.frame.size.width * i , 0, self.frame.size.width , self.frame.size.height)];
            [imageIV sd_setImageWithURL:[NSURL URLWithString:imgURLString] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
            imageIV.contentMode = UIViewContentModeScaleAspectFill;
            imageIV.layer.masksToBounds = YES;
            [_scrollView addSubview:imageIV];
            
        }
        
        UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
        [_scrollView addGestureRecognizer:tapGesturRecognizer];
        
    }
    return _scrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - ZoomSize(25) ,self.frame.size.width, ZoomSize(6))];
        _pageControl.numberOfPages = _photoArray.count;
        _pageControl.currentPage = _pozitionNum;
        _pageControl.hidesForSinglePage = YES;
        
        if (@available(iOS 14.0, *)) {
            _pageControl.currentPageIndicatorTintColor = WhiteColor;
            _pageControl.pageIndicatorTintColor = TextGray;
        } else {
            [_pageControl setValue:[UIImage imageNamed:@"pageControl_current"] forKeyPath:@"_currentPageImage"];
            [_pageControl setValue:[UIImage imageNamed:@"pageControl_bg"] forKeyPath:@"_pageImage"];
        }
    }
    return _pageControl;
}

@end

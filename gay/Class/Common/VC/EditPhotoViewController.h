//
//  EditPhotoViewController.h
//  gay
//
//  Created by steve on 2020/9/16.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditPhotoViewController : UIViewController

@property (nonatomic, strong) UIImage *getImage;

@property (nonatomic, copy) CallBack callbackImg;


@end

NS_ASSUME_NONNULL_END

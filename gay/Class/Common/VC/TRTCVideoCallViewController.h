//
//  TRTCVideoCallViewController.h
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TRTCVideoCallViewController : UIViewController

@property (nonatomic, copy) ClickBlock dismissBLock;
@property (nonatomic, strong) UserProfileDataModel *profileModel;
@property (nonatomic, strong) NSString *roomId;


@end

NS_ASSUME_NONNULL_END

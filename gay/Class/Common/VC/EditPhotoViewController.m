//
//  EditPhotoViewController.m
//  gay
//
//  Created by steve on 2020/9/16.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "EditPhotoViewController.h"
#import <JPImageresizerView.h>

@interface EditPhotoViewController (){
    UIButton *rotateBtn;
    UIButton *cancleBtn;
    UIButton *recoveryBtn;
    UIButton *doneBtn;
    JPImageresizerView *imageresizerView;
}

@end

@implementation EditPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

- (void)viewDidAppear:(BOOL)animated {
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)setUI {
    
    self.view.backgroundColor = [UIColor blackColor];
   
    JPImageresizerConfigure *configure = [JPImageresizerConfigure defaultConfigureWithImage:_getImage make:^(JPImageresizerConfigure *configure) {
        configure
        .jp_frameType(JPClassicFrameType)
        .jp_viewFrame(CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight - 100))
        .jp_contentInsets(UIEdgeInsetsMake(ZoomSize(10), ZoomSize(10), ZoomSize(10), ZoomSize(10)))
        .jp_animationCurve(JPAnimationCurveLinear);
    }];
    
     __weak typeof(self) wSelf = self;
    imageresizerView = [[JPImageresizerView alloc] initWithConfigure:configure imageresizerIsCanRecovery:^(BOOL isCanRecovery) {
        __strong typeof(wSelf) sSelf = wSelf;
        if (!sSelf) return;
        sSelf->recoveryBtn.enabled = isCanRecovery;
    } imageresizerIsPrepareToScale:^(BOOL isPrepareToScale) {
    }];
    
    [self.view insertSubview:imageresizerView atIndex:0];
    
    rotateBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, SCREEN_Height - 100, 40, 40)];
    [self.view addSubview:rotateBtn];
    [rotateBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [rotateBtn addTarget:self action:@selector(rotationAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *conditionLineView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height - 60, SCREEN_Width , 1)];
    //    conditionLineView.backgroundColor = ThemeBlackColor;
    [self.view addSubview:conditionLineView];
    
    cancleBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, SCREEN_Height - 50, 70, 30)];
    [self.view addSubview:cancleBtn];
    [cancleBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    cancleBtn.titleLabel.textColor = [UIColor whiteColor];
    [cancleBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    
    recoveryBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width / 2 - 35, SCREEN_Height - 50, 70, 30)];
    [self.view addSubview:recoveryBtn];
    [recoveryBtn setTitle:@"Redo" forState:UIControlStateNormal];
    [recoveryBtn addTarget:self action:@selector(recoveryAction) forControlEvents:UIControlEventTouchUpInside];
    [recoveryBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [recoveryBtn setTitleColor:ThemeTextGrayColor forState:UIControlStateDisabled];
    recoveryBtn.enabled = NO;
    
    doneBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - 80, SCREEN_Height - 50, 70, 30)];
    [self.view addSubview:doneBtn];
    [doneBtn setTitle:@"Finish" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor = [UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void)rotationAction {
    [imageresizerView rotation];
}

- (void)cancelAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doneAction {
    
    [self->imageresizerView cropPictureWithCacheURL:nil errorBlock:^(NSURL *cacheURL, JPImageresizerErrorReason reason) {
        
    } completeBlock:^(UIImage *finalImage, NSURL *cacheURL, BOOL isCacheSuccess) {
        if (self->_callbackImg) {
            self->_callbackImg(finalImage);
            [[DataTool getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
}

- (void)recoveryAction {
    [imageresizerView recoveryByInitialResizeWHScale:YES];
}

@end

//
//  WebViewController.h
//  gay
//
//  Created by steve on 2020/9/14.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *url;

@end

NS_ASSUME_NONNULL_END

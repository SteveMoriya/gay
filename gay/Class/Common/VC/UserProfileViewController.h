//
//  UserProfileViewController.h
//  gay
//
//  Created by steve on 2020/11/4.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserProfileViewController : UIViewController

@property (nonatomic, assign) int clickImgIndex;
@property (nonatomic, assign) BOOL isFromShare;
@property (nonatomic, strong) UserProfileDataModel *profileDataModel;

@end

NS_ASSUME_NONNULL_END

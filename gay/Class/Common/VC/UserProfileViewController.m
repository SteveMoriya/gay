//
//  UserProfileViewController.m
//  gay
//
//  Created by steve on 2020/11/4.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserPostsViewController.h"
#import "UserProfileViewController.h"

#import "FollowedViewController.h"
#import "FollowingViewController.h"
#import "PhotoBannerView.h"

@interface UserProfileViewController ()<UIScrollViewDelegate>{
    UIButton *followBt;
    UIButton *videoBt;
    
    UIButton *likeBt;
    UIButton *messageBt;
    
    UIButton *popBt;
    UIButton *funBt;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation UserProfileViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    
    [self.view addSubview:self.contentScrollView];
        
    popBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight  , ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:popBt];
    [popBt setImage:[UIImage imageNamed:@"nav_close_gray"] forState:UIControlStateNormal];
    [popBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    funBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(45), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:funBt];
    [funBt setImage:[UIImage imageNamed:@"discover_report"] forState:UIControlStateNormal];
    [funBt addTarget:self action:@selector(reportBlockAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.navView]; //导航栏
    
    likeBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width/4.0 - ZoomSize(75), SCREEN_Height - ZoomSize(75), ZoomSize(150), ZoomSize(50))];
    [self.view addSubview:likeBt];
    likeBt.backgroundColor = RGB(41, 54, 56);
    likeBt.layer.cornerRadius = ZoomSize(25);
    [likeBt setTitle:@" Like" forState:UIControlStateNormal];
    likeBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    [likeBt setImage:[UIImage imageNamed:@"common_personal_like_normal"] forState:UIControlStateNormal];
    [likeBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    [likeBt addTarget:self action:@selector(likeAction) forControlEvents:UIControlEventTouchUpInside];
    
    messageBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width * (3.0/4.0) - ZoomSize(75), SCREEN_Height - ZoomSize(75), ZoomSize(150), ZoomSize(50))];
    [self.view addSubview:messageBt];
    messageBt.backgroundColor = RGB(41, 54, 56);
    messageBt.layer.cornerRadius = ZoomSize(25);
    [messageBt setTitle:@" Message" forState:UIControlStateNormal];
    messageBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    [messageBt setImage:[UIImage imageNamed:@"common_personal_messages"] forState:UIControlStateNormal];
    [messageBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    [messageBt addTarget:self action:@selector(messageAction) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setData {
    
    if ([_profileDataModel.userId isEqualToString:[UserInfo shareInstant].user.userId]) {
        followBt.hidden = YES;
        videoBt.hidden = YES;
        likeBt.hidden = YES;
        messageBt.hidden = YES;
        funBt.hidden = YES;
    }
    
    if ([_profileDataModel.like isEqual:@"1"]) {
        [followBt setTitle:@"Followed" forState:UIControlStateNormal];
        followBt.userInteractionEnabled = NO;
    }
    
    if ([_profileDataModel.love isEqual:@"1"]) {
        [likeBt setImage:[UIImage imageNamed:@"common_personal_like"] forState:UIControlStateNormal];
        [likeBt setTitle:@" Liked" forState:UIControlStateNormal];
        likeBt.userInteractionEnabled = NO;
    }
    
//    用户来自共享数据，不能执行like与follow
    if (_isFromShare) {
        followBt.hidden = YES;
        likeBt.hidden = YES;
    }
    
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) reportBlockAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserProfile_reportBlock"];
    
    [DataTool reportBlockUserName:_profileDataModel.nickName andBlockUserId:_profileDataModel.userId reportCallback:^(id  _Nullable obj) {
    } blockCallback:^(id  _Nullable obj) {
        [self backAction];
    }];
}

- (void) followAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserProfile_follow"];
    
    NSDictionary *paramsDic = @{@"like": [NSNumber numberWithBool:true],  @"id": _profileDataModel.userId };
    [[NetTool shareInstance] startRequest:API_user_like method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        [self->followBt setTitle:@"Followed" forState:UIControlStateNormal];
        self->followBt.userInteractionEnabled = NO;
    }];
}

- (void) videoCallAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserProfile_videoCall"];
    
    if ( ![[UserInfo shareInstant] checkUserIsVip]) {
        [DataTool showBuyVipView];
        return;
    }
    
    [[UserInfo shareInstant] showCallOutView:self->_profileDataModel];
}

- (void) likeAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserProfile_like"];
    
    NSDictionary *paramsDic = @{@"love": [NSNumber numberWithBool:true],  @"id": _profileDataModel.userId };
    [[NetTool shareInstance] startRequest:API_user_love method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        [self->likeBt setImage:[UIImage imageNamed:@"common_personal_like"] forState:UIControlStateNormal];
        [self->likeBt setTitle:@" Liked" forState:UIControlStateNormal];
        self->likeBt.userInteractionEnabled = NO;
    }];
}

- (void) followerAction {
    NSLog(@"followerAction");
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_follower"];
    FollowedViewController *vc = [[FollowedViewController alloc] init];
    vc.userId = _profileDataModel.userId;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) followingAction {
    NSLog(@"followingAction");
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Me_following"];
    FollowingViewController *vc = [[FollowingViewController alloc] init];
    vc.userId = _profileDataModel.userId;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) postAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Post_normalPosts"];
    UserPostsViewController *vc = [[UserPostsViewController alloc] init];
    vc.userId = _profileDataModel.userId;
    
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

- (void) messageAction {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserProfile_message"];
    if (![[UserInfo shareInstant] checkUserIsVip]) {
        [DataTool showBuyVipView];
        return;
    }
    
    NSLog(@"messageAction");
    [DataTool showChatViewControllerWithType:TIM_C2C receiver:_profileDataModel.userId receiverProfileModel:_profileDataModel];
}

#pragma mark - delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetToShow = ZoomSize(300);//根据距离，设置自定义导航栏颜色 //滑动多少就完全显示
    CGFloat alpha = 1 - (offsetToShow - scrollView.contentOffset.y) / offsetToShow;
    _navView.alpha = alpha;
}

#pragma Lazy

- (UIView *)navView {
    if (!_navView) {

        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navView.backgroundColor = BGBlackColor;
        _navView.alpha = 0;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_close_gray"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];

        UIButton *blockReportBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(45), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navView addSubview:blockReportBt];
        [blockReportBt setImage:[UIImage imageNamed:@"discover_report"] forState:UIControlStateNormal];
        [blockReportBt addTarget:self action:@selector(reportBlockAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.delegate = self;
        
        if (@available(iOS 11.0, *)) { //设置内容到顶部
            _contentScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
#pragma mark - photoView
        NSMutableArray *photoArray = [NSMutableArray arrayWithObjects:_profileDataModel.headImgUrl, nil];
        
        PhotoBannerView *photoBannerView = [[PhotoBannerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(554)) photoArray:photoArray andPageIndex:_clickImgIndex];
        
        [_contentScrollView addSubview:photoBannerView];
        
        photoBannerView.clickIndexBlock = ^(id  _Nullable obj) {
            if (photoArray.count > 0) {
                int clickIndex = [obj intValue];
                [DataTool showPhotoViewWithArray:photoArray andChooseIndex:clickIndex];
            }
            
        };
        
#pragma mark - basicinfo name Address height
        
        UIView *basicInfoBGView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(416), SCREEN_Width, ZoomSize(138))];
        [_contentScrollView addSubview:basicInfoBGView];
        
        CAGradientLayer *basicInfogl = [CAGradientLayer layer];
        basicInfogl.frame = basicInfoBGView.bounds;
        basicInfogl.startPoint = CGPointMake(0.5, 0);
        basicInfogl.endPoint = CGPointMake(0.5, 1);
        basicInfogl.colors = @[(__bridge id)[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0].CGColor, (__bridge id)[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.49].CGColor];
        basicInfogl.locations = @[@(0), @(1.0f)];
        [basicInfoBGView.layer addSublayer:basicInfogl];
        
        
        UILabel *userNameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(450), ZoomSize(300), ZoomSize(30))];
        [_contentScrollView addSubview:userNameLb];
        userNameLb.textColor = WhiteColor;
        userNameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightMedium];
        NSString *ageString = @"";
        if (![DataTool isEmptyString:_profileDataModel.age]) {
            ageString = [NSString stringWithFormat:@",%@", _profileDataModel.age];
        }
        userNameLb.text = [NSString stringWithFormat:@"%@%@",_profileDataModel.nickName, ageString] ;
        
        NSString *heightString = @"";
        if (![DataTool isEmptyString:_profileDataModel.height] && ![_profileDataModel.height isEqualToString:@"0"]) {
            heightString = [NSString stringWithFormat:@"%@cm", _profileDataModel.height];
        }
        
        NSString *identityString = @"";
        if (![DataTool isEmptyString:_profileDataModel.identity]) {
            identityString = [NSString stringWithFormat:@" / %@", _profileDataModel.identity];
        }
        
        NSString *roleString = @"";
        if (![DataTool isEmptyString:_profileDataModel.role]) {
            roleString = [NSString stringWithFormat:@" / %@", _profileDataModel.role];
        }
        
        NSString *addressString = @"";
        if (![DataTool isEmptyString:_profileDataModel.address]) {
            addressString = [NSString stringWithFormat:@"\n%@ ", _profileDataModel.address];
        }
        
        UILabel *heightLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(480), ZoomSize(300), ZoomSize(40))];
        heightLb.numberOfLines = 0;
        [_contentScrollView addSubview:heightLb];
        heightLb.text = [NSString stringWithFormat:@"%@ %@ %@ %@", heightString, identityString, roleString, addressString];
        heightLb.textColor = WhiteColor;
        heightLb.font = [UIFont systemFontOfSize:ZoomSize(14) weight:UIFontWeightMedium];
        
        followBt = [[UIButton alloc] initWithFrame:CGRectMake( Screen_Width - ZoomSize(150), ZoomSize(450), ZoomSize(130), ZoomSize(37))];
        [_contentScrollView addSubview:followBt];
        followBt.backgroundColor = RGB(250, 180, 22);
        followBt.layer.cornerRadius = ZoomSize(18.5);
        [followBt setTitle:@" Follow" forState:UIControlStateNormal];
        followBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        [followBt setImage:[UIImage imageNamed:@"common_add_follow"] forState:UIControlStateNormal];
        [followBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        [followBt addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        videoBt = [[UIButton alloc] initWithFrame:CGRectMake( Screen_Width - ZoomSize(150), ZoomSize(500), ZoomSize(130), ZoomSize(37))];
        [_contentScrollView addSubview:videoBt];
        videoBt.backgroundColor = RGB(250, 180, 22);
        videoBt.layer.cornerRadius = ZoomSize(18.5);
        [videoBt setTitle:@" VideoCall" forState:UIControlStateNormal];
        videoBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        [videoBt setImage:[UIImage imageNamed:@"messages_video_start"] forState:UIControlStateNormal];
        [videoBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        [videoBt addTarget:self action:@selector(videoCallAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma followView
        UIView *followView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(565), Screen_Width - ZoomSize(30), ZoomSize(63))];
        [_contentScrollView addSubview:followView];
        followView.layer.cornerRadius = ZoomSize(6);
        followView.layer.masksToBounds = YES;
        followView.backgroundColor = RGB(40, 46, 47);
        
        UILabel *followersNumLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(5), Screen_Width/3.0 - ZoomSize(10), ZoomSize(40))];
        [followView addSubview:followersNumLb];
        followersNumLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        followersNumLb.textAlignment = NSTextAlignmentCenter;
        followersNumLb.textColor = WhiteColor;
        followersNumLb.text = ([DataTool isEmptyString:_profileDataModel.fansNumber] ? @"0" : _profileDataModel.fansNumber);
        
        UILabel *followersNumTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(41), Screen_Width/3.0 - ZoomSize(10), ZoomSize(16))];
        [followView addSubview:followersNumTipLb];
        followersNumTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        followersNumTipLb.textAlignment = NSTextAlignmentCenter;
        followersNumTipLb.textColor = WhiteColor;
        followersNumTipLb.text = @"Follows";
        
        UIButton *followersBt = [[UIButton alloc] initWithFrame:CGRectMake(0,   ZoomSize(0), Screen_Width/3.0 - ZoomSize(10), ZoomSize(63))];
        [followView addSubview:followersBt];
        [followersBt addTarget:self action:@selector(followerAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *followLineView = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width/3.0 - ZoomSize(10) , ZoomSize(10), ZoomSize(1) , ZoomSize(40))];
        [followView addSubview:followLineView];
        followLineView.backgroundColor = RGB(54, 62, 63);
        
        UILabel *followingNumLb = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width/3.0 - ZoomSize(10) , ZoomSize(5), Screen_Width/3.0 - ZoomSize(10), ZoomSize(40))];
        [followView addSubview:followingNumLb];
        followingNumLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        followingNumLb.textAlignment = NSTextAlignmentCenter;
        followingNumLb.textColor = WhiteColor;
        followingNumLb.text = ([DataTool isEmptyString:_profileDataModel.concernNumber] ? @"0" : _profileDataModel.concernNumber);
        
        UILabel *followingNumTipLb = [[UILabel alloc] initWithFrame:CGRectMake( Screen_Width/3.0 - ZoomSize(10), ZoomSize(41), Screen_Width/3.0 - ZoomSize(10), ZoomSize(16))];
        [followView addSubview:followingNumTipLb];
        followingNumTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        followingNumTipLb.textAlignment = NSTextAlignmentCenter;
        followingNumTipLb.textColor = WhiteColor;
        followingNumTipLb.text = @"Following";
        
        UIButton *followingBt = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width/3.0 - ZoomSize(10), ZoomSize(0), Screen_Width/3.0 - ZoomSize(10), ZoomSize(63))];
        [followView addSubview:followingBt];
        [followingBt addTarget:self action:@selector(followingAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *followingLineView = [[UIView alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)) , ZoomSize(10), ZoomSize(1) , ZoomSize(40))];
        [followView addSubview:followingLineView];
        followingLineView.backgroundColor = RGB(54, 62, 63);
        
        UILabel *postNumbLb = [[UILabel alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)) , ZoomSize(5), Screen_Width/3.0 - ZoomSize(10), ZoomSize(40))];
        [followView addSubview:postNumbLb];
        postNumbLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        postNumbLb.textAlignment = NSTextAlignmentCenter;
        postNumbLb.textColor = WhiteColor;
        postNumbLb.text = ([DataTool isEmptyString:_profileDataModel.postNumber] ? @"0" : _profileDataModel.postNumber);
        
        UILabel *postNumTipLb = [[UILabel alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)), ZoomSize(41), Screen_Width/3.0 - ZoomSize(10), ZoomSize(16))];
        [followView addSubview:postNumTipLb];
        postNumTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        postNumTipLb.textAlignment = NSTextAlignmentCenter;
        postNumTipLb.textColor = WhiteColor;
        postNumTipLb.text = @"Posts";
        
        UIButton *postBt = [[UIButton alloc] initWithFrame:CGRectMake( 2 * (Screen_Width/3.0 - ZoomSize(10)), ZoomSize(0), Screen_Width/3.0 - ZoomSize(10), ZoomSize(63))];
        [followView addSubview:postBt];
        [postBt addTarget:self action:@selector(postAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma mark  about
        
        CGFloat aboutHeight = [DataTool getTextHeightWithFont:[UIFont systemFontOfSize:18] getTextString:_profileDataModel.intro getFrameWidth:Screen_Width - ZoomSize(30)] + ZoomSize(10);
        
        UILabel *contentLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), CGRectGetMaxY(followView.frame) + ZoomSize(10), Screen_Width - ZoomSize(30), aboutHeight)];
        contentLb.text = _profileDataModel.intro;
        contentLb.textColor = WhiteColor;
        contentLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        contentLb.numberOfLines = 0;
        [_contentScrollView addSubview:contentLb];
        
#pragma mark - bodytype language lookingfor
        
        UIView *advancedView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(contentLb.frame) + ZoomSize(5), SCREEN_Width, ZoomSize(180))];
        [_contentScrollView addSubview:advancedView];
        
        UIView *advancedLineView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(0), SCREEN_Width, ZoomSize(5))];
        [advancedView addSubview:advancedLineView];
        advancedLineView.backgroundColor = RGB(30, 34, 35);

        UILabel *bodyTypeTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(20), ZoomSize(100), ZoomSize(20))];
        [advancedView addSubview:bodyTypeTipLb];
        bodyTypeTipLb.text = @"Body type";
        bodyTypeTipLb.textColor = RGB(223, 223, 223);
        bodyTypeTipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];

        UILabel *bodyTypeLb = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width - ZoomSize(215), ZoomSize(20), ZoomSize(200), ZoomSize(20))];
        [advancedView addSubview:bodyTypeLb];
        bodyTypeLb.text = _profileDataModel.bodyType;
        bodyTypeLb.textColor = WhiteColor;
        bodyTypeLb.textAlignment = NSTextAlignmentRight;
        bodyTypeLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];

        UILabel *languageTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(60), ZoomSize(100), ZoomSize(20))];
        [advancedView addSubview:languageTipLb];
        languageTipLb.text = @"Languages";
        languageTipLb.textColor = RGB(223, 223, 223);
        languageTipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];

        UILabel *languageLb = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width - ZoomSize(215), ZoomSize(60), ZoomSize(200), ZoomSize(20))];
        [advancedView addSubview:languageLb];
        languageLb.text = _profileDataModel.languages;
        languageLb.textColor = WhiteColor;
        languageLb.textAlignment = NSTextAlignmentRight;
        languageLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];

        UILabel *enthnicityTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(100), ZoomSize(100), ZoomSize(20))];
        [advancedView addSubview:enthnicityTipLb];
        enthnicityTipLb.text = @"Enthnicity";
        enthnicityTipLb.textColor = RGB(223, 223, 223);
        enthnicityTipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];

        UILabel *enthnicityLb = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width - ZoomSize(215), ZoomSize(100), ZoomSize(200), ZoomSize(20))];
        [advancedView addSubview:enthnicityLb];
        enthnicityLb.text = _profileDataModel.ethnicity;
        enthnicityLb.textColor = WhiteColor;
        enthnicityLb.textAlignment = NSTextAlignmentRight;
        enthnicityLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];

        UILabel *lookingforTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(140), ZoomSize(100), ZoomSize(20))];
        [advancedView addSubview:lookingforTipLb];
        lookingforTipLb.text = @"Looking for";
        lookingforTipLb.textColor = RGB(223, 223, 223);
        lookingforTipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];

        UILabel *lookingforLb = [[UILabel alloc] initWithFrame:CGRectMake(Screen_Width - ZoomSize(215), ZoomSize(140), ZoomSize(200), ZoomSize(20))];
        [advancedView addSubview:lookingforLb];
        lookingforLb.text = _profileDataModel.lookFor;
        lookingforLb.textColor = WhiteColor;
        lookingforLb.textAlignment = NSTextAlignmentRight;
        lookingforLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(advancedView.frame) + ZoomSize(100) + BottomSafeAreaHeight);
        
    }
    return _contentScrollView;
}

@end

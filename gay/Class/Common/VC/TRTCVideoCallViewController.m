//
//  TRTCVideoCallViewController.m
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "TRTCVideoCallViewController.h"

#import "TUIKitConfig.h"
#import "TUIKit.h"
#import <TRTCCloud.h>
#import <TRTCCloudDelegate.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "TUIKitConfig.h"
#import "TUIKit.h"

@interface TRTCVideoCallViewController ()<TRTCCloudDelegate> {
    UIView *localView;
    UIView *remoteView;
    
    UILabel *timeLb;
    NSTimer *codeTimer;
    NSString *callCheckTime;
}

@end

@implementation TRTCVideoCallViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self startTimer];
    [self setdata];
}

- (void)setupUI {
    
    remoteView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:remoteView];
    remoteView.backgroundColor = BGBlackColor;
    
    localView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(224),Screen_Height - ZoomSize(330) - BottomSafeAreaHeight , ZoomSize(138), ZoomSize(185))];
    [self.view addSubview:localView];
    localView.layer.cornerRadius = ZoomSize(16);
    localView.layer.masksToBounds = YES;
    localView.backgroundColor = RGB(58, 66, 68);

    UIImageView* logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight, ZoomSize(47), ZoomSize(47))];
    [logoIV sd_setImageWithURL:[NSURL URLWithString:_profileModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    logoIV.layer.cornerRadius = ZoomSize(23.5);
    logoIV.contentMode = UIViewContentModeScaleAspectFill;
    logoIV.layer.masksToBounds = YES;
    [self.view addSubview:logoIV];
    
    UILabel *nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(67), StatusBarHeight + ZoomSize(22), ZoomSize(130), ZoomSize(24))];
//    nameLb.text =  [NSString stringWithFormat:@"%@,%@",  _profileModel.nickName, _profileModel.age];
    nameLb.text =  [NSString stringWithFormat:@"%@",  _profileModel.nickName];
    nameLb.textColor = WhiteColor;
    nameLb.adjustsFontSizeToFitWidth = YES;
    nameLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightSemibold];
    [self.view addSubview:nameLb];
    
//    UIButton *followBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(185), StatusBarHeight + ZoomSize(22), ZoomSize(85), ZoomSize(26))];
//    [self.view addSubview:followBt];
//    followBt.backgroundColor = RGB(250, 180, 22);
//    followBt.layer.cornerRadius = ZoomSize(13);
//    [followBt setTitle:@" Follow" forState:UIControlStateNormal];
//    followBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
//    [followBt setImage:[UIImage imageNamed:@"common_add_follow"] forState:UIControlStateNormal];
//    [followBt setTitleColor:WhiteColor forState:UIControlStateNormal];
//    [followBt addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *blockReportBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width -  ZoomSize(45), StatusBarHeight + ZoomSize(18) , ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:blockReportBt];
    [blockReportBt setImage:[UIImage imageNamed:@"discover_report"] forState:UIControlStateNormal];
    [blockReportBt addTarget:self action:@selector(reportAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *hangUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(130), Screen_Height - ZoomSize(145) - BottomSafeAreaHeight, ZoomSize(115), ZoomSize(115))];
    [self.view addSubview:hangUpBt];
    [hangUpBt setImage:[UIImage imageNamed:@"common_hangup_big"] forState:UIControlStateNormal];
    [hangUpBt addTarget:self action:@selector(hangUpAction) forControlEvents:UIControlEventTouchUpInside];
    
    timeLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(137), Screen_Height - ZoomSize(35) - BottomSafeAreaHeight , ZoomSize(100), ZoomSize(20))];
    [self.view addSubview:timeLb];
    timeLb.textAlignment = NSTextAlignmentCenter;
    timeLb.textColor = WhiteColor;
    timeLb.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightMedium];
    
}

- (void)setdata {
    
    [TRTCCloud sharedInstance].delegate = self;
    
    TRTCVideoEncParam *videoEncParam = [[TRTCVideoEncParam alloc] init]; /// 设置视频通话的画质（帧率 15fps，码率550, 分辨率 360*640）
    videoEncParam.videoResolution = TRTCVideoResolution_640_360;
    videoEncParam.resMode = TRTCVideoResolutionModePortrait;
    [[TRTCCloud sharedInstance] setVideoEncoderParam:videoEncParam];
    
    TXBeautyManager *beautyManager = [[TRTCCloud sharedInstance] getBeautyManager]; //    设置默认美颜效果（美颜效果：自然，美颜级别：5, 美白级别：1）
    [beautyManager setBeautyStyle:TXBeautyStyleSmooth];
    [beautyManager setBeautyLevel:5]; //美颜
    [beautyManager setWhitenessLevel:5]; //美白
    [beautyManager setRuddyLevel:1]; //红润

    [[TRTCCloud sharedInstance] setLocalViewFillMode:TRTCVideoFillMode_Fill];  //设置本地
    [[TRTCCloud sharedInstance] startLocalPreview:YES view:localView];
    [[TRTCCloud sharedInstance] startLocalAudio];
    
    
    [self enterTRTCRoom:[_roomId intValue]];
}

#pragma function
- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) followAction {
    NSLog(@"followAction");
}

- (void)reportAction {
    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
    } blockCallback:^(id  _Nullable obj) {
        [self hangUpAction];
    }];
}

- (void)hangUpAction {
    [[TRTCCloud sharedInstance] exitRoom];
    [self backAction];
}

- (void)enterTRTCRoom:(int ) getRoomId {
    TRTCParams *params = [[TRTCParams alloc] init];
    params.sdkAppId = [TXIMSDK_APP_KEY intValue];
    params.userId = [UserInfo shareInstant].user.userId;
    params.userSig = [UserInfo shareInstant].imSig;
    params.roomId = getRoomId;
    [[TRTCCloud sharedInstance] enterRoom:params appScene:TRTCAppSceneVideoCall];
}

- (void)startTimer {
    
    callCheckTime = [DataTool getTimeStampString];
    timeLb.text = @"00:00:00";
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:codeTimer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    [codeTimer invalidate];
    codeTimer = nil;
}

- (void)countDownAction {
    
    long dif = [[DataTool getTimeStampString] intValue] - [callCheckTime intValue];
    
    if (dif > 0) {
        timeLb.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(dif/3600)%24, (dif/60)%60, dif%60];
    }
    
}

#pragma TRTCCloud delegate
- (void)onEnterRoom:(NSInteger)result {
}

- (void)onError:(TXLiteAVError)errCode errMsg:(NSString *)errMsg extInfo:(NSDictionary *)extInfo {
}

- (void)onUserVideoAvailable:(NSString *)userId available:(BOOL)available {
    if (available) {
        [[TRTCCloud sharedInstance] startRemoteView:userId view:remoteView];
        [[TRTCCloud sharedInstance] setRemoteViewFillMode:userId mode:TRTCVideoFillMode_Fit];
        
    } else {
        [[TRTCCloud sharedInstance] stopRemoteView:userId];
    }
}

//- (void)onExitRoom:(NSInteger)reason {
////    [[TRTCCloud sharedInstance] exitRoom];
//}

- (void)onRemoteUserLeaveRoom:(NSString *)userId reason:(NSInteger)reason {
    [[TRTCCloud sharedInstance] exitRoom];
    [self backAction];
}

@end

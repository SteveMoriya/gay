//
//  PostTableViewCell.m
//  gay
//
//  Created by steve on 2020/11/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "PostTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/SDWebImageDownloader.h>
#import "TagListView.h"
#import "TagView.h"

@interface PostTableViewCell() {
    NSMutableArray *getPhotoList;
}

@property (weak, nonatomic) IBOutlet UIButton *closeBt;
@property (weak, nonatomic) IBOutlet UIButton *reportBt;

@property (weak, nonatomic) IBOutlet UIImageView *headIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLb;
@property (weak, nonatomic) IBOutlet UILabel *contentLb;

@property (weak, nonatomic) IBOutlet UIButton *messageBt;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;

@property (weak, nonatomic) IBOutlet UIView *imageContentView;
@property (weak, nonatomic) IBOutlet UIView *tagContentView;
@property (weak, nonatomic) IBOutlet UIView *addressView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLbHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageContentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagContentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressViewHeight;


@end

@implementation PostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setPostDataModel:(PostDataModel *)postDataModel {
    _postDataModel = postDataModel;
    
    if ([_postDataModel.userId isEqualToString:[UserInfo shareInstant].user.userId]) {
        _closeBt.hidden = NO;
        _reportBt.hidden = YES;
    } else {
        _closeBt.hidden = YES;
        _reportBt.hidden = NO;
    }
    
#pragma _nameLb
    _nameLb.text = _postDataModel.nickName;
    if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
        _nameLb.text = @"Anonymous user";
    }
    
#pragma _headIV
    if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:_postDataModel.headImgUrl] completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
            [DataTool compressSizeImageToSize:image setSize:self->_headIV.frame.size finishblock:^(id  _Nullable obj) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [DataTool tg_blurryImage:obj withBlurLevel:5 finishblock:^(id  _Nullable obj) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self->_headIV.image = obj;
                        });
                    }];
                });
            }];
        }];
        
    } else {
        [_headIV sd_setImageWithURL:[NSURL URLWithString:_postDataModel.headImgUrl] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
    }
    
#pragma _timeLb
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"];
    NSDate *date = [format dateFromString:_postDataModel.createTime];
    _timeLb.text = [DataTool getTimTimeFromString:date];
    
#pragma _contentLb
    _postDataModel.rowHeight = ZoomSize(80); //设置初始值
    if ( ![DataTool isEmptyString:_postDataModel.content]) {
        
        CGFloat contentHeight = [DataTool getTextHeightWithFont:[UIFont systemFontOfSize:ZoomSize(18)] getTextString:_postDataModel.content getFrameWidth:Screen_Width - ZoomSize(30)];
        _contentLb.text = _postDataModel.content;
        _contentLbHeight.constant = contentHeight;
        _postDataModel.rowHeight =  _postDataModel.rowHeight + contentHeight;
    }
    
#pragma _imageContentView
    _imageContentView.hidden = YES;
    _imageContentViewHeight.constant = 0;
    
    getPhotoList = [NSMutableArray array];
    [getPhotoList removeAllObjects];
    for (int i = 0; i < _postDataModel.msgList.count; i++) {
        if (![DataTool isEmptyString:_postDataModel.msgList[i]] && i < 3) {
            [getPhotoList addObject:_postDataModel.msgList[i] ];
        }
    }
    
    if (getPhotoList.count > 0) {
        
        for (UIView *subView in _imageContentView.subviews) {
            [subView removeFromSuperview];
        }
        
        _postDataModel.rowHeight =  _postDataModel.rowHeight + ZoomSize(110);
        _imageContentView.hidden = NO;
        _imageContentViewHeight.constant = ZoomSize(110);
        
        for (int i = 0; i < getPhotoList.count; i++) {
            
            UIImageView *msgIV = [[UIImageView alloc] initWithFrame:CGRectMake( ZoomSize(120) * (i%3) , ZoomSize(0) , ZoomSize(105), ZoomSize(105))];
            [_imageContentView addSubview:msgIV];
            msgIV.layer.cornerRadius = ZoomSize(4);
            msgIV.layer.masksToBounds = YES;
            msgIV.contentMode = UIViewContentModeScaleAspectFill;
            msgIV.image = [UIImage imageNamed:@"common_message_male"];
            
            if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
                
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:_postDataModel.msgList[i]] completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                    
                    [DataTool compressSizeImageToSize:image setSize:msgIV.frame.size finishblock:^(id  _Nullable obj) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [DataTool tg_blurryImage:obj withBlurLevel:5 finishblock:^(id  _Nullable obj) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    msgIV.image = obj;
                                });
                            }];
                            
                        });
                    }];
                }];
                
            } else {
                [msgIV sd_setImageWithURL:[NSURL URLWithString:_postDataModel.msgList[i]] placeholderImage:[UIImage imageNamed:@"common_message_male"]];
            }
            
            UIButton *photoBt = [[UIButton alloc] initWithFrame:msgIV.frame];
            [_imageContentView addSubview:photoBt];
            photoBt.tag = i;
            [photoBt addTarget:self action:@selector(contentImgAction:) forControlEvents:UIControlEventTouchUpInside];
            
            if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
                [photoBt setImage:[UIImage imageNamed:@"post_personal_eyes"] forState:UIControlStateNormal];
            }
        }
        
    }
    
#pragma _tagContentView
    
    _tagContentView.hidden = YES;
    _tagContentViewHeight.constant = 0;
    
    if ( [_postDataModel.storyType isEqual:@"1"] && ![DataTool isEmptyString:_postDataModel.tag]) {
        
        _messageBt.hidden = YES;
        if (![_postDataModel.userId isEqualToString:[UserInfo shareInstant].user.userId]) {
            _messageBt.hidden = NO;
        }
        
        for (UIView *subView in _tagContentView.subviews) {
            if (subView.tag == 100) {
                [subView removeFromSuperview];
            }
        }
        
        _postDataModel.rowHeight =  _postDataModel.rowHeight + ZoomSize(60);
        _tagContentView.hidden = NO;
        _tagContentViewHeight.constant = ZoomSize(60);
        
        NSArray *tagArray = [_postDataModel.tag componentsSeparatedByString:@","];
        if (tagArray.count > 0) {
            
            TagListView *personTagListView  = [[TagListView alloc] initWithFrame:CGRectMake(0, ZoomSize(30), Screen_Width - ZoomSize(80), ZoomSize(30))];
            [_tagContentView addSubview:personTagListView];
            personTagListView.tag = 100;
            personTagListView.tagBackgroundColor = RGB(34, 39, 41);
            personTagListView.cornerRadius = ZoomSize(6);
            personTagListView.paddingX = ZoomSize(8);
            personTagListView.paddingY = ZoomSize(6);
            personTagListView.marginX = ZoomSize(8);
            personTagListView.marginY = ZoomSize(6);
            personTagListView.tagViewHeight = ZoomSize(30);
            personTagListView.textColor = WhiteColor;
            personTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];

            for (NSString *textString in tagArray) {
                [[personTagListView addTag:textString] setOnTap:^(TagView *tagView) {
                }];
            }
        }
    }
    
#pragma _addressView
    
    _addressView.hidden = YES;
    _addressViewHeight.constant = 0;
    if ( ![DataTool isEmptyString:_postDataModel.address]) {
        _addressLb.text = _postDataModel.address;
        _postDataModel.rowHeight =  _postDataModel.rowHeight + ZoomSize(30);
        _addressView.hidden = NO;
        _addressViewHeight.constant = ZoomSize(30);
    }
    
    [self layoutIfNeeded];
}

//- (void) headIMGTouchAction {
//    NSLog(@"headIMGTouchAction");
//}

- (void) contentImgAction:(UIButton *)sender {
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"PostCell_Img"];
    
    if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
        [DataTool showBuyVipView];
        return;
    }
    
    [DataTool showPhotoViewWithArray:getPhotoList andChooseIndex:sender.tag];
}

- (IBAction)headBt:(id)sender {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"PostCell_headBt"];
    
    if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
        [DataTool showBuyVipView];
        return;
    }
    
    NSDictionary *userParams = @{@"id":_postDataModel.userId};
    
    [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
//        [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0];
        [DataTool openUserProfileView:userProfileDataModel andClickImgIndex:0 isFromShare:NO];
    }];
    
}

//- (IBAction)moreBt:(id)sender {
//    [DataTool reportBlockUserName:_postDataModel.nickName andBlockUserId:_postDataModel.userId reportCallback:^(id  _Nullable obj) {
//    } blockCallback:^(id  _Nullable obj) {
//    }];
//}

- (IBAction)closeAction:(id)sender {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"PostCell_close"];
//    NSLog(@"closeAction");
    if (_closeBlock) {
        _closeBlock();
    }
}

- (IBAction)reportAction:(id)sender {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"PostCell_reportBlock"];
    [DataTool reportBlockUserName:_postDataModel.nickName andBlockUserId:_postDataModel.userId reportCallback:^(id  _Nullable obj) {
    } blockCallback:^(id  _Nullable obj) {
    }];
}

- (IBAction)messageBt:(id)sender {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"PostCell_message"];
    if ( [_postDataModel.storyType isEqual:@"1"] && ![[UserInfo shareInstant] checkUserIsVip] ) {
        [DataTool showBuyVipView];
        return;
    }
    
    NSDictionary *userParams = @{@"id":_postDataModel.userId};
    [[NetTool shareInstance] startRequest:API_user_index method:GetMethod params:userParams needShowHUD:YES callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        [DataTool showChatViewControllerWithType:TIM_C2C receiver:userProfileDataModel.userId receiverProfileModel:userProfileDataModel];
    }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end

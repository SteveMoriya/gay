//
//  PostTableViewCell.h
//  gay
//
//  Created by steve on 2020/11/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostTableViewCell : UITableViewCell

@property (nonatomic, copy) ClickBlock closeBlock;

@property (nonatomic, copy) ClickBlock headImgBlock;
@property (nonatomic, copy) ClickBlock contentImgBlock;
@property (nonatomic, copy) ClickBlock messageBlock;

@property (nonatomic, strong) PostDataModel *postDataModel;

@end

NS_ASSUME_NONNULL_END

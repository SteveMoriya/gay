//
//  UserPostsView.m
//  gay
//
//  Created by steve on 2020/10/29.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserPostsView.h"
#import "MJRefresh.h"

#import "PostTableViewCell.h"
#import "PostDataModel.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"PostTableViewCell";

@interface UserPostsView()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
    UISwitch *seachSwitch;
}

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation UserPostsView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
        self.array = [NSMutableArray array];
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHeaderData) name:NeedRefleshProfilePage object:nil]; //购买完消失
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blockUser:) name:blockUserNotification object:nil];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.tableView];
//    [self addSubview:self.writePostBt];
}

#pragma mark - action

- (void)reloadData {
    [self.tableView reloadData];
}

- (void)getHeaderData {
    self->pageIndex = 1;
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserPostView_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserPostView_down"];
}

- (void)requestData {
    
//    userSortEnum:CREATE_TIME/DISTANCE
//    sortEnum:ASC 升序,DESC 降序
//    storyType: 0 公开类型 1 私密类型
    
    int story_Type = 0;
    NSString *searchSortEnum = @"CREATE_TIME"; //默认为时间排序
    
    if (_infoType == normalInfoType) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:UserPostSwitch]) {
            searchSortEnum = @"DISTANCE";
        }
    } else if (_infoType == anonymousInfoType) {
        story_Type = 1;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:UserAnonymousSwitch]) {
            searchSortEnum = @"DISTANCE";
        }
    }
    
    NSDictionary *paramsDic = @{ @"UserSortEnum":searchSortEnum,@"sortEnum":@"DESC" ,@"storyType": [NSNumber numberWithInt:story_Type], @"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:pageIndex] };
    
    [[NetTool shareInstance] startRequest:API_story_pageList method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
        }
        self->pageIndex ++;
        
        NSArray *storyArray = [PostDataModel mj_objectArrayWithKeyValuesArray:obj];
        [self.array addObjectsFromArray:storyArray];
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];
}

- (void) switchFunctionAction {
    NSLog(@"switchFunctionAction");
    
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserPostView_near"];
    
    if (_infoType == normalInfoType) {
        if (seachSwitch.isOn == YES) {
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:UserPostSwitch];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:UserPostSwitch];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
    } else if (_infoType == anonymousInfoType) {
        if (seachSwitch.isOn == YES) {
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:UserAnonymousSwitch];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:UserAnonymousSwitch];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    [self getHeaderData];
    
}

- (void) blockUser:(NSNotification *)notification{
    
    NSString *userIdString = notification.object;
    BOOL needDelete = NO;
    for (PostDataModel *obj in self.array) { //将系统的数据放在第一个
        if ([obj.userId isEqualToString:userIdString]) {
            needDelete = YES;
            break;
        }
    }
    
    if (needDelete) {
        [self getHeaderData];
    }
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PostDataModel *postDataModel = self.array[indexPath.row];
    return postDataModel.rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return ZoomSize(60);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *nearestView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(60))];
    nearestView.backgroundColor = BGBlackColor;
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(15), ZoomSize(130), ZoomSize(30))];
    [nearestView addSubview:tipLb];
    tipLb.text = @"Nearest to me";
    tipLb.textColor = WhiteColor;
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
    
    seachSwitch = [[UISwitch alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(15) - 52 , (ZoomSize(60) - 32)/2.0, 25, 32 )];
    seachSwitch.onTintColor = RGB(250, 180, 22);
    [seachSwitch addTarget:self action:@selector(switchFunctionAction) forControlEvents:UIControlEventValueChanged];
    [nearestView addSubview:seachSwitch];
    seachSwitch.on = NO;
    
    if (_infoType == normalInfoType) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:UserPostSwitch]) {
            seachSwitch.on = YES;
        }
    } else if (_infoType == anonymousInfoType) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:UserAnonymousSwitch]) {
            seachSwitch.on = YES;
        }
    }
    return nearestView;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(cell == nil) {
        cell = [[PostTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    PostDataModel *postModel = self.array[indexPath.row];
    cell.postDataModel = postModel;
    
    cell.closeBlock = ^{
//        [self.array removeObjectAtIndex:indexPath.row];
//        [self.tableView reloadData];
        
        NSDictionary *paramsDic = @{@"id":postModel.postId};
        [[NetTool shareInstance] startRequest:API_story_delete method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            [self.array removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
        }];
        
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - lazy

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width , self.frame.size.height) style:UITableViewStylePlain];
        _tableView.backgroundColor = UIColor.clearColor;
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];;
    }
    return _tableView;
}

#pragma mark - setFunction

- (void)setInfoType:(PostInfoType)infoType {
    _infoType = infoType;
}

@end

//
//  UserPostsView.h
//  gay
//
//  Created by steve on 2020/10/29.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserPostsView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

//@property (nonatomic, copy) CallBack infoBlock;
//@property (nonatomic, copy) CallBack chatBlock;

@property (nonatomic, strong) UINavigationController *getNav;
@property (nonatomic, assign) PostInfoType infoType;

- (void) getHeaderData;
//- (void) reloadData;

@end

NS_ASSUME_NONNULL_END

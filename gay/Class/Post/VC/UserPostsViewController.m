//
//  UserPostsViewController.m
//  gay
//
//  Created by steve on 2020/11/9.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "UserPostsViewController.h"
#import "NewPostViewController.h"
#import "MJRefresh.h"

#import "PostTableViewCell.h"
#import "PostDataModel.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

static NSString * const reuseIdentifier = @"PostTableViewCell";

@interface UserPostsViewController ()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation UserPostsViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.tableView];
    
    UIButton *normalPostsBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(90), Screen_Height - ZoomSize(80) - TabBarHeight, ZoomSize(72), ZoomSize(72))];
    [self.view addSubview:normalPostsBt];
    [normalPostsBt addTarget:self action:@selector(normalPostsBtAction) forControlEvents:UIControlEventTouchUpInside];
    [normalPostsBt setBackgroundImage:[UIImage imageNamed:@"post_release"] forState:UIControlStateNormal];
    normalPostsBt.hidden = YES;
    if ([_userId isEqualToString:[UserInfo shareInstant].user.userId]) {
        normalPostsBt.hidden = NO;
    }
    
}

- (void)setData {
    self.array = [NSMutableArray array];
    [self getHeaderData];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) normalPostsBtAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Post_normalPosts"];
    
    NewPostViewController *vc = [[NewPostViewController alloc] init];
    vc.infoType = normalInfoType;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)getHeaderData {
    self->pageIndex = 1;
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserPost_up"];
}

- (void)getFooterData {
    [self requestData];
    [[NetTool shareInstance] sendFireBaseWithEventName:@"UserPost_down"];
}

- (void)requestData {
    
    
    NSDictionary *paramsDic = @{ @"UserSortEnum":@"CREATE_TIME",@"sortEnum":@"DESC", @"pageSize": [NSNumber numberWithInt:20], @"pageNum": [NSNumber numberWithInt:pageIndex], @"userId":_userId };
    
    [[NetTool shareInstance] startRequest:API_story_pageList method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
        }
        self->pageIndex ++;
        
        NSArray *storyArray = [PostDataModel mj_objectArrayWithKeyValuesArray:obj];
        [self.array addObjectsFromArray:storyArray];
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];
    
}


#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PostDataModel *postDataModel = self.array[indexPath.row];
    return postDataModel.rowHeight;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(cell == nil) {
        cell = [[PostTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    PostDataModel *postModel = self.array[indexPath.row];
    cell.postDataModel = postModel;
    
    cell.closeBlock = ^{
        
        NSDictionary *paramsDic = @{@"id":postModel.postId};
        
        [[NetTool shareInstance] startRequest:API_story_delete method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            [self.array removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
        }];
    };
    
    return cell;
}


#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight,Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Posts List";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
    }
    return _navBgView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.backgroundColor = BGBlackColor;
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
        _tableView.rowHeight = ZoomSize(124);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds viewTitle:@"" tipsInfo:@"" btTitle:@""];
        
    }
    return _tableView;
}

@end

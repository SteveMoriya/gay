//
//  NewPostViewController.h
//  gay
//
//  Created by steve on 2020/10/29.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewPostViewController : UIViewController

@property (nonatomic, assign) PostInfoType infoType;
@property (nonatomic, strong) NSMutableArray *tagArray;

@end

NS_ASSUME_NONNULL_END

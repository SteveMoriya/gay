//
//  AnonyousPostViewController.m
//  gay
//
//  Created by steve on 2020/11/4.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "AnonyousPostViewController.h"
#import "NewPostViewController.h"


#import "TagListView.h"
#import "TagView.h"

@interface AnonyousPostViewController (){
    TagListView *sportTagListView;
    TagListView *personTagListView;
    
//    NSMutableArray *sportArray;
//    NSMutableArray *personArray;
    NSMutableArray *tagArray;
    
    UIButton *continueBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation AnonyousPostViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    
    tagArray = [NSMutableArray array];
    
    for (NSString *textString in SportsArray) {
        [[sportTagListView addTag:textString] setOnTap:^(TagView *tagView) {
            [self setTagViewStyle:tagView];
        }];
    }
    
    for (NSString *textString in PersionalArray) {
        [[personTagListView addTag:textString] setOnTap:^(TagView *tagView) {
            [self setTagViewStyle:tagView];
        }];
    }
    
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) setTagViewStyle:(TagView *)tagView {
    
    for(TagView *tagView in [sportTagListView tagViews]) {
        tagView.borderWidth = 1;
        tagView.textColor = RGB(214, 215, 215);
        tagView.backgroundColor = [UIColor clearColor];
    }
    
    for(TagView *tagView in [personTagListView tagViews]) {
        tagView.borderWidth = 1;
        tagView.textColor = RGB(214, 215, 215);
        tagView.backgroundColor = [UIColor clearColor];
    }
    
    [tagArray removeAllObjects];
    
    [tagArray addObject:tagView.titleLabel.text];
    tagView.borderWidth = 0;
    tagView.textColor = UIColor.whiteColor;
    tagView.backgroundColor = ThemeYellow;
    
//    if ([tagArray containsObject:tagView.titleLabel.text]) {
//        [tagArray removeObject:tagView.titleLabel.text];
//
//        tagView.borderWidth = ZoomSize(1);
//        tagView.textColor =  RGB(214, 215, 215);
//        tagView.backgroundColor = UIColor.clearColor;
//
//    } else {
//        [tagArray addObject:tagView.titleLabel.text];
//
//        tagView.borderWidth = 0;
//        tagView.textColor = UIColor.whiteColor;
//        tagView.backgroundColor = ThemeYellow;
//    }
    
    if (tagArray.count > 0) {
        [self->continueBt setBackgroundColor:ThemeYellow];
        self->continueBt.enabled = YES;
    } else {
        [self->continueBt setBackgroundColor:TextGray];
        self->continueBt.enabled = NO;
    }
}

- (void)continueAction {
    NSLog(@"continueAction");
    [[NetTool shareInstance] sendFireBaseWithEventName:@"AnonyousPost_continue"];
    
    NewPostViewController *vc = [[NewPostViewController alloc] init];
    vc.infoType = anonymousInfoType;
    vc.tagArray = tagArray;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight , Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Anonymous Post";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = BGBlackColor;
        
        UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(20), SCREEN_Width, ZoomSize(20))];
        [_contentScrollView addSubview:infoLb];
        infoLb.textAlignment = NSTextAlignmentCenter;
        infoLb.text = @"Please choose an activity type you want to post.";
        infoLb.textColor = RGB(186, 179, 187);
        infoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        
        UILabel *sportLocationTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(60), ZoomSize(175), ZoomSize(20))];
        [_contentScrollView addSubview:sportLocationTipLb];
        sportLocationTipLb.text = @"Sports";
        sportLocationTipLb.textColor = WhiteColor;
        sportLocationTipLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        
        UIView *sportLocationLineView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(90), SCREEN_Width - ZoomSize(30) , ZoomSize(1))];
        [_contentScrollView addSubview:sportLocationLineView];
        sportLocationLineView.backgroundColor = RGB(54, 62, 63);
        
        sportTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(110), Screen_Width - ZoomSize(30), ZoomSize(80))];
        [_contentScrollView addSubview:sportTagListView];
        sportTagListView.cornerRadius = ZoomSize(6);
        sportTagListView.borderWidth = ZoomSize(1);
        sportTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
        sportTagListView.paddingX = ZoomSize(6);
        sportTagListView.paddingY = ZoomSize(8);
        sportTagListView.marginX = ZoomSize(6);
        sportTagListView.marginY = ZoomSize(8);
        sportTagListView.tagViewHeight = ZoomSize(30);
        sportTagListView.textColor = RGB(214, 215, 215);
        sportTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        
        UILabel *personLocationTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(200), ZoomSize(175), ZoomSize(20))];
        [_contentScrollView addSubview:personLocationTipLb];
        personLocationTipLb.text = @"Personal";
        personLocationTipLb.textColor = WhiteColor;
        personLocationTipLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightMedium];
        
        UIView *personLocationLineView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(230), SCREEN_Width - ZoomSize(30) , ZoomSize(1))];
        [_contentScrollView addSubview:personLocationLineView];
        personLocationLineView.backgroundColor = RGB(54, 62, 63);
        
        personTagListView = [[TagListView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(250), Screen_Width - ZoomSize(30), ZoomSize(40))];
        [_contentScrollView addSubview:personTagListView];
        personTagListView.cornerRadius = ZoomSize(6);
        personTagListView.borderWidth = ZoomSize(1);
        personTagListView.borderColor = [UIColor colorWithRed:57/255.0 green:66/255.0 blue:67/255.0 alpha:1];
        personTagListView.paddingX = ZoomSize(6);
        personTagListView.paddingY = ZoomSize(8);
        personTagListView.marginX = ZoomSize(6);
        personTagListView.marginY = ZoomSize(8);
        personTagListView.tagViewHeight = ZoomSize(30);
        personTagListView.textColor = RGB(214, 215, 215);
        personTagListView.textFont = [UIFont systemFontOfSize:ZoomSize(16)];
        
        continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), ZoomSize(500), Screen_Width - ZoomSize(72), ZoomSize(48)) ];
        [_contentScrollView addSubview:continueBt];
        continueBt.layer.cornerRadius = ZoomSize(24);
        [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
        [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        continueBt.backgroundColor = TextGray;
        [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
        continueBt.enabled = NO;
        
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(continueBt.frame) + ZoomSize(50));
        
    }
    return _contentScrollView;
}

@end

//
//  PostViewController.m
//  gay
//
//  Created by steve on 2020/9/15.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "PostViewController.h"
#import "UserPostsView.h"
#import "NewPostViewController.h"
#import "AnonyousPostViewController.h"

@interface PostViewController () {
    UserPostsView *normalPostsView;
    UserPostsView *anonymousPostsView;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation PostViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
    
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    [normalPostsView getHeaderData];
    [anonymousPostsView getHeaderData];
}

#pragma function
-(void)segmentSelectedAction:(UISegmentedControl*)sender{
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self->_contentScrollView setContentOffset:CGPointMake( 0, 0) animated:YES];
            break;
        case 1:
            [self->_contentScrollView setContentOffset:CGPointMake( SCREEN_Width, 0) animated:YES];
            break;
        default:
            break;
    }
}

- (void) normalPostsBtAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Post_normalPosts"];
    
    NewPostViewController *vc = [[NewPostViewController alloc] init];
    vc.infoType = normalInfoType;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) anonymousPostAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"Post_anonymousPost"];
    AnonyousPostViewController *vc = [[AnonyousPostViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navBgView.backgroundColor = BGBlackColor;

        NSArray *arr = [[NSArray alloc]initWithObjects:@"Posts", @"Anonymous", nil];

        UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:arr];
        segment.frame = CGRectMake(ZoomSize(40), StatusBarHeight, SCREEN_Width - ZoomSize(80), ZoomSize(30));
        [_navBgView addSubview:segment];
        [segment setBackgroundColor:BGBlackColor];
        [segment setTitleTextAttributes:@{NSForegroundColorAttributeName: WhiteColor,  NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(20)]} forState:UIControlStateSelected];
        [segment setTitleTextAttributes:@{NSForegroundColorAttributeName: RGB(250, 180, 22), NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(20)]} forState:UIControlStateNormal];
//        segment.layer.borderColor = RGB(250, 180, 22).CGColor;
//        segment.layer.borderWidth = ZoomSize(2);

        [segment addTarget:self action:@selector(segmentSelectedAction:) forControlEvents:UIControlEventValueChanged];
        segment.tintColor = RGB(250, 180, 22);
        if (@available(iOS 13.0, *)) {
            [segment setSelectedSegmentTintColor:RGB(250, 180, 22)];
        }
        segment.selectedSegmentIndex = 0;
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight - TabBarHeight )];
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width * 2, SCREEN_Height - NavigationHeight - TabBarHeight);
        _contentScrollView.bounces = NO ;
        _contentScrollView.alwaysBounceHorizontal = NO ;
        _contentScrollView.alwaysBounceVertical = NO ;
        
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.pagingEnabled = NO ;
        _contentScrollView.scrollEnabled = NO;
        
        normalPostsView = [[UserPostsView alloc] initWithFrame:CGRectMake(SCREEN_Width * 0, 0, SCREEN_Width, _contentScrollView.frame.size.height)];
        normalPostsView.getNav = self.navigationController;
        normalPostsView.infoType = normalInfoType;
        [_contentScrollView addSubview:normalPostsView];
        
        UIButton *normalPostsBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(90), _contentScrollView.frame.size.height - ZoomSize(80), ZoomSize(72), ZoomSize(72))];
        [_contentScrollView addSubview:normalPostsBt];
        [normalPostsBt addTarget:self action:@selector(normalPostsBtAction) forControlEvents:UIControlEventTouchUpInside];
        [normalPostsBt setBackgroundImage:[UIImage imageNamed:@"post_release"] forState:UIControlStateNormal];
        
        anonymousPostsView = [[UserPostsView alloc] initWithFrame:CGRectMake(SCREEN_Width * 1, 0, SCREEN_Width, _contentScrollView.frame.size.height)];
        anonymousPostsView.getNav = self.navigationController;
        anonymousPostsView.infoType = anonymousInfoType;
        [_contentScrollView addSubview:anonymousPostsView];
        
        UIButton *anonymousPostsBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width * 2 - ZoomSize(90), _contentScrollView.frame.size.height - ZoomSize(80), ZoomSize(72), ZoomSize(72))];
        [_contentScrollView addSubview:anonymousPostsBt];
        [anonymousPostsBt addTarget:self action:@selector(anonymousPostAction) forControlEvents:UIControlEventTouchUpInside];
        [anonymousPostsBt setBackgroundImage:[UIImage imageNamed:@"post_write"] forState:UIControlStateNormal];
        
    }
    return _contentScrollView;
}

@end

//
//  NewPostViewController.m
//  gay
//
//  Created by steve on 2020/10/29.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "NewPostViewController.h"
#import <SDWebImage/UIButton+WebCache.h>

#import "SelectPhotoManager.h"

#define textLength 512
#define textPlaceHolder @"Write something you need..."

@interface NewPostViewController ()<UITextViewDelegate>{
    UITextView *feedbackTV;
    UILabel *feedbackTipLb;
    UILabel *textNumLb;
    
    UIButton *addLocationBt;
    UIButton *continueBt;
    NSMutableArray *imgArray;
    UIImage *getPlaceHolderImg;//上传的占位图片
    
    NSString *getAddressString;
    NSString *latitudeString;
    NSString *longitudeString;

}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation NewPostViewController

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGBlackColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    _photoManager = [[SelectPhotoManager alloc] init];
    imgArray = [NSMutableArray array];
    getAddressString = @"";
    latitudeString = @"";
    longitudeString = @"";
    
    [self resetPage];
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"NewPost_submit"];
    
    NSDictionary *paramsDic;
    NSString *tagString = @"";
    if (_tagArray.count > 0) {
        tagString =  [_tagArray componentsJoinedByString:@","];
    }
    
    paramsDic = @{ @"storyType":[NSNumber numberWithInteger:_infoType], @"address":getAddressString,@"latitude":latitudeString, @"longitude":longitudeString, @"content": feedbackTV.text, @"tag":tagString};
    
    if (imgArray.count == 1) {
        paramsDic = @{ @"storyType":[NSNumber numberWithInteger:_infoType], @"address":getAddressString,@"latitude":latitudeString, @"longitude":longitudeString,  @"content": feedbackTV.text, @"msg1":[imgArray objectAtIndex:0], @"tag":tagString};
    } if (imgArray.count == 2) {
        paramsDic = @{ @"storyType":[NSNumber numberWithInteger:_infoType], @"address":getAddressString,@"latitude":latitudeString, @"longitude":longitudeString, @"content": feedbackTV.text, @"msg1":[imgArray objectAtIndex:0], @"msg2":[imgArray objectAtIndex:1], @"tag":tagString};
    } if (imgArray.count == 3) {
        paramsDic = @{ @"storyType":[NSNumber numberWithInteger:_infoType], @"address":getAddressString,@"latitude":latitudeString, @"longitude":longitudeString, @"content": feedbackTV.text, @"msg1":[imgArray objectAtIndex:0], @"msg2":[imgArray objectAtIndex:1], @"msg3":[imgArray objectAtIndex:2], @"tag":tagString};
    }
    
    [[NetTool shareInstance] startRequest:API_story_saveOrEdite method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    
}

- (void) addPhoto:(UIButton *) sender {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"NewPost_addPhoto"];
    
    [self.view endEditing:YES];
    
    long getTag = sender.tag - 100;
    long imgCount = imgArray.count;
    
    if ((imgCount - getTag) > 0 ) {
        NSLog(@"getTag %ld", getTag);
    } else {
        
        UIActivityIndicatorView *indicatorView = [self.contentScrollView viewWithTag:120 + getTag];
        [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
        
        __weak __typeof (self) weakSelf = self;
        _photoManager.successHandle = ^(SelectPhotoManager * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
            __strong __typeof(self) strongSelf = weakSelf;
            self->getPlaceHolderImg = resizeImage;
            
            [sender setImage:originalImage forState:UIControlStateNormal];
            [indicatorView startAnimating];
            
            NSData *resizeImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:resizeImage], 0.25);
            NSMutableArray *dataArray = [NSMutableArray arrayWithObjects: resizeImageData, nil];
            [[NetTool shareInstance] putDatas:dataArray fileType:@"jpg" needShowHUD:NO callback:^(id  _Nullable obj) {
                
                NSMutableArray *getArray = obj;
                [strongSelf->imgArray addObject:getArray[0]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf resetPage];
                });
            }];
        };
    }
}

- (void) deletePhoto:(UIButton *) sender {
    [imgArray removeObjectAtIndex:sender.tag - 110];
    [self resetPage];
}

- (void) addLocationAction {
    [[NetTool shareInstance] sendFireBaseWithEventName:@"NewPost_addLocation"];
    
    [[UserInfo shareInstant] setLocationInfo];
    [UserInfo shareInstant].addressCallBack = ^(id  _Nullable obj) {
        
        self->addLocationBt.userInteractionEnabled = NO;
        NSDictionary *addressDic = obj;
        if (![DataTool isEmptyString:addressDic[@"addressString"]]) {
            self->getAddressString = addressDic[@"addressString"];
            
            NSArray *addressArray = addressDic[@"addressArray"];
            self->latitudeString = addressArray.firstObject;
            self->longitudeString = addressArray.lastObject;
            [self->addLocationBt setTitle:addressDic[@"addressString"] forState:UIControlStateNormal];
        } else {
            [self->addLocationBt setTitle:@"Can't get address" forState:UIControlStateNormal];
        }
    };
}

- (void) resetPage {
    
    for (int i = 0; i<3; i++) {
        UIButton *subBt = [_contentScrollView viewWithTag:100 + i];
        UIButton *deleteBt = [_contentScrollView viewWithTag:110 + i];
        UIActivityIndicatorView *indicatorView = [_contentScrollView viewWithTag:120 + i];
        [indicatorView stopAnimating];
        
        if (i< imgArray.count ) {
            NSString *imgUrlString = imgArray[i];
            
            subBt.hidden = NO;
            deleteBt.hidden = NO;
            [subBt sd_setImageWithURL:[NSURL URLWithString:imgUrlString] forState:UIControlStateNormal placeholderImage:getPlaceHolderImg];
        } else if (i == imgArray.count) {
            subBt.hidden = NO;
            [subBt setImage:[UIImage imageNamed:@"post_add_photo"] forState:UIControlStateNormal];
            deleteBt.hidden = YES;
        } else {
            subBt.hidden = YES;
            deleteBt.hidden = YES;
        }
    }
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView*)textView {
    if([textView.text isEqualToString:textPlaceHolder]) {
        textView.text = @"";
        textView.textColor = WhiteColor;
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    if(textView.text.length<1) {
        textView.text = textPlaceHolder;
        textView.textColor= RGB(186, 179, 187);
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *tempStr = textView.text;
    if (tempStr.length > textLength) {
        tempStr = [tempStr substringToIndex:textLength];
        textView.text = tempStr;
    }
    
    textNumLb.text = [NSString stringWithFormat:@"( %lu/%d )", + textView.text.length, textLength];
    if (textView.text.length > 0) {
        [self->continueBt setBackgroundColor:ThemeYellow];
        self->continueBt.enabled = YES;
    } else {
        [self->continueBt setBackgroundColor:TextGray];
        self->continueBt.enabled = NO;
    }
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(18), StatusBarHeight , ZoomSize(30), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:[UIImage imageNamed:@"nav_new_back"] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), StatusBarHeight, Screen_Width - ZoomSize(100), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(22) weight:UIFontWeightMedium];
        
        if ( _infoType == normalInfoType) {
            titleLb.text = @"New Post";
        } else {
            titleLb.text = @"New Anonymous Post";
        }
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.backgroundColor = BGBlackColor;
        
        feedbackTV = [[UITextView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(0), SCREEN_Width - ZoomSize(30), ZoomSize(160)) ];
        [_contentScrollView addSubview:feedbackTV];
        feedbackTV.backgroundColor = BGBlackColor;
        feedbackTV.tintColor = RGB(250, 180, 22);
        feedbackTV.font = [UIFont systemFontOfSize:ZoomSize(16)];
        feedbackTV.delegate = self;
        feedbackTV.text = textPlaceHolder;
        feedbackTV.textColor= RGB(186, 179, 187);
        
        textNumLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(115), ZoomSize(160) , ZoomSize(90), ZoomSize(20))];
        [_contentScrollView addSubview:textNumLb];
        textNumLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        textNumLb.text = [NSString stringWithFormat:@"0/%d", textLength];
        textNumLb.textColor = RGB(186, 179, 187);
        textNumLb.textAlignment = NSTextAlignmentRight;
        
        for (int i = 0; i < 3; i++) {
            
            UIButton *photoBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(25) + ZoomSize(110) * (i%3) , ZoomSize(200), ZoomSize(100), ZoomSize(100))];
            [_contentScrollView addSubview:photoBt];
            photoBt.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [photoBt.imageView setContentMode:UIViewContentModeScaleAspectFill];
            
            [photoBt setImage:[UIImage imageNamed:@"post_add_photo"] forState:UIControlStateNormal];
            [photoBt addTarget:self action:@selector(addPhoto:) forControlEvents:UIControlEventTouchUpInside];
            photoBt.layer.cornerRadius = ZoomSize(4);
            photoBt.layer.masksToBounds = YES;
            photoBt.tag = 100 + i;
            photoBt.hidden = YES;
            
            UIButton *deleteBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(110) + ZoomSize(110) * (i%3), ZoomSize(195), ZoomSize(22), ZoomSize(22) )];
            [_contentScrollView addSubview:deleteBt];
            [deleteBt setBackgroundImage:[UIImage imageNamed:@"post_delete"] forState:UIControlStateNormal];
            [deleteBt addTarget:self action:@selector(deletePhoto:) forControlEvents:UIControlEventTouchUpInside];
            deleteBt.tag = 110 + i;
            deleteBt.hidden = YES;
            
            UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:photoBt.frame];
            indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [_contentScrollView addSubview:indicatorView];
            indicatorView.tag = 120 + i ;
        }
        
        addLocationBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(24), ZoomSize(320), ZoomSize(200), ZoomSize(30)) ];
        [_contentScrollView addSubview:addLocationBt];
        [addLocationBt setImage:[UIImage imageNamed:@"post_location"] forState:UIControlStateNormal];
        [addLocationBt setTitle:@"Add Location" forState:UIControlStateNormal];
        [addLocationBt setTitleColor:RGB(104, 111, 113) forState:UIControlStateNormal];
        addLocationBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
        addLocationBt.imageEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(0), 0, 0); //设置图片文字距离
        addLocationBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(7), 0, 0);
        addLocationBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(14)];
        [addLocationBt addTarget:self action:@selector(addLocationAction) forControlEvents:UIControlEventTouchUpInside];
        
        continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(36), ZoomSize(504), Screen_Width - ZoomSize(72), ZoomSize(48)) ];
        [_contentScrollView addSubview:continueBt];
        continueBt.layer.cornerRadius = ZoomSize(24);
        [continueBt setTitle:@"Post" forState:UIControlStateNormal];
        [continueBt setTitleColor:WhiteColor forState:UIControlStateNormal];
        continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(20) weight:UIFontWeightMedium];
        continueBt.backgroundColor = TextGray;
        [continueBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
        continueBt.enabled = NO;
        
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, CGRectGetMaxY(continueBt.frame) + ZoomSize(50));
    }
    return _contentScrollView;
}

@end

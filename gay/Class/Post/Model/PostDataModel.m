//
//  PostDataModel.m
//  gay
//
//  Created by steve on 2020/11/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import "PostDataModel.h"

@implementation PostDataModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"postId":@"id"};
}

@end

//
//  PostDataModel.h
//  gay
//
//  Created by steve on 2020/11/10.
//  Copyright © 2020 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PostDataModel : NSObject

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *commentNumber;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *createTime;

@property (nonatomic, strong) NSString *greatNumber;
@property (nonatomic, strong) NSString *headImgUrl;
@property (nonatomic, strong) NSString *postId;

//@property (nonatomic, strong) NSString *msg1;
//@property (nonatomic, strong) NSString *msg2;
//@property (nonatomic, strong) NSString *msg3;

@property (nonatomic, strong) NSMutableArray *msgList;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *storyType;
@property (nonatomic, strong) NSString *tag;
@property (nonatomic, strong) NSString *userCreateTime;
@property (nonatomic, strong) NSString *userId;

@property (nonatomic, assign) CGFloat rowHeight;

@end

NS_ASSUME_NONNULL_END

//{
//address = "";
//commentNumber = 0;
//content = "Hi morons";
//createTime = "2020-11-02T03:08:38.000+0000";

//greatNumber = 0;
//headImgUrl = "https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20201020040309445-8980.jpg";
//id = 1323099668363743232;
//msg1 = "https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20201102110836444-6282.jpg";
//msg2 = "";
//msg3 = "";
//msg4 = "<null>";
//msg5 = "<null>";
//msg6 = "<null>";
//msgList =             (
//"https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20201102110836444-6282.jpg",
//"",
//""
//);
//nickName = Steve;
//storyType = 0;
//tag = "\U7f8e\U666f";
//userCreateTime = "2020-10-19T09:55:48.000+0000";
//userId = 1988961;
//}

